type resolved_typename = 
  | SUnitType
  | SIntType
  | SBoolType
  | SStringType
  | SByteType
  | SFloatType
  | SEmptyListType
  | SFileType
  | SListType of resolved_typename
  | SFunctionType of resolved_argument list * resolved_typename
  | SActorType of resolved_argument list * resolved_typename
  | SStructType of Ast.identifier * resolved_argument list
and resolved_argument = Ast.identifier * resolved_typename


type sbranch = sexpr * sstmt
and sexpr_node = 
  | SAlloc
  | SUnitLiteral
  | SIntLiteral of int
  | SByteLiteral of char
  | SFileLiteral of int
  | SFloatLiteral of float
  | SBoolLiteral of bool
  | SStringLiteral of string
  | SListIndex of sexpr * sexpr
  | SListLiteral of sexpr list
  | SIdentifier of Ast.identifier
  | SFunctionCall of sexpr * sexpr list
  | SActorCall of sexpr * sexpr list
  | SFunctionLambda of sstmt
  | SActorLambda of sstmt
  | SBinop of sexpr * Ast.binop * sexpr
  | SUnop of Ast.unop * sexpr
  | SAssignment of sexpr * sexpr
  | SStructDereference of sexpr * int * string
and sexpr = resolved_typename * sexpr_node
and sstmt = 
  | SBlock of sstmt list
  | SExpr of sexpr
  | SIf of (sbranch list) * sstmt
  | SWhile of sexpr * sstmt
  | SFor of resolved_argument * sexpr * sstmt
  | SReturn of sexpr
  | SEmit of sexpr
  | SDeclarations of sdecl list
and sdecl = 
  | SVariable of Ast.identifier * resolved_typename * sexpr option
  | SType of Ast.identifier * resolved_typename

type sprogram = sdecl list
