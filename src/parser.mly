%{ open Ast %}

%token SEMI COLON EQUALS ARROW COMMA EOF
%token ACTOR FN 
%token TYPE STRUCT
%token LPAREN RPAREN LBRACE RBRACE LSQUARE RSQUARE

%token GT GEQ LT LEQ EQ NEQ
%token PLUS MINUS TIMES DIVIDE MOD
%token AND OR NOT
%token BIND
%token DOT
%token PLUSEQ MINUSEQ

%token WHILE
%token FOR IN
%token IF ELSE ELIF
%token RETURN EMIT
%token LET
%token <Ast.identifier> IDENTIFIER
%token <int> INT_LITERAL
%token <int> FILE_LITERAL
%token <float> FLOAT_LITERAL
%token <string> STRING_LITERAL
%token <char> BYTE_LITERAL
%token <bool> BOOL_LITERAL

%nonassoc ELSE
%right BIND
%right EQUALS
%right PLUSEQ
%right MINUSEQ
%left DOT
%left OR
%left AND
%left EQ NEQ
%left LT GT LEQ GEQ
%left PLUS MINUS
%left TIMES DIVIDE MOD
%right NOT NEG

%start program decl expr stmt
%type <Ast.program> program
%type <Ast.stmt> stmt
%type <Ast.decl> decl
%type <Ast.expr> expr

%%


program:
    | program_helper EOF  { List.rev $1 }

program_helper:
    | decl                { [$1] }
    | program_helper decl { ($2 :: $1) }

decl:
    | LET argument SEMI                                       { let id, typename = $2 in Variable(id, typename, None) }
    | LET argument EQUALS expr SEMI                           { let id, typename = $2 in Variable(id, typename, Some $4) }
    | FN IDENTIFIER arguments_list ARROW typename SEMI        { Variable($2, (FunctionTypename($3, $5)), None) }
    | FN IDENTIFIER arguments_list ARROW typename block       { Variable($2, (FunctionTypename($3, $5)), Some(FunctionLambda(FunctionTypename($3, $5), $6))) }
    | ACTOR IDENTIFIER arguments_list ARROW typename SEMI     { Variable($2, (ActorTypename($3, $5)), None) }
    | ACTOR IDENTIFIER arguments_list ARROW typename block    { Variable($2, (ActorTypename($3, $5)), Some(ActorLambda(ActorTypename($3, $5), $6))) }
    | TYPE IDENTIFIER EQUALS typename SEMI                    { Type($2, $4) }
    | TYPE IDENTIFIER EQUALS STRUCT arguments_list SEMI       { Type($2, StructTypename($2, $5)) }

argument:
    | IDENTIFIER COLON typename { ($1, $3) }

arguments_list_helper:
    | argument                             { [$1] }
    | arguments_list_helper COMMA argument { $3 :: $1 }

arguments_list:
    | LPAREN RPAREN { [] }
    | LPAREN arguments_list_helper RPAREN { List.rev $2 }

declaration_argument:
    | IDENTIFIER COLON typename { ($1, $3) }
    | typename { ("", $1) }

declaration_arguments_list_helper:
    | declaration_argument                                         { [$1] }
    | declaration_arguments_list_helper COMMA declaration_argument { $3 :: $1 }

declaration_arguments_list:
    | LPAREN RPAREN { [] }
    | LPAREN declaration_arguments_list_helper RPAREN { List.rev $2 }

variable_typename:
    | IDENTIFIER { Typename($1) }
    | LPAREN RPAREN { UnitType }
    | LSQUARE typename RSQUARE { ListTypename($2) }

function_typename:
    | FN declaration_arguments_list ARROW typename    { FunctionTypename($2, $4) }

actor_typename:
    | ACTOR declaration_arguments_list ARROW typename { ActorTypename($2, $4) }

typename:
    | variable_typename { $1 }
    | function_typename { $1 }
    | actor_typename { $1 }

elseif_helper: /* nothing */        { [] }
    | elseif_helper ELIF expr block { ($3, $4) :: $1 }

stmt:
    | decl                                   { Declaration($1) }
    | RETURN expr SEMI                       { Return($2) }
    | EMIT expr SEMI                         { Emit($2) }
    | expr SEMI                              { Expr($1) }
    | block                                  { $1 }
    | WHILE expr block                       { While($2, $3) }
    | FOR argument IN expr block             { For($2, $4, $5) }
    | IF expr block elseif_helper            { If(($2, $3) :: (List.rev $4), Block([])) }
    | IF expr block elseif_helper ELSE block { If(($2, $3) :: (List.rev $4), $6) }

block_helper:
    | stmt                { [$1] }
    | block_helper stmt   { $2 :: $1 }

block:
    | LBRACE RBRACE              { Block([]) }
    | LBRACE block_helper RBRACE { Block(List.rev $2) }

list_literal_helper:
    | expr                           { [$1] }
    | list_literal_helper COMMA expr { $3 :: $1 }

list_literal:
    | LSQUARE RSQUARE                     { [] } 
    | LSQUARE list_literal_helper RSQUARE { List.rev $2 } 

call_argument:
    | expr { $1 }

call_arguments_list_helper:
    | call_argument                                  { [$1] }
    | call_arguments_list_helper COMMA call_argument { $3 :: $1 }

call_arguments_list:
    | LPAREN RPAREN                            { [] }
    | LPAREN call_arguments_list_helper RPAREN { List.rev $2 }

/* A reference to a variable value; something that can meaningfully be assigned to */
reference:
    | IDENTIFIER                     { Identifier($1) }
    | reference DOT IDENTIFIER       { Binop($1, Dot, Identifier($3)) }
    | reference call_arguments_list  { FunctionCall($1, $2) }
    | reference LSQUARE expr RSQUARE { ListIndex($1, $3)}

expr:
    | INT_LITERAL             { IntLiteral($1) }
    | FLOAT_LITERAL           { FloatLiteral($1) }
    | FILE_LITERAL            { FileLiteral($1) }
    | STRING_LITERAL          { StringLiteral($1) }
    | BYTE_LITERAL            { ByteLiteral($1) }
    | BOOL_LITERAL            { BoolLiteral($1) }
    | LPAREN RPAREN           { UnitLiteral }
    | function_typename block { FunctionLambda($1, $2) }
    | actor_typename block    { ActorLambda($1, $2) }
    | reference               { $1 }
    | list_literal            { ListLiteral($1) }
    | expr PLUS expr          { Binop($1, Add, $3) }
    | expr MINUS expr         { Binop($1, Sub, $3) }
    | expr TIMES expr         { Binop($1, Mul, $3) }
    | expr DIVIDE expr        { Binop($1, Div, $3) }
    | expr EQ expr            { Binop($1, Equal, $3) }
    | expr NEQ expr           { Binop($1, Neq, $3) }
    | expr LT expr            { Binop($1, Less, $3) }
    | expr LEQ expr           { Binop($1, Leq, $3) }
    | expr GT expr            { Binop($1, Greater, $3) }
    | expr GEQ expr           { Binop($1, Geq, $3) }
    | expr MOD expr           { Binop($1, Mod, $3) }
    | expr AND expr           { Binop($1, And, $3) }
    | expr OR expr            { Binop($1, Or, $3) }
    | expr BIND expr          { Binop($1, Bind, $3) }
    | expr PLUSEQ expr        { Binop($1, MagicBind, $3) }
    | expr MINUSEQ expr       { Binop($1, MagicUnbind, $3) }
    | MINUS expr %prec NEG    { Unop(Neg, $2) }
    | NOT expr                { Unop(Not, $2) }
    | LPAREN expr RPAREN      { $2 }
    | reference EQUALS expr   { Assignment($1, $3) }
