open Ast;;
open Printf;;


module type Value = sig
  type t
  val key : t -> string
  val to_string : t -> string
end;;


module Make(ValueType: Value) = struct 
  module Map = Map.Make(String);;

  type value = ValueType.t

  type t = {
    values : value Map.t;
    parent  : t option;
  };;


  let empty = { values=Map.empty; parent=None }


  let child (parent: t) = { values=Map.empty; parent=Some(parent) }


  let depth (table: t) : int =
    let rec depth_helper (table: t) current = match table.parent with
      | None -> 0
      | Some(parent) -> depth_helper parent (current + 1)
    in
    depth_helper table 0


  let rec to_string table =
    let parent_str = match table.parent with
    | None -> ""
    | Some(parent) -> to_string parent
    in
    let padding = String.make (depth table) '\t' in
    let str = match table.values with 
    | _ when Map.is_empty table.values -> sprintf "%s<no elements>" padding
    | _ -> Map.fold (fun k v s -> s ^ (sprintf "%s%s:%s;\n" padding k (ValueType.to_string v))) table.values "" 
    in 
    sprintf "{\n%s, \nchild:{\n%s\n}}" parent_str str


  let rec find (id: string) (table: t) : value =
    try 
      Map.find id table.values
    with Not_found -> match table.parent with
      | None -> raise Not_found
      | Some(parent) -> find id parent


  let add (v: value) (table: t) : t = 
    let key = ValueType.key v in
    { table with values=(Map.add key v table.values) }


  let of_list (l: value list) : t =
    List.fold_left (fun table v -> add v table) empty l
end;;
