open Sast;;
open Core;;
open Printf;;
open Color;;

let keyword = green
let literal = cyan
let function_call = blue
let special_keyword = (fun s -> bold (red s))

let actor_keyword = keyword "actor"
let fn_keyword = keyword "fn"
let let_keyword = keyword "let"
let type_keyword = keyword "type"

let if_keyword = keyword "if"
let elif_keyword = keyword "elif"
let else_keyword = keyword "else"

let while_keyword = keyword "while"

let emit_keyword = special_keyword "emit"
let return_keyword = special_keyword "return"


let indentation_depth = 2
let indent depth s =
  (String.make (depth * indentation_depth) ' ') ^ s

let rec code_of_argument ((id, t): resolved_argument) : string =
  match id with
  | "" -> code_of_typename t
  | _ -> sprintf "%s: %s" id (code_of_typename t)

and code_of_arguments (arguments: resolved_argument list) : string =
  (String.concat ~sep:", " (List.map arguments ~f:code_of_argument))

and code_of_typename (t: resolved_typename) : string = 
  let s = match t with
  | SFunctionType(arguments, returntype) -> sprintf "%s(%s) -> %s" fn_keyword (code_of_arguments arguments) (code_of_typename returntype)
  | SActorType(arguments, returntype) -> sprintf "%s(%s) -> %s" actor_keyword (code_of_arguments arguments) (code_of_typename returntype)
  | SEmptyListType -> "[]"
  | SListType(t) -> sprintf "[%s]" (code_of_typename t)
  | SStructType(id, _) -> id
  | SUnitType -> "()"
  | SIntType -> "int"
  | SBoolType -> "bool"
  | SStringType -> "str"
  | SByteType -> "byte"
  | SFloatType -> "float"
  | SFileType -> "file"
  in
  yellow s

and code_of_sexpr ?depth:(depth=0) (sexpr: sexpr) : string =
  match sexpr with
  | _type, SAlloc             -> ""
  | _type, SUnitLiteral       -> "()"
  | _type, SIntLiteral(i)     -> literal (sprintf "%d" i)
  | _type, SFloatLiteral(f)   -> literal (sprintf "%f" f)
  | _type, SBoolLiteral(b)    -> literal (if b then "true" else "false")
  | _type, SByteLiteral(c)    -> literal (sprintf "'%c'" c)
  | _type, SStringLiteral(s)  -> literal (sprintf "\"%s\"" s)
  | _type, SFunctionLambda(s) -> sprintf "%s %s" (code_of_typename _type) (code_of_sstmt ~depth:depth s)
  | _type, SActorLambda(s)    -> sprintf "%s %s" (code_of_typename _type) (code_of_sstmt ~depth:depth s)
  | _type, SListLiteral(l)    -> sprintf "[%s]" (String.concat ~sep:", " (List.map l ~f:(code_of_sexpr ~depth:depth)))
  | _type, SUnop(op, x) -> 
    let op_str = match op with
      | Neg -> "-"
      | Not -> "!"
    in
    sprintf "%s%s" op_str (code_of_sexpr ~depth:depth x)
  | _type, SListIndex(l, i) -> sprintf "%s[%s]" (code_of_sexpr l) (code_of_sexpr i)
  | _type, SBinop(l, op, r) -> 
    begin
      match op with
      | _ -> 
        let op_str = match op with
          | Add -> "+"
          | Sub -> "-"
          | Mul -> "*"
          | Div -> "/"
          | Mod -> "%"
          | Ast.Equal -> "="
          | Neq -> "!="
          | Ast.Less -> "<"
          | Leq -> "<="
          | Dot -> "."
          | Ast.Greater -> ">"
          | Geq -> ">="
          | And -> "&&"
          | Or -> "||"
          | Bind -> "=>"
          | MagicBind -> "+="
          | MagicUnbind -> "-="
        in
        sprintf "(%s %s %s)" (code_of_sexpr ~depth:depth l) op_str (code_of_sexpr ~depth:depth r)
    end
  | _type , SIdentifier(s) -> s
  | _type , SAssignment(lvalue, expr) -> sprintf "%s = %s" (code_of_sexpr ~depth:depth lvalue) (code_of_sexpr ~depth:depth expr)
  | _type, SStructDereference(sexpr, i, s) -> sprintf "%s.%s" (code_of_sexpr ~depth:depth sexpr) s
  | _type, SFunctionCall(fn, arguments) -> 
    let fn_str = match fn with
    | _, SIdentifier(i) -> function_call i
    | _, _ -> code_of_sexpr ~depth:depth fn
    in
    sprintf "%s(%s)" fn_str (String.concat ~sep:", " (List.map arguments ~f:(code_of_sexpr ~depth:depth)))
  | _ -> "UNKNOWN_SEXPR"

and code_of_sstmt ?depth:(depth=0) (sstmt: sstmt) : string = 
  let s, should_indent = match sstmt with
  | SExpr(e) ->   (code_of_sexpr ~depth:depth e) ^ ";", true
  | SReturn(e) -> sprintf "%s %s;" return_keyword (code_of_sexpr ~depth:depth e), true
  | SEmit(e) ->   sprintf "%s %s;" emit_keyword (code_of_sexpr ~depth:depth e), true
  | SIf(branches, else_sblock) -> 
    let code_of_branch i ((sexpr, sstmt) : sbranch) = 
      let keyword = if i = 0 then if_keyword else elif_keyword in
      sprintf "%s %s %s" keyword (code_of_sexpr ~depth:depth sexpr) (code_of_sstmt ~depth:depth sstmt)
    in
    let branches_str = String.concat ~sep:" " (List.mapi branches ~f:code_of_branch) in
    let else_str = match else_sblock with
    | SBlock([]) -> ""
    | _ -> sprintf " %s %s" else_keyword (code_of_sstmt ~depth:depth else_sblock)
    in
    (branches_str ^ else_str), true

  | SBlock(stmts) -> 
    if List.length stmts = 0 then "{}", false
    else 
      let sstmts = (String.concat ~sep:"\n" (List.map stmts ~f:(code_of_sstmt ~depth:(depth + 1)))) in
      sprintf "{\n%s\n%s}" sstmts (indent depth ""), false
  | SWhile(expr, stmt) -> sprintf "%s %s %s" while_keyword (code_of_sexpr ~depth:depth expr) (code_of_sstmt ~depth:depth stmt), true
  | SFor((id, t), l, sblock) -> sprintf "for %s: %s in %s %s" id (code_of_typename t) (code_of_sexpr ~depth:depth l) (code_of_sstmt ~depth:depth sblock), true
  | SDeclarations(sdecls) -> (String.concat ~sep:"\n\n" (List.map sdecls ~f:(code_of_sdecl ~depth:depth))), false
  in
  if should_indent then indent depth s else s

and code_of_sdecl ?depth:(depth=0) (sdecl: sdecl) = 
  let s = match sdecl with
  | SType(id, resolved_typename) -> 
    let type_str = match resolved_typename with
    | SStructType(id, arguments) -> sprintf "%s(%s)" id (code_of_arguments arguments)
    | _ -> code_of_typename resolved_typename
    in
    sprintf "%s %s = %s;" type_keyword id type_str
  | SVariable(id, typ, value) -> 
    begin
      let prefix = sprintf "%s %s: %s" let_keyword id (code_of_typename typ) in
      begin
        match value with
        | None -> prefix ^ ";" 
        | Some v -> 
          match v with
          | SFunctionType(args, returntype), SFunctionLambda(sblock) -> 
            sprintf "%s %s(%s) -> %s %s" fn_keyword id (code_of_arguments args) (code_of_typename returntype) (code_of_sstmt ~depth:depth sblock)
          | _, SAlloc -> prefix ^ ";"
          | _ -> sprintf "%s = %s;" prefix (code_of_sexpr ~depth:depth v)
      end
    end
  in
  indent depth s

and code_of_sprogram (sprogram: sprogram) = 
  String.concat ~sep:"\n\n" (List.map sprogram ~f:(code_of_sdecl ~depth:0))
