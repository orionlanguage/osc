open Sast;;
module L = Llvm;;
open Core;;
module Debug = Oscdebug;;
open Printf;;
module String = String;;


(* Should be thrown if the functionality is not yet implemented in the semantic checker. *)
exception NotImplemented of string


exception ExpectedBuilder


exception ExpectedFunction of Sast.resolved_typename


(* Gets an appropriate variable name (i.e. LLVM register name) for the sexpr provided *)
let variable_name_of_sexpr (sexpr: sexpr) (default: string): string = match sexpr with
| _, SIdentifier(id) -> id
| _, SStructDereference(_, _, id) -> id
| _, _ -> default


let translate_sprogram (sprogram: Sast.sprogram) : (Codegen_env.t) =
  let env = Codegen_env.create () in

  (* Types *)
  let i32_t      = L.i32_type    env.context
  and i8_t       = L.i8_type     env.context
  and i1_t       = L.i1_type     env.context
  and float_t    = L.double_type env.context
  and void_t     = L.void_type   env.context 
  and size_t     = L.i64_type    env.context 

  and vector_t = L.named_struct_type env.context "vector_"
  and string_t = L.named_struct_type env.context "string"
  and actor_t = L.named_struct_type env.context "actor_"
  and file_t = L.named_struct_type env.context "file_"
  in

  let ptr_i32_t    = L.pointer_type i32_t    [@@ocaml.warning "-26"]
  and ptr_i8_t     = L.pointer_type i8_t
  and ptr_i1_t     = L.pointer_type i1_t     [@@ocaml.warning "-26"]
  and ptr_float_t  = L.pointer_type float_t  [@@ocaml.warning "-26"]
  and ptr_void_t   = L.pointer_type i32_t (* LLVM does not like void* *)
  and ptr_size_t   = L.pointer_type size_t   [@@ocaml.warning "-26"]
  and ptr_vector_t = L.pointer_type vector_t
  and ptr_string_t = L.pointer_type string_t
  and ptr_actor_t  = L.pointer_type actor_t [@@ocaml.warning "-26"]
  and ptr_file_t  = L.pointer_type file_t
  in

  let rec lltype_of_typename (typename: Sast.resolved_typename) (env: Codegen_env.t) : L.lltype = match typename with
    | SIntType         -> i32_t
    | SByteType        -> i8_t
    | SBoolType        -> i1_t
    | SStringType      -> ptr_string_t
    | SFloatType       -> float_t
    | SUnitType        -> void_t
    | SListType(_)     -> ptr_vector_t
    | SEmptyListType   -> ptr_vector_t
    | SActorType(_, _) -> ptr_actor_t
    | SFileType -> ptr_file_t
    | SFunctionType(arguments, return_type) ->
      let llreturn_type = (lltype_of_typename return_type env) in
      let llarguments = Array.map (Array.of_list arguments) ~f:(fun (_, t) -> lltype_of_typename t env) in
      L.pointer_type (L.function_type llreturn_type llarguments)
    | SStructType(identifier, members) ->
      try
        (Codegen_env.find_type identifier env).lltype
      with Not_found -> raise (NotImplemented ("Failed to find struct " ^ identifier))
    | _ -> raise (NotImplemented (sprintf "lltype_of_typename: %s" (Oscdebug.string_of_resolved_typename typename)))
  in

  let actor_fn_t = L.function_type void_t [| ptr_actor_t;  ptr_void_t |] in
  let ptr_actor_fn_t = L.pointer_type actor_fn_t in

  (* actor.h function declarations *)
  let llfn_actor_make_t = L.function_type ptr_actor_t [| ptr_actor_fn_t |] in
  let llfn_actor_make = L.declare_function "actor_make" llfn_actor_make_t env.the_module [@@ocaml.warning "-26"] in

  let llfn_actor_init_t = L.function_type void_t [| ptr_actor_fn_t;  ptr_actor_fn_t |] in
  let llfn_actor_init = L.declare_function "actor_init" llfn_actor_init_t env.the_module [@@ocaml.warning "-26"] in

  let llfn_actor_free_t = L.function_type void_t [| ptr_actor_t |] in
  let llfn_actor_free = L.declare_function "actor_free" llfn_actor_free_t env.the_module [@@ocaml.warning "-26"] in
  
  let llfn_actor_destroy_t = L.function_type void_t [| L.pointer_type ptr_actor_t |] in
  let llfn_actor_destroy_t = L.declare_function "actor_destroy" llfn_actor_destroy_t env.the_module [@@ocaml.warning "-26"] in

  let llfn_actor_subscribe_t = L.function_type ptr_actor_t [| ptr_actor_t; ptr_actor_t |] in
  let llfn_actor_subscribe = L.declare_function "actor_subscribe" llfn_actor_subscribe_t env.the_module [@@ocaml.warning "-26"] in

  let llfn_actor_copy_t = L.function_type ptr_actor_t [| ptr_actor_t |] in
  let llfn_actor_copy = L.declare_function "actor_copy" llfn_actor_copy_t env.the_module [@@ocaml.warning "-26"] in

  
  (* string.h function declarations *)
  let llfn_string_copy_t = L.function_type ptr_string_t [| ptr_string_t |] in
  let llfn_string_copy = L.declare_function "string_copy" llfn_string_copy_t env.the_module [@@ocaml.warning "-26"] in

  let llfn_string_destroy_t = L.function_type void_t [| L.pointer_type ptr_string_t  |] in
  let llfn_string_destroy = L.declare_function "string_destroy" llfn_string_destroy_t env.the_module [@@ocaml.warning "-26"] in

  (* vector.h function declarations *)
  let llfn_vector_init_t = L.function_type void_t [| ptr_vector_t; size_t |] in
  let llfn_vector_init = L.declare_function "vector_init" llfn_vector_init_t env.the_module [@@ocaml.warning "-26"] in
  let llfn_vector_free_t = L.function_type void_t [| ptr_vector_t |] in
  let llfn_vector_free = L.declare_function "vector_free" llfn_vector_free_t env.the_module [@@ocaml.warning "-26"] in
  let llfn_vector_make_t = L.function_type ptr_vector_t [| size_t |] in
  let llfn_vector_make = L.declare_function "vector_make" llfn_vector_make_t env.the_module [@@ocaml.warning "-26"] in
  let llfn_vector_destroy_t = L.function_type void_t [| L.pointer_type ptr_vector_t |] in
  let llfn_vector_destroy = L.declare_function "vector_destroy" llfn_vector_destroy_t env.the_module [@@ocaml.warning "-26"] in
  let llfn_vector_copy_t = L.function_type ptr_vector_t [| ptr_vector_t |] in
  let llfn_vector_copy = L.declare_function "vector_copy" llfn_vector_copy_t env.the_module [@@ocaml.warning "-26"] in
  let llfn_vector_reserve_t = L.function_type ptr_vector_t [| ptr_vector_t; size_t |] in
  let llfn_vector_reserve = L.declare_function "vector_reserve" llfn_vector_reserve_t env.the_module [@@ocaml.warning "-26"] in

  let copy_element_fn_t = L.function_type void_t [| ptr_void_t; ptr_void_t |] in
  let ptr_copy_element_fn_t = L.pointer_type copy_element_fn_t in
  let llfn_vector_deepcopy_t = L.function_type ptr_vector_t [| ptr_vector_t; ptr_copy_element_fn_t |] in
  let llfn_vector_deepcopy = L.declare_function "vector_deepcopy" llfn_vector_deepcopy_t env.the_module [@@ocaml.warning "-26"] in
  let llfn_vector_deepcopy_into_t = L.function_type void_t [| ptr_vector_t; ptr_copy_element_fn_t; ptr_vector_t |] in
  let llfn_vector_deepcopy_into = L.declare_function "vector_deepcopy_into" llfn_vector_deepcopy_into_t env.the_module [@@ocaml.warning "-26"] in

  let llfn_vector_append_t = L.function_type void_t [| ptr_vector_t; ptr_void_t |] in
  let llfn_vector_append = L.declare_function "vector_append" llfn_vector_append_t env.the_module [@@ocaml.warning "-26"] in

  let llfn_string_append_t = L.function_type void_t [| ptr_string_t; ptr_void_t |] in
  let llfn_string_append = L.declare_function "string_append" llfn_string_append_t env.the_module [@@ocaml.warning "-26"] in

  let llfn_vector_remove_t = L.function_type void_t [| ptr_vector_t; i32_t |] in
  let llfn_vector_remove = L.declare_function "vector_remove" llfn_vector_remove_t env.the_module  in

  let llfn_vector_pop_back_t = L.function_type void_t [| ptr_vector_t |] in
  let llfn_vector_pop_back = L.declare_function "vector_pop_back" llfn_vector_pop_back_t env.the_module [@@ocaml.warning "-26"] in
  let llfn_vector_set_t = L.function_type ptr_void_t [| ptr_vector_t; i32_t; ptr_void_t |] in
  let llfn_vector_set = L.declare_function "vector_set" llfn_vector_set_t env.the_module [@@ocaml.warning "-26"] in
  let llfn_vector_get_t = L.function_type ptr_void_t [| ptr_vector_t; i32_t |] in
  let llfn_vector_get = L.declare_function "vector_get" llfn_vector_get_t env.the_module [@@ocaml.warning "-26"] in
  let llfn_vector_clear_t = L.function_type void_t [| ptr_vector_t |] in
  let llfn_vector_clear = L.declare_function "vector_clear" llfn_vector_clear_t env.the_module [@@ocaml.warning "-26"] in

  let llfn_vector_size_t = L.function_type i32_t [| ptr_vector_t |] in
  let llfn_vector_size = L.declare_function "vector_size" llfn_vector_size_t env.the_module [@@ocaml.warning "-26"] in

  let declare_builtin_functions (env: Codegen_env.t) : Codegen_env.t =
    
    let llfn_string_format_t = L.var_arg_function_type ptr_string_t [| ptr_string_t |] in
    let llfn_string_format = L.declare_function "string_format" llfn_string_format_t env.the_module [@@ocaml.warning "-26"] in

    let llfn_string_print_t = L.function_type void_t [| ptr_string_t |] in
    let llfn_string_print = L.declare_function "string_print" llfn_string_print_t env.the_module [@@ocaml.warning "-26"] in

    let llfn_string_stoi_t = L.function_type i32_t [| ptr_string_t |] in
    let llfn_string_stoi = L.declare_function "string_stoi" llfn_string_stoi_t env.the_module [@@ocaml.warning "-26"] in

    let llfn_string_stof_t = L.function_type float_t [| ptr_string_t |] in
    let llfn_string_stof = L.declare_function "string_stof" llfn_string_stof_t env.the_module [@@ocaml.warning "-26"] in

    let llfn_vector_size_t = L.function_type i32_t [| ptr_vector_t |] in
    let llfn_vector_size = L.declare_function "vector_size" llfn_vector_size_t env.the_module [@@ocaml.warning "-26"] in

    let llfn_vector_range_t = L.function_type ptr_vector_t [| i32_t |] in
    let llfn_vector_range = L.declare_function "vector_range" llfn_vector_range_t env.the_module in

    let llfn_open_t = L.function_type ptr_file_t [| ptr_string_t; ptr_string_t |] in
    let llfn_open = L.declare_function "oopen" llfn_open_t env.the_module in

    let llfn_file_make_t = L.function_type ptr_file_t [| i32_t |] in
    let llfn_file_make = L.declare_function "file_make" llfn_file_make_t env.the_module in

    let llfn_close_t = L.function_type i32_t [| ptr_file_t |] in
    let llfn_close = L.declare_function "oclose" llfn_close_t env.the_module  in

    let llfn_read_t = L.function_type i32_t [| ptr_file_t; i32_t; ptr_string_t |] in
    let llfn_read = L.declare_function "oread" llfn_read_t env.the_module  in

    let llfn_write_t = L.function_type i32_t [| ptr_file_t; ptr_string_t |] in
    let llfn_write = L.declare_function "owrite" llfn_write_t env.the_module  in

    let llfn_good_t = L.function_type i1_t [| ptr_file_t |] in
    let llfn_good = L.declare_function "file_good" llfn_good_t env.the_module  in

    let llfn_tcp_bind_t = L.function_type ptr_file_t  [| i32_t |] in
    let llfn_tcp_bind = L.declare_function "tcp_bind" llfn_tcp_bind_t env.the_module  in

    let llfn_tcp_accept_t = L.function_type ptr_file_t  [| ptr_file_t |] in
    let llfn_tcp_accept = L.declare_function "tcp_accept" llfn_tcp_accept_t env.the_module  in

    let llfn_tcp_connect_t = L.function_type ptr_file_t  [| ptr_string_t; i32_t |] in
    let llfn_tcp_connect = L.declare_function "tcp_connect" llfn_tcp_connect_t env.the_module  in

    let llfn_tcp_ready_t = L.function_type i1_t  [| ptr_file_t |] in
    let llfn_tcp_ready = L.declare_function "tcp_ready" llfn_tcp_ready_t env.the_module  in

    let llfn_tcp_select_t = L.function_type i32_t  [| ptr_vector_t; float_t |] in
    let llfn_tcp_select = L.declare_function "tcp_select" llfn_tcp_select_t env.the_module  in

    let llfn_input_t = L.var_arg_function_type ptr_string_t [| |] in
    let llfn_input = L.declare_function "input" llfn_input_t env.the_module [@@ocaml.warning "-26"] in

    let builtins = [
      ("print",   SFunctionType(["s", SStringType], SUnitType),   llfn_string_print);
      ("format",  SFunctionType(["s", SStringType], SStringType), llfn_string_format);

      ("stoi",   SFunctionType(["s", SStringType], SIntType),   llfn_string_stoi);
      ("stof",   SFunctionType(["s", SStringType], SFloatType),   llfn_string_stof);
      ("string_append", SFunctionType(["string", SStringType; "character", SByteType], SUnitType), llfn_string_append);

      ("input",   SFunctionType([], SStringType), llfn_input);
      ("clear",   SFunctionType(["a", SListType(SIntType)], SUnitType), llfn_vector_clear);
      ("len",     SFunctionType(["a", SListType(SIntType)], SIntType), llfn_vector_size);
      ("range",   SFunctionType(["exclusive_max", SIntType], SListType(SIntType)), llfn_vector_range);
      ("append", SFunctionType(["list", SEmptyListType], SUnitType), llfn_vector_append);
      ("remove", SFunctionType(["list", SEmptyListType; "index", SIntType], SUnitType), llfn_vector_remove);

      ("len_Int", SFunctionType(["l", SListType(SIntType)], SIntType), llfn_vector_size);
      ("open",    SFunctionType(["filename", SStringType; "mode", SStringType], SFileType), llfn_open);
      ("close",   SFunctionType(["fileobj", SFileType], SIntType), llfn_close);
      ("read",    SFunctionType(["fileobj", SFileType; "nbyte", SIntType; "buffer", SStringType], SIntType), llfn_read);
      ("write",   SFunctionType(["fileobj", SFileType; "str", SStringType], SIntType), llfn_write);
      ("good",    SFunctionType(["fileobj", SFileType], SBoolType), llfn_good);

      ("file_make",    SFunctionType(["fd", SIntType], SFileType), llfn_file_make);

      ("tcp_bind",   SFunctionType(["port", SIntType], SFileType), llfn_tcp_bind);
      ("tcp_accept", SFunctionType(["socket", SFileType], SFileType), llfn_tcp_accept);
      ("tcp_ready", SFunctionType(["socket", SFileType], SBoolType), llfn_tcp_ready);

      ("tcp_select", SFunctionType(["sockets", SListType(SFileType); "timeout", SFloatType], SIntType), llfn_tcp_select);

      ("tcp_connect", SFunctionType(["address", SStringType], SFileType), llfn_tcp_connect);
    ] in

    List.fold_left builtins ~f:(fun env (id, t, ll) -> fst(Codegen_env.define_function id t ll env)) ~init:env
  in

  (* Add builtins like print to the env. *)
  let env = declare_builtin_functions env in

  let rec translate_function (env: Codegen_env.t) (fn: L.llvalue) (sexpr: Sast.sexpr) : (Codegen_env.t) = 
    let define_argument env (name, typename) llarg = 
      (* copy argument onto the stack, for consistancy *)
      let llstack_arg = Codegen_env.llstore_alloc name llarg env in
      Codegen_env.define_variable name typename llstack_arg env in

    let llparams_list = (Array.to_list (L.params fn)) in

    let typename, sexpr_node = sexpr in
    match sexpr with
    | SFunctionType(arguments, return_type), SFunctionLambda(sblock) ->
      let builder = L.builder_at_end env.context (L.entry_block fn) in

      let env_function = { env with scope_context = FunctionScope(typename); builder = builder } in

      let env = List.fold2_exn arguments llparams_list ~f:define_argument ~init:env_function in

      _translate_sstmt env sblock

    | SActorType(arguments, return_type), SActorLambda(sblock) ->
      let builder = L.builder_at_end env.context (L.entry_block fn) in

      let env = { env with scope_context = ActorScope(typename); builder = builder } in

      let llthis = Codegen_env.llstore_alloc "this_arg" (L.params fn).(0) env in
      let env = Codegen_env.define_variable "this" (SActorType([], SUnitType)) llthis env in

      let argument_identifier, argument_typename = List.hd_exn arguments in
      let llbitcast = L.build_bitcast (L.params fn).(1) (L.pointer_type (lltype_of_typename argument_typename env)) (argument_identifier ^ "_void_ptr") env.builder in
      let llload = L.build_load llbitcast (argument_identifier ^ "_val") env.builder in
      let lldata = Codegen_env.llstore_alloc argument_identifier llload env in
      let env = Codegen_env.define_variable argument_identifier argument_typename lldata env in

      let env = _translate_sstmt env sblock in
      let _  = L.build_ret_void env.builder in
      env

    | _ -> let typename, _ = sexpr in raise (ExpectedFunction(typename))


  (* Given a sexpr, produce LLVM that represents the VALUE of the sexpr, i.e.
   * can be used as the first operand in a store instruction *)
  and _translate_value_sexpr ?identifier (env: Codegen_env.t) (sexpr: Sast.sexpr) : (Codegen_env.t * L.llvalue) =
    let typename, sexpr_node = sexpr in

    let env, lle = _translate_sexpr ?identifier env sexpr in
    let llvalue = match sexpr_node with 
    | SAlloc | SByteLiteral(_) |SIntLiteral(_) | SListLiteral(_) | SBoolLiteral(_) | SFileLiteral(_) | SFloatLiteral(_) | SStringLiteral(_) | SFunctionCall(_, _) | SUnop(_,_) | SBinop(_,_,_) | SActorCall(_, _) | SActorLambda(_) -> lle
    | _ -> L.build_load lle (Option.value ~default:"tmp-1" identifier) env.builder
    in
    env, llvalue


  (* Given a sexpr, produce LLVM that represents the ADDRESS of the sexpr, i.e.
   * can be used as the second operand in a store instruction *)
  and _translate_sexpr ?identifier (env: Codegen_env.t) (sexpr: Sast.sexpr) : (Codegen_env.t * L.llvalue) = 
    let typename, sexpr_node = sexpr in
    match sexpr_node with
    | SAlloc ->
      let llfn_mmalloc_t = L.function_type ptr_void_t [| size_t |] in
      let llfn_mmalloc = L.declare_function "mmalloc" llfn_mmalloc_t env.the_module [@@ocaml.warning "-26"] in

      let lltype = lltype_of_typename typename env in
      let llstruct_t = L.element_type lltype in

      let llcall = L.build_call llfn_mmalloc [| L.size_of llstruct_t |] "" env.builder in
      let llbitcast = L.build_bitcast llcall lltype "" env.builder in

      env, llbitcast

    | SStructDereference(structure, i, member_name) ->
      let env_after_struct, llstruct = _translate_value_sexpr env structure in
      let llgep = L.build_struct_gep llstruct i (member_name ^ "_ptr") env.builder in
      let lltype = lltype_of_typename typename env in
      let bitcast = L.build_bitcast llgep (L.pointer_type lltype) member_name env.builder in
      env, bitcast

    | SAssignment(lsexpr, rsexpr) -> 
      let variable_name = match lsexpr with 
        | _ ,SListIndex((_,SIdentifier(id)),r) -> id
        | _, SIdentifier(id) -> id
        | _, SStructDereference(_, _, id) -> id
        | _, _ -> ""
      in
      let env, lllsexpr = match lsexpr with 
                            | _ , SBinop ((_,SListIndex(_l,_r)),Equal,_) ->   begin
                                                                                let _env, __l = _translate_value_sexpr env _l in
                                                                                let _env1, __r = _translate_value_sexpr _env _r in
                                                                                let llvector_val = L.build_call llfn_vector_get [| __l; __r |] variable_name _env1.builder in
                                                                                  _env1, llvector_val
                                                                               end
                            | _ ->  _translate_sexpr ~identifier:variable_name env lsexpr in
      let env, llrsexpr = match rsexpr with
      | _, SFunctionLambda(_) -> _translate_value_sexpr ~identifier:variable_name env rsexpr
      | _, SActorLambda(_) -> _translate_value_sexpr ~identifier:(variable_name ^ "_fn") env rsexpr
      | _, _ ->_translate_value_sexpr ~identifier:variable_name env rsexpr
      in
      let _ = L.build_store llrsexpr lllsexpr env.builder in
      env, lllsexpr

    | SIdentifier(identifier) -> 
      let llentry = Codegen_env.find_variable identifier env in
      env, llentry.llvalue

    | SStringLiteral(s) -> 
      let llfn_string_make_t = L.function_type ptr_string_t [| ptr_i8_t |] in
      let llfn_string_make = L.declare_function "string_make" llfn_string_make_t env.the_module in

      let builder = env.builder in
      let llstring = L.build_global_stringptr s "" builder in

      let varname = match identifier with
      | Some(s) -> s
      | None -> "str"
      in

      let llcall = L.build_call llfn_string_make [| llstring |] (varname ^ "_init") builder in
      env, llcall

    | SByteLiteral(b) -> 
      let lltype = lltype_of_typename typename env in
      env, L.const_int lltype (int_of_char b)

    | SFloatLiteral(f) -> 
      let lltype = lltype_of_typename typename env in
      env, L.const_float lltype f

    | SIntLiteral(i) -> 
      let lltype = lltype_of_typename typename env in
      env, L.const_int lltype i

    | SBoolLiteral(b) -> 
      let lltype = lltype_of_typename typename env in
      env, L.const_int lltype (if b then 1 else 0)

    | SFileLiteral(i) -> 
      let lltype = lltype_of_typename typename env in
      env, L.const_int lltype i

    | SUnitLiteral -> 
      let lltype = lltype_of_typename typename env in
      env, L.const_int lltype 0
    
    | SListLiteral(s) ->
      let varname = match identifier with
      | Some(_s) -> _s
      | None -> "list"
      in
      let llelement_type = lltype_of_typename typename env in
      let element_size = L.size_of llelement_type in 
      let llvector = L.build_call llfn_vector_make [| element_size |] (varname^ "_init") env.builder in
      let _create_lists_helper (_env, _llvector) sexpr = 
        let new_env, llval = _translate_value_sexpr ~identifier:varname _env sexpr in
        (* Like function calls, we need to store tmp copies locally before the appends *)
        let lltmp = Codegen_env.llstore_alloc "" llval env in
        let llbitcast = L.build_bitcast lltmp (ptr_void_t) "" _env.builder in
        let _ = L.build_call llfn_vector_append [| _llvector; llbitcast |] "" new_env.builder in
        new_env, _llvector
      in
      let _env, llval_ = List.fold_left s ~f:_create_lists_helper ~init:(env, llvector) in 
      _env, llvector

    | SListIndex(lsexpr, rsexpr) -> 
      let env, l = _translate_value_sexpr env lsexpr in
      let env, r = _translate_value_sexpr env rsexpr in

      let varname = match identifier with
        | Some(s) -> s
        | None -> "value"
      in

      let () = match fst lsexpr with
        | SListType(_) -> ()
        | SStringType -> ()
        | _ -> raise (Failure "Semantic checker let ListIndex be called on a non-list!")
      in
      let l = L.build_bitcast l ptr_vector_t (varname^"_cast") env.builder in

      let llcall = L.build_call llfn_vector_get [| l; r |] "list_get" env.builder in
      let llbitcast = L.build_bitcast llcall (L.pointer_type (lltype_of_typename typename env)) varname env.builder in

      env, llbitcast

    | SUnop(op, sexpr) ->
      let env, s = _translate_value_sexpr env sexpr in
      let typename, _ = sexpr in

      let llvalue = match typename with
      | SBoolType ->
        begin
          match op with
          | Not     -> L.build_not s "" env.builder
          | _       -> raise (Failure "SUnop: Wrong operator for SBoolType.")
        end
      | SIntType ->
        begin
          match op with
          | Neg -> L.build_neg s "" env.builder
          | _       -> raise (Failure "SUnop: Wrong operator for SFloatType.")
        end
      | SFloatType ->
        begin
          match op with
          | Neg -> L.build_fneg s "" env.builder
          | _       -> raise (Failure "SUnop: Wrong operator for SFloatType.")
        end
      | _ -> raise (Failure "SUnop: Wrong type.")
      in
      env, llvalue


    | SBinop(lsexpr, op, rsexpr) ->
      let env, l = _translate_value_sexpr env lsexpr in
      let env, r = _translate_value_sexpr env rsexpr in

      begin
        match op with
        | _ ->
          let l_typename, _ = lsexpr in
          begin

            match l_typename with
            | SActorType(_, _) -> 
              let llfn_actor_copy_subscribe = L.declare_function "actor_copy_subscribe" llfn_actor_subscribe_t env.the_module in
              let llfn_actor_subscribe = L.declare_function "actor_subscribe" llfn_actor_subscribe_t env.the_module in
              let llfn_actor_unsubscribe = L.declare_function "actor_unsubscribe" llfn_actor_subscribe_t env.the_module in
              begin
                match op with
                | Bind -> env, L.build_call llfn_actor_copy_subscribe [| l; r; |] "actor_tmp" env.builder
                | MagicBind -> env, L.build_call llfn_actor_subscribe [| l; r; |] "actor_tmp" env.builder
                | MagicUnbind -> env, L.build_call llfn_actor_unsubscribe [| l; r; |] "actor_tmp" env.builder
                | _       -> raise (Failure "Wrong operator for SIntType.")
              end
            | SFileType -> 
              begin
                match op with
                | Equal -> 
                  let llfn_file_equal_t = L.function_type i1_t [| ptr_file_t; ptr_file_t |] in
                  let llfn_file_equal = L.declare_function "file_equal" llfn_file_equal_t env.the_module in
                  env, L.build_call llfn_file_equal [| l; r; |] "tmp" env.builder
                | Neq -> _translate_value_sexpr ?identifier env (SBoolType, SUnop(Not, (SBoolType, SBinop(lsexpr, Equal, rsexpr))))
                | _       -> raise (Failure "Wrong operator for SFileType.")
              end
            | SStringType -> 
              begin
                match op with
                | Add -> 
                  let llfn_string_concatenate_t = L.function_type ptr_string_t [| ptr_string_t; ptr_string_t |] in
                  let llfn_string_concatenate = L.declare_function "string_concatenate" llfn_string_concatenate_t env.the_module in
                  env, L.build_call llfn_string_concatenate [| l; r; |] "tmp" env.builder
                | Equal -> 
                  let llfn_string_equal_t = L.function_type i1_t [| ptr_string_t; ptr_string_t |] in
                  let llfn_string_equal = L.declare_function "string_equal" llfn_string_equal_t env.the_module in
                  env, L.build_call llfn_string_equal [| l; r; |] "tmp" env.builder
                | Neq -> _translate_value_sexpr ?identifier env (SBoolType, SUnop(Not, (SBoolType, SBinop(lsexpr, Equal, rsexpr))))
                | _       -> raise (Failure "Wrong operator for SStringType.")
              end
            | _ -> 
              let llop_builder = 
                match l_typename with
                | SByteType -> 
                  begin
                    match op with
                    | Add     -> L.build_add
                    | Sub     -> L.build_sub
                    | Mul     -> L.build_mul
                    | Div     -> L.build_sdiv
                    | Equal   -> L.build_icmp L.Icmp.Eq
                    | Neq     -> L.build_icmp L.Icmp.Ne
                    | Greater -> L.build_icmp L.Icmp.Ugt
                    | Geq     -> L.build_icmp L.Icmp.Uge
                    | Less    -> L.build_icmp L.Icmp.Ult
                    | Leq     -> L.build_icmp L.Icmp.Ule
                    | _       -> raise (Failure "Wrong operator for SIntType.")
                  end
                | SIntType -> 
                  begin
                    match op with
                    | Add     -> L.build_add
                    | Sub     -> L.build_sub
                    | Mul     -> L.build_mul
                    | Div     -> L.build_sdiv
                    | Equal   -> L.build_icmp L.Icmp.Eq
                    | Neq     -> L.build_icmp L.Icmp.Ne
                    | Greater -> L.build_icmp L.Icmp.Ugt
                    | Geq     -> L.build_icmp L.Icmp.Uge
                    | Less    -> L.build_icmp L.Icmp.Ult
                    | Leq     -> L.build_icmp L.Icmp.Ule
                    | _       -> raise (Failure "Wrong operator for SIntType.")
                  end
                | SFloatType -> 
                  begin
                    match op with
                    | Add     -> L.build_fadd
                    | Sub     -> L.build_fsub
                    | Mul     -> L.build_fmul
                    | Div     -> L.build_fdiv
                    | Equal   -> L.build_fcmp L.Fcmp.Ueq
                    | Neq     -> L.build_fcmp L.Fcmp.Une
                    | Greater -> L.build_fcmp L.Fcmp.Ugt
                    | Geq     -> L.build_fcmp L.Fcmp.Uge
                    | Less    -> L.build_fcmp L.Fcmp.Ult
                    | Leq     -> L.build_fcmp L.Fcmp.Ule
                    | _       -> raise (Failure "Wrong operator for SFloatType.")
                  end
                | SBoolType ->
                  begin
                    match op with
                    | And -> L.build_and
                    | Or -> L.build_or
                    | _       -> raise (Failure "Wrong operator for SBoolType.")
                  end
                | _ -> raise (NotImplemented "Only float, int, bool operations for now.")
              in

              env, llop_builder l r "" env.builder
          end
      end

    | SActorLambda(sblock) -> 
      let identifier = match identifier with
        | Some(id) -> id
        | None -> "actor"
      in

      let llfn = L.define_function identifier actor_fn_t (env.the_module) in

      let _ = translate_function env llfn sexpr in

      env, L.build_call llfn_actor_make [| llfn |] identifier env.builder

    | SFunctionLambda(sblock) -> 
      let identifier = match identifier with
        | Some(id) -> id
        | None -> "lambda"
      in

      let llfn_t = lltype_of_typename typename env in
      let llfn = L.define_function identifier (L.element_type llfn_t) (env.the_module) in
      let _ = translate_function env llfn sexpr in

      let ptr_identifier = (identifier ^ "_ptr") in

      let llfn_ptr = match env.scope_context with
      | GlobalScope -> L.define_global ptr_identifier llfn env.the_module
      | _ -> Codegen_env.llstore_alloc ptr_identifier llfn env
      in

      env, llfn_ptr

    | SActorCall(actor_expr, arguments) -> 
      let id = variable_name_of_sexpr actor_expr "lambda" in
      let env, llactor = _translate_value_sexpr ~identifier:id env actor_expr in

      let llfn_actor_start_t = L.function_type void_t [| ptr_actor_t; ptr_void_t |] in
      let llfn_actor_start = L.declare_function "actor_start" llfn_actor_start_t env.the_module in

      let argument = match arguments with
        | [argument] -> argument
        | _ -> raise (Failure "SActorCall expected only one argument!")
      in

      let env, llargument_value = _translate_value_sexpr ~identifier:(id ^ "_arg0_value") env argument in
      let llargument = Codegen_env.llstore_alloc (id ^ "_arg0") llargument_value env in

      let llbitcast = L.build_bitcast llargument ptr_void_t "" env.builder in

      let llcall = L.build_call llfn_actor_start [| llactor; llbitcast |] "" env.builder in
      env, llcall

    | SFunctionCall(fn_expr, arguments) -> 
      let id = variable_name_of_sexpr fn_expr "lambda" in

      let _t, function_type = fn_expr in
      let f_id = match fn_expr with 
        | _, SIdentifier(id) -> id
        | _ -> "tmp"
      in 
      let is_format = String.equal "format" f_id in
      let is_append = String.equal "append" f_id in
      let is_len = String.equal "len" f_id in

      let env, llfn = match is_append, (List.hd arguments) with
        | true, (Some (SStringType, _)) -> _translate_value_sexpr env (SFunctionType(["string", SStringType; "character", SByteType], SUnitType), SIdentifier("string_append"))
        | _ -> _translate_value_sexpr env fn_expr 
      in

      let translate_argument (i, env, arguments) sexpr =
        let env_after_argument, llsexpr = 
          if is_append && i <> 0 then 
            let env, value = _translate_value_sexpr ~identifier:(id ^ "_arg" ^ (string_of_int i)) env sexpr in
            let llvalue = Codegen_env.llstore_alloc "" value env in
            let llbitcast = L.build_bitcast llvalue ptr_void_t "" env.builder in
            env, llbitcast
          else if is_len && i = 0 then
            let env, llvalue = _translate_value_sexpr ~identifier:(id ^ "_arg" ^ (string_of_int i)) env sexpr in
            let llbitcast = L.build_bitcast llvalue ptr_vector_t "" env.builder in
            env, llbitcast
          else _translate_value_sexpr ~identifier:(id ^ "_arg" ^ (string_of_int i)) env sexpr 
        in

        (* when Oscar strings are passed into format(), we need to turn them into const char* using string_get *)
        let llfn_string_get_t = L.function_type ptr_i8_t [| ptr_string_t |] in
        let llfn_string_get = L.declare_function "string_get" llfn_string_get_t env.the_module in
        let modified_llsexpr = 
          if i = 0 || not is_format then llsexpr else 
              match sexpr with
              | SStringType, _ -> L.build_call llfn_string_get [| llsexpr |] "s" env.builder
              | _ -> llsexpr
        in

        i + 1, env_after_argument, arguments @ [modified_llsexpr]
      in

      let _, env_after_arguments, llarguments = List.fold_left ~f:translate_argument ~init:(0, env, []) arguments in

      (* If function returns void (unit type), result must be named "" for Llvm to be happy. *)
      let result_variable_name = match fst fn_expr with 
      | SFunctionType(_, return_type) -> begin
          match return_type with
          | SUnitType -> ""
          | _ -> id ^ "_result"
        end
      | t -> raise (ExpectedFunction t)
      in

      let llcall = L.build_call llfn (Array.of_list llarguments) result_variable_name env.builder in
      env_after_arguments, llcall


  and _translate_sdecl (env: Codegen_env.t) (sdecl: Sast.sdecl) : (Codegen_env.t) = match sdecl with
    | SVariable(identifier, typename, sexpr_opt) ->
      begin
        match sexpr_opt with
        | None -> 
          begin
            let lltype = (lltype_of_typename typename env) in
            let llvalue = match env.scope_context with
            | GlobalScope -> 
              let uninitialized = (L.const_int i32_t 0) in
              let llinit = L.build_bitcast uninitialized lltype "" env.builder in
              L.define_global identifier llinit env.the_module
            | _ -> L.build_alloca lltype identifier env.builder 
            in
            Codegen_env.define_variable identifier typename llvalue env
          end

        | Some(sexpr) ->
          match env.scope_context with
          | GlobalScope ->
            let _, llfn = match snd sexpr with
            | SFunctionLambda(_) -> _translate_sexpr ~identifier:identifier env sexpr
            | _ -> raise (NotImplemented (sprintf "You cannot initialize '%s' at global scope in codegen (Semantic Checker should have added to __initialize())!" (Fmtlib.code_of_sexpr sexpr)))
            in
            Codegen_env.define_variable identifier typename llfn env
          | _ -> 
            match sexpr with
              | SListType(ty), SListLiteral([]) ->  let llelement_type = lltype_of_typename ty env in
                                                    let element_size = L.size_of llelement_type in 
                                                    let llvector = L.build_call llfn_vector_make [| element_size |] (identifier^ "_init") env.builder in
                                                    let llresult = Codegen_env.llstore_alloc identifier llvector env in
                                                    Codegen_env.define_variable identifier typename llresult env

              | _ ->let env, llvalue = _translate_value_sexpr ~identifier:(identifier ^ "_val") env sexpr in
                    let llresult = Codegen_env.llstore_alloc identifier llvalue env in
                    Codegen_env.define_variable identifier typename llresult env
      end

    | SType(identifier, resolved_typename) ->
      begin
        match resolved_typename with
        | SStructType(_, members) ->
          let llstruct_t = L.named_struct_type env.context identifier in
          let llmembers = Array.map (Array.of_list members) ~f:(fun (_, t) -> lltype_of_typename t env) in
          let _ = L.struct_set_body llstruct_t llmembers true in
          let ptr_llstruct_t = L.pointer_type llstruct_t in
          Codegen_env.define_type identifier resolved_typename ptr_llstruct_t env
        | _ -> raise (NotImplemented "Haven't implemented type aliasing yet")
      end

  and _translate_sstmt (env: Codegen_env.t) (sstmt: Sast.sstmt) : (Codegen_env.t) = 
    match sstmt with
  | SDeclarations(sdecls) -> List.fold_left sdecls ~f:_translate_sdecl ~init:env
  | SBlock(sstmts) -> 
    let block_env = Codegen_env.new_scope env in
    List.fold_left sstmts ~f:_translate_sstmt ~init:block_env
  
  | SFor((arg_id, arg_type), sexpr, sstmt) -> 
    let vector_sexpr_identifier = "__for_vector__" in
    let vector_sexpr = (SListType(arg_type), SIdentifier(vector_sexpr_identifier)) in

    let len_type = SFunctionType(["a", SListType(SIntType)], SIntType) in
    let len_call = (SIntType, SFunctionCall((len_type, SIdentifier("len")), [vector_sexpr])) in

    let index_sexpr = (SIntType, SIdentifier("__for_index__")) in

    let while_condition = (SBoolType, SBinop(index_sexpr, Less, len_call)) in
    let equivalent = SBlock([
      SDeclarations(
        [
          (* The index for looping over *)
          SVariable("__for_index__", SIntType, Some (SIntType, SIntLiteral(0)));

          (* Local copy of the vector, evaluated once, so you can call a function for example. *)
          SVariable(vector_sexpr_identifier, SListType(arg_type), Some sexpr);

          (* The value, with name and type provided by user *)
          SVariable(arg_id, arg_type, None);
        ]
      );
      SWhile(while_condition, SBlock(
        (* let x: str = v[__for_index__] *)
        SExpr(arg_type, SAssignment((arg_type, SIdentifier(arg_id)), (arg_type, SListIndex(vector_sexpr, index_sexpr)))) ::

        (* __for_index__ += 1 *)
        SExpr(SIntType, SAssignment(index_sexpr, (SIntType, SBinop(index_sexpr, Add, (SIntType, SIntLiteral(1)))))) ::

        [sstmt]
      ))
    ])
    in
    _translate_sstmt env equivalent

  | SWhile(sexpr, sstmt) -> 
    (*let block_env = Codegen_env.new_scope env in*)
    let add_terminal builder instr = match L.block_terminator (L.insertion_block builder) with
      | Some _ -> ()
      | None -> ignore (instr builder) 
    in

    let cur_bb = L.insertion_block env.builder in
    let parent = L.block_parent cur_bb in
    let merge_bb = L.append_block env.context "merge" parent in
    let branch_instr = L.build_br merge_bb in
    (* for loop checker block*)
    let while_c = L.insert_block env.context "while_c" merge_bb in
    let branch_instr_whilec = L.build_br while_c in
    let () = add_terminal env.builder branch_instr_whilec in
    (* add terminal to curr_bb then allocate iterator*)
    let whilec_build =  L.builder_at_end env.context while_c in
    let wc_env = Codegen_env.env_with_builder env whilec_build in
    let env_expr, _llval_expr = _translate_value_sexpr wc_env sexpr in
    (* For get value*)
    let w_bb = L.insert_block env_expr.context "while" merge_bb in
    let _ = L.build_cond_br (_llval_expr) (w_bb) merge_bb env_expr.builder in
    (* Get value for for loop*)
    let w_build =  L.builder_at_end env_expr.context w_bb in
    let w_env = Codegen_env.env_with_builder env_expr w_build in
    (* deal with for sstmts*)
    let env_after_for_stmts = _translate_sstmt w_env sstmt in
    let () = add_terminal env_after_for_stmts.builder (L.build_br while_c) in
    (* Set ret to end of merge*)
    let ret_build =  L.builder_at_end env_after_for_stmts.context merge_bb in
    let ret = Codegen_env.env_with_builder env_after_for_stmts ret_build in
    ret

  | SIf(sstmts, else_sstmt) ->

    let add_terminal builder instr = match L.block_terminator (L.insertion_block builder) with
      | Some _ -> ()
      | None -> ignore (instr builder) 
    in

    let cur_bb = L.insertion_block env.builder in
    let parent = L.block_parent cur_bb in
    let merge_bb = L.append_block env.context "merge" parent in
    let branch_instr = L.build_br merge_bb in

    let head x = match x with
      | u::y -> u(*print_endline (L.string_of_llvalue (L.value_of_block u))*)
      | [] -> raise (NotImplemented "Not supporting SType yet.")(*print_endline (L.string_of_llvalue (L.value_of_block merge_bb))*)
    in
    let rec tail x = match x with
      | [] -> raise (NotImplemented "Not supporting SType yet.")(*print_endline (L.string_of_llvalue (L.value_of_block merge_bb))*)
      | y::[] -> y
      | u::y -> tail y(*print_endline (L.string_of_llvalue (L.value_of_block u))*)
    in
    let _make_bb_block_list_help  (_env, (l_if_bb, last_env_expr, _l_if_bb, _l_if_c_bb ))   sbranch   = 
      
      let sexpr, sblock = sbranch in
      (*let () = head l_if_bb in*)
      let if_bb , if_lc_bb , _llval_expr = match l_if_bb with
        | u::y -> let env_expr, _llval_expr = _translate_value_sexpr _env sexpr in
                  (*let () = print_endline (L.string_of_llvalue _llval_expr) in*)
                  let if_lc_bb = L.insertion_block env_expr.builder in (*L.instr_parent _llval_expr in *) 
                  (*let () = L.delete_instruction _llval_expr in*)
                  let if_bb = L.insert_block env_expr.context "if" merge_bb in
                  let ifs_build =  L.builder_at_end env_expr.context if_bb in
                  let ifs_env = Codegen_env.env_with_builder env_expr ifs_build in
                  let env_after_stmts = _translate_sstmt ifs_env sblock in
                  let () = add_terminal env_after_stmts.builder branch_instr in

                  if_bb , if_lc_bb, _llval_expr

        | [] -> let if_c = L.insert_block _env.context "if_c" merge_bb in
                let branch_instr_ifc = L.build_br if_c in
                let () = add_terminal _env.builder branch_instr_ifc in

                let ifc_build = L.builder_at_end _env.context if_c in
                let ifc_env = Codegen_env.env_with_builder _env ifc_build in
                let env_expr, _llval_expr = _translate_value_sexpr ifc_env sexpr in
                (*let () = print_endline (L.string_of_llvalue _llval_expr) in*)
                let if_bb = L.insert_block _env.context "if" merge_bb in
                (*write stmts to if block*)
                let ifs_build =  L.builder_at_end _env.context if_bb in
                let ifs_env = Codegen_env.env_with_builder _env ifs_build in
                let env_after_stmts = _translate_sstmt ifs_env sblock in
                let () = add_terminal env_after_stmts.builder branch_instr in

                if_bb , if_c, _llval_expr
      in
      (*write sexpr in if_c*)
      let next_env    = if (List.length l_if_bb) <> (List.length sstmts) - 1 then 
                        let ifc_next = L.insert_block _env.context "if_c" merge_bb in
                        
                        let _build  = L.builder_at_end _env.context if_lc_bb in
                        let _ = L.build_cond_br (_llval_expr) (if_bb) ifc_next _build in

                        let ifc_build = L.builder_at_end _env.context ifc_next in
                        let ifc_next_env = Codegen_env.env_with_builder _env ifc_build in
                        ifc_next_env
                      else
                        _env
      in
      (next_env, ( l_if_bb@[if_bb] , [_llval_expr] @ last_env_expr, [if_bb] @ _l_if_bb, [if_lc_bb] @ _l_if_c_bb ))
    in
    let new_env, (l_if_bb,last_env_expr, _last_if_bb, if_lc_bb) = List.fold_left (sstmts)  ~f:_make_bb_block_list_help  ~init:(env, ([],[],[],[])) in 
    let else_bb = L.insert_block new_env.context "else" merge_bb in
    let _builder = L.builder_at_end new_env.context else_bb in
    let env_with_builder = Codegen_env.env_with_builder new_env _builder in
    let env_after_else = _translate_sstmt env_with_builder else_sstmt in
    let () = add_terminal env_after_else.builder branch_instr in

    let _build  = L.builder_at_end new_env.context (head if_lc_bb) in
    let _ = L.build_cond_br (head last_env_expr) (head _last_if_bb) else_bb _build in
    (*Position Builder after merge_bb *)
    let _builder = L.builder_at_end env_after_else.context merge_bb in                      
    let ret = Codegen_env.env_with_builder env_after_else _builder in
    ret

  | SExpr(sexpr) -> fst (_translate_sexpr env sexpr)
  | SEmit(sexpr) -> 
    let llfn_actor_emit_t = L.function_type void_t [| ptr_actor_t; ptr_void_t |] in
    let llfn_actor_emit = L.declare_function "actor_emit" llfn_actor_emit_t env.the_module in

    let llthis_entry = Codegen_env.find_variable "this" env in
    let env, llexpr = _translate_value_sexpr env sexpr in
    let lltmp = Codegen_env.llstore_alloc "emit_value" llexpr env in
    let llargument = L.build_bitcast lltmp ptr_void_t "" env.builder in

    let llthis = L.build_load llthis_entry.llvalue "this_val" env.builder in
    let _ = L.build_call llfn_actor_emit [| llthis; llargument |] "" env.builder in
    env

  | SReturn(sexpr) -> 
    match fst sexpr with
    | SUnitType ->
      let _ = L.build_ret_void env.builder in
      env
    | _ ->
      let env_after_expr, llexpr = _translate_value_sexpr env sexpr in

      let _ = L.build_ret llexpr env.builder in 
      env_after_expr
  in

  let _translate_sprogram (env: Codegen_env.t) (sprogram: Sast.sprogram) : (Codegen_env.t) =
    List.fold_left sprogram ~f:_translate_sdecl ~init:env
  in

  _translate_sprogram env sprogram
