open Sast;;
open Semantic_checker;;

open Printf;;
open Core;;
module Debug = Oscdebug;;


let parse_buffer ?print_error:(print_error=true) parse lexbuf = 
  if print_error then
  try
    parse Scanner.token lexbuf
  with Parsing.Parse_error ->
    begin
      let curr = lexbuf.Lexing.lex_curr_p in
      let line = curr.Lexing.pos_lnum in
      let cnum = curr.Lexing.pos_cnum - curr.Lexing.pos_bol in
      let str = (Bytes.to_string lexbuf.lex_buffer) in
      printf "\nParsing error line %d(%d):\n" line cnum;
      printf "%s\n" str;
      printf "%s^\n" (String.make (cnum) ' ');

      Debug.print_tokens_of_str str;

      raise Parsing.Parse_error
    end
  else
    parse Scanner.token lexbuf


let from_string ?print_error:(print_error=true) parse s = 
  let lexbuf = Lexing.from_string s in
  parse_buffer ~print_error:print_error parse lexbuf

let from_channel ?print_error:(print_error=true) parse channel = 
  let lexbuf = Lexing.from_channel channel in
  parse_buffer ~print_error:print_error parse lexbuf

let from_filepath ?print_error:(print_error=true) parse path = from_channel ~print_error:print_error parse (In_channel.create path)

let program_of_string ?print_error:(print_error=true) = from_string ~print_error:print_error Parser.program
let program_of_channel ?print_error:(print_error=true) = from_channel ~print_error:print_error Parser.program
let program_of_filepath ?print_error:(print_error=true) = from_filepath ~print_error:print_error Parser.program

let stmt_of_string ?print_error:(print_error=true) = from_string ~print_error:print_error Parser.stmt
let stmt_of_channel ?print_error:(print_error=true) = from_channel ~print_error:print_error Parser.stmt
let stmt_of_filepath ?print_error:(print_error=true) = from_filepath ~print_error:print_error Parser.stmt

let decl_of_string ?print_error:(print_error=true) = from_string ~print_error:print_error Parser.decl
let decl_of_channel ?print_error:(print_error=true) = from_channel ~print_error:print_error Parser.decl
let decl_of_filepath ?print_error:(print_error=true) = from_filepath ~print_error:print_error Parser.decl

let expr_of_string ?print_error:(print_error=true) = from_string ~print_error:print_error Parser.expr
let expr_of_channel ?print_error:(print_error=true) = from_channel ~print_error:print_error Parser.expr
let expr_of_filepath ?print_error:(print_error=true) = from_filepath ~print_error:print_error Parser.expr

let check_expr = Semantic_checker.check_expr
let check_decl = Semantic_checker.check_decl
let check_stmt = Semantic_checker.check_stmt
let check_program = Semantic_checker.check_program


exception LinkFailure


let compile_oscar (program: Ast.program) output_filepath =
  try 
    let _, sprogram = Semantic_checker.check_program ~scope:GlobalScope program in
    let codegen_env = Codegen.translate_sprogram sprogram in
    Llvm.print_module output_filepath codegen_env.the_module
  with e ->
    let () = match e with
      | Semantic_checker.TypeMismatch(sexpr, resolved_typename) -> printf "Type mismatch: expected %s but got %s (sexpr was %s)\n" (Fmtlib.code_of_typename resolved_typename) (Fmtlib.code_of_typename (fst sexpr)) (Fmtlib.code_of_sexpr sexpr)
      | Codegen.NotImplemented(s) -> printf "NotImplemented exception: %s\n" s
      | Codegen.ExpectedFunction(typename) -> printf "Expected this sexpr to be a function: %s\n" (Oscdebug.string_of_resolved_typename typename)
      | Semantic_checker_env.SymbolTable.NotFound(id) -> printf "Failed to find symbol: %s\n" id
      | _ -> ()
    in
    raise e


let compile_oscar_of_string (oscar_source: string) output_filepath =
  compile_oscar (program_of_string oscar_source) output_filepath


let compile_oscar_of_filepath (oscar_source_filepath: string) output_filepath =
  compile_oscar (program_of_filepath oscar_source_filepath) output_filepath


let link_llvm ?(libactors="./support/build/") llvm_filepath output_filepath : unit =
  let linker = "gcc" in
  let new_o_file = (String.sub llvm_filepath ~pos:0 ~len:((String.length llvm_filepath)-2)) ^ "o" in 
  let link_command = 
		(sprintf "llc -filetype=obj %s && %s %s -L%s -lm -lactors -o %s" 
			llvm_filepath 
			linker 
			new_o_file 
			libactors 
			output_filepath)
 in
  let link_result = Unix.system link_command in
  let result = match link_result with
    | Ok(_) -> ()
    | Error(e) -> raise LinkFailure(*raise (Codegen.NotImplemented((sprintf "LinkFailure: (c) %s\n" (E.to_string_hum (E.t_of_sexp (E.sexp_of_error e)))))) *)
  in
  result


let compile_and_link ?(libactors="./support/build/") filepath : unit =
  let () = printf "Compiling %s...\n" (filepath |> Color.blue) in
  let llvm_filepath = sprintf "%s.ll" (Core.Filename.chop_extension filepath) in
  let executable_filepath = (Core.Filename.basename (Core.Filename.chop_extension filepath)) in
  let () = printf "Outputting llvm to %s...\n" llvm_filepath in

  let _ = compile_oscar_of_filepath filepath llvm_filepath in

  (* Link LLVM bytecode into an executable *)
  let () = link_llvm ~libactors:libactors llvm_filepath executable_filepath in

  printf "Created ./%s.\n" (executable_filepath |> Color.green)
