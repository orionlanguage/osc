type identifier = string


type binop = Add | Sub | Mul | Div | Mod | Equal | Neq | Less | Leq | Greater | Geq | And | Or | Bind | Dot | MagicBind | MagicUnbind
type unop = Neg | Not


type typename = 
  | UnitType
  | Typename of identifier 
  | EmptyListTypename
  | ListTypename of typename
  | FunctionTypename of argument list * typename
  | ActorTypename of argument list * typename
  | StructTypename of identifier * argument list
and argument = identifier * typename


type branch = expr * stmt
and expr = 
  | UnitLiteral                                 (* ()        *)
  | IntLiteral of int                           (* 42        *)
  | FloatLiteral of float                       (* 42.0      *)
  | FileLiteral of int 
  | ByteLiteral of char                         (* 'a'       *)
  | BoolLiteral of bool                         (* true      *)
  | StringLiteral of string                     (* "42"      *)
  | ListIndex of expr * expr                    (* a[0]      *)
  | ListLiteral of expr list                    (* [42, 3]   *)
  | Identifier of identifier                    (* num       *)
  | FunctionCall of expr * expr list            (* add(3, 4) *)
  | FunctionLambda of typename * stmt           (* fn(i: int) -> int { return i + 3; } *)
  | ActorLambda of typename * stmt              (* actor(i: int) -> int { return i + 3; } *)
  | Binop of expr * binop * expr                (* 3 + 4 *)
  | Unop of unop * expr                         (* -3 *)
  | Assignment of expr * expr                   (* num = 42  *)
and stmt = 
  | Block of stmt list                (* { ... } *)
  | Expr of expr                      (* expr; *)
  | If of (branch list) * stmt          (* if expr { ... } else { ... } *)
  | While of expr * stmt              (* while expr { ... } *)
  | For of argument * expr * stmt     (* for i: int in [3, 4, 5] { ... } *)
  | Return of expr                    (* return expr; *)
  | Emit of expr                      (* emit expr; *)
  | Declaration of decl               (* let x: int = 9; *)
and decl = 
  | Variable of identifier * typename * expr option
  | Type of identifier * typename


type program = decl list
