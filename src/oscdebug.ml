open Core;;
open Ast;;
open Sast;;


let rec string_of_argument argument = 
  let (id, typ) = argument in sprintf "%s: %s" id (string_of_typename typ)
and string_of_typename typename = 
  let fmt_function typename arguments returntype = 
    let arguments_str = (String.concat ~sep:", " (List.map arguments ~f:string_of_argument)) in
    sprintf "%s(args: [%s], returns: %s)" typename arguments_str (string_of_typename returntype) in
  match typename with
  | FunctionTypename(arguments, returntype) -> fmt_function "FunctionTypename" arguments returntype
  | ActorTypename(arguments, returntype) -> fmt_function "ActorTypename" arguments returntype
  | ListTypename(s) -> sprintf "ListTypename(%s)" (string_of_typename s)
  | EmptyListTypename -> "EmptyListTypename"
  | Typename(s) -> s
  | UnitType -> "()"
  | StructTypename(id, properties) -> 
    let properties_str = (String.concat ~sep:", " (List.map properties ~f:string_of_argument)) in
    sprintf "%s(%s)" id properties_str

let rec string_of_resolved_argument argument = 
  let (id, typ) = argument in sprintf "%s: %s" id (string_of_resolved_typename typ)
and string_of_resolved_typename resolved_typename = 
  let fmt_function resolved_typename arguments returntype = 
    let arguments_str = (String.concat ~sep:", " (List.map arguments ~f:string_of_resolved_argument)) in
    sprintf "%s(%s) -> %s" resolved_typename arguments_str (string_of_resolved_typename returntype) in
  match resolved_typename with
  | SFunctionType(arguments, returntype) -> fmt_function "FunctionType" arguments returntype
  | SActorType(arguments, returntype) -> fmt_function "ActorType" arguments returntype
  | SEmptyListType -> "EmptyListType"
  | SListType(s) -> sprintf "ListType(%s)" (string_of_resolved_typename s)
  | SStructType(id, properties) -> "struct " ^ id
  | SUnitType -> "()"
  | SIntType -> "int"
  | SFileType -> "file"
  | SBoolType -> "bool"
  | SStringType -> "str"
  | SByteType -> "byte"
  | SFloatType -> "float"


let rec string_of_expr = function
| UnitLiteral -> "()"
| ListLiteral(l)   -> sprintf "[ %s ]" (String.concat ~sep:", " (List.map l ~f:string_of_expr))
| ByteLiteral(b)  -> sprintf "ByteLiteral(%c)" b
| FloatLiteral(f)  -> sprintf "FloatLiteral(%f)" f
| BoolLiteral(b)   -> sprintf "BoolLiteral(%B)" b
| StringLiteral(s) -> sprintf "StringLiteral(%s)" s
| FileLiteral(i)    -> sprintf "FileLiteral(%d)" i
| IntLiteral(i)    -> sprintf "IntLiteral(%d)" i
| Identifier(s)    -> sprintf "Identifier(%s)" s
| ListIndex(l, i)  -> sprintf "ListIndex(%s[%s])" (string_of_expr l) (string_of_expr i)
| FunctionCall(id,expr)  -> sprintf "(FunctionCall: %s(%s))" (string_of_expr id) (sprintf "%s" (String.concat ~sep:"," (List.map expr ~f:string_of_expr)))
| FunctionLambda(typename, stmt) -> sprintf "FunctionLambda(%s, %s)" (string_of_typename typename) (string_of_stmt stmt)
| ActorLambda(typename, stmt) -> sprintf "ActorLambda(%s, %s)" (string_of_typename typename) (string_of_stmt stmt)
| Unop(op, x) -> 
    let op_str = match op with
      | Neg -> "Neg"
      | Not -> "Not"
    in
    sprintf "%s(%s)" op_str (string_of_expr x)
  | Binop(l, op, r) -> 
    let op_str = match op with
      | MagicBind -> "MagicBind"
      | MagicUnbind -> "MagicUnbind"
      | Add -> "Add"
      | Sub -> "Sub"
      | Mul -> "Mul"
      | Div -> "Div"
      | Mod -> "Mod"
      | Ast.Equal -> "Equal"
      | Neq -> "Neq"
      | Ast.Less -> "Less"
      | Leq -> "Leq"
      | Dot -> "Dot"
      | Ast.Greater -> "Greater"
      | Geq -> "Geq"
      | And -> "And"
      | Or -> "Or"
      | Bind -> "Bind"
    in
    sprintf "%s(%s, %s)" op_str (string_of_expr l) (string_of_expr r)

  | Assignment(lvalue, expr) -> sprintf "Assignment(%s = %s)" (string_of_expr lvalue) (string_of_expr expr)
  (* | _ -> "UNKNOWN_EXPR" *)
and string_of_stmt = function
  | Expr(e) -> string_of_expr e
  | Return(e) -> sprintf "Return(%s)" (string_of_expr e)
  | Emit(e) -> sprintf "Emit(%s)" (string_of_expr e)
  | If(branches, else_stmt) -> let string_of_branch branch = 
                                 let (cond, block) = branch in 
                                 sprintf "Branch(%s, %s)" (string_of_expr cond) (string_of_stmt block) in
    sprintf "If([%s], %s)" (String.concat ~sep:", " (List.map branches ~f:string_of_branch)) (string_of_stmt else_stmt)

  | Block(stmts) -> if List.length stmts = 0 then " { <no statements> } " else sprintf "{\n%s\n}" (String.concat ~sep:";\n" (List.map stmts ~f:string_of_stmt))
  | While(expr, stmt) -> sprintf "While(%s, %s)" (string_of_expr expr) (string_of_stmt stmt)
  | For((id, _), l, stmt) -> sprintf "For(%s in %s, %s)" id (string_of_expr l) (string_of_stmt stmt)
  | Declaration(decl) -> sprintf "Declaration(%s)" (string_of_decl decl)
and string_of_decl = function
    | Type(id, typename) -> sprintf "Type(%s = %s)" id (string_of_typename typename)
    | Variable(id, typ, value) -> match value with
      | None -> sprintf "Variable(%s: %s)" id (string_of_typename typ)
      | Some v -> sprintf "Variable(%s: %s = %s)" id (string_of_typename typ) (string_of_expr v)
and string_of_program = function 
  | decls -> if List.length decls = 0 then " { <No Declarations> } " else sprintf "{\n%s\n}" (String.concat ~sep:";\n" (List.map decls ~f:string_of_decl))

let rec print_tokens lexbuf = 
  let token = Scanner.token lexbuf in
  let str = match token with
    | SEMI -> "SEMI"
    | COLON -> "COLON"
    | EQUALS -> "EQUALS" 

    | ARROW -> "ARROW"
    | COMMA -> "COMMA"
    | EOF -> "EOF"

    | FN -> "FN"
    | ACTOR -> "ACTOR"

    | LPAREN -> "LPAREN"
    | RPAREN -> "RPAREN"
    | LBRACE -> "LBRACE"
    | RBRACE -> "RBRACE"
    | LSQUARE -> "LSQUARE"
    | RSQUARE -> "RSQUARE"

    | GT -> "GT"
    | GEQ -> "GEQ"
    | LT -> "LT"
    | LEQ -> "LEQ"
    | EQ -> "EQ"
    | NEQ -> "NEQ"

    | PLUS -> "PLUS"
    | MINUS -> "MINUS"
    | TIMES -> "TIMES"
    | DIVIDE -> "DIVIDE"
    | MOD -> "MOD"

    | BIND -> "BIND"
    | DOT -> "DOT"

    | STRUCT -> "STRUCT"
    | TYPE -> "TYPE"


    | AND -> "AND"
    | OR -> "OR"
    | NOT -> "NOT"
    | WHILE -> "WHILE"
    | FOR -> "FOR"
    | IN -> "IN"
    | IF -> "IF"
    | ELSE -> "ELSE"
    | RETURN -> "RETURN"
    | EMIT -> "EMIT"
    | LET -> "LET"
    | INT_LITERAL(i) -> sprintf "INT_LITERAL(%d)" i
    | FILE_LITERAL(i) -> sprintf "FILE_LITERAL(%d)" i
    | STRING_LITERAL(s) -> sprintf "STRING_LITERAL(\"%s\")" s
    | BYTE_LITERAL(c) -> sprintf "BYTE_LITERAL(\"%c\")" c
    | FLOAT_LITERAL(f) -> sprintf "FLOAT_LITERAL(%f)" f
    | BOOL_LITERAL(b) -> sprintf "BOOL_LITERAL(%b)" b
    | IDENTIFIER(s) -> sprintf "IDENTIFIER(\"%s\")" s
    | _ -> "UNKNOWN"
  in
  match token with
  | EOF -> ()
  | _ -> printf "%s " str; print_tokens lexbuf



let print_tokens_of_str str =
  print_tokens (Lexing.from_string str)

(*                  | SFunctionCall of sexpr * sexpr list
                    | SActorCall of sexpr * sexpr list
                    | SFunctionLambda of sstmt
                    | SActorLambda of sstmt
                    | SBinop of sexpr * Ast.binop * sexpr
                    | SUnop of Ast.unop * sexpr
                  *)
let rec string_of_sexpr = function
  | _type, SUnitLiteral       -> sprintf "{ %s , %s }" (string_of_resolved_typename SUnitType) "SUnitLiteral"
  | _type, SIntLiteral(i)     -> sprintf "{ %s , S%s }" (string_of_resolved_typename _type) (string_of_expr (IntLiteral(i)))
  | _type, SFileLiteral(i)     -> sprintf "{ %s , S%s }" (string_of_resolved_typename _type) (string_of_expr (FileLiteral(i)))
  | _type, SFloatLiteral(f)   -> sprintf "{ %s , S%s }" (string_of_resolved_typename _type) (string_of_expr (FloatLiteral(f)))
  | _type, SBoolLiteral(b)    -> sprintf "{ %s , S%s }" (string_of_resolved_typename _type) (string_of_expr (BoolLiteral(b)))
  | _type, SStringLiteral(s)  -> sprintf "{ %s , S%s }" (string_of_resolved_typename _type) (string_of_expr (StringLiteral(s)))
  | _type, SFunctionLambda(s) -> sprintf "{SFunctionLambda(%s, %s)}" (string_of_resolved_typename _type) (string_of_sstmt s)
  | _type, SActorLambda(s)    -> sprintf "{SActorLambda(%s, %s)}" (string_of_resolved_typename _type) (string_of_sstmt s)
  | _type, SListLiteral(l)    -> let r_str = match l with 
                                              | [] -> sprintf "{ListTypename(%s , %s)}" (string_of_resolved_typename (_type)) (string_of_expr (ListLiteral([])))
                                              | x::y -> sprintf "{ %s , %s)}"  (string_of_resolved_typename (_type)) (String.concat ~sep:"; " (List.map l ~f:string_of_sexpr)) in 
                                r_str
  | _type, SUnop(op, x) -> 
    let op_str = match op with
      | Neg -> "Neg"
      | Not -> "Not"
    in
    sprintf "%s(%s)" op_str (string_of_sexpr x)
  | _type, SBinop(l, op, r) -> 
    let op_str = match op with
      | MagicBind -> "MagicBind"
      | MagicUnbind -> "MagicUnbind"
      | Add -> "Add"
      | Sub -> "Sub"
      | Mul -> "Mul"
      | Div -> "Div"
      | Mod -> "Mod"
      | Ast.Equal -> "Equal"
      | Neq -> "Neq"
      | Ast.Less -> "Less"
      | Leq -> "Leq"
      | Dot -> "Dot"
      | Ast.Greater -> "Greater"
      | Geq -> "Geq"
      | And -> "And"
      | Or -> "Or"
      | Bind -> "Bind"
    in
    sprintf "(%s) %s(%s, %s)" (string_of_resolved_typename _type) op_str (string_of_sexpr l) (string_of_sexpr r)
  | _type , SIdentifier(s) -> sprintf "%s SIdentifier(%s)" (string_of_resolved_typename _type) s
  | _type , SAssignment(lvalue, expr) -> sprintf "SAssignment(%s = %s)" (string_of_sexpr lvalue) (string_of_sexpr expr)
  | _type, SStructDereference(sexpr, i, s) -> sprintf "SStructDereference(%s, %i, %s)" (string_of_sexpr sexpr) i s
  | _type, SFunctionCall(fn, arguments) -> sprintf "%s, SFunctionCall(%s, %s)" (string_of_resolved_typename _type) (string_of_sexpr fn) (String.concat ~sep:", " (List.map arguments ~f:string_of_sexpr))
  | _ -> "UNKNOWN_SEXPR"
and string_of_sstmt = function
  | SExpr(e) -> string_of_sexpr e
  | SReturn(e) -> sprintf "{SReturn(%s)}" (string_of_sexpr e)
  | SEmit(e) -> sprintf "{SEmit(%s)}" (string_of_sexpr e)
  | SIf(branches, else_stmt) -> let string_of_branch branch = 
                                  let (cond, block) = branch in 
                                  sprintf "SBranch(%s, %s)" (string_of_sexpr cond) (string_of_sstmt block) in
                                sprintf "SIf([%s], %s)" (String.concat ~sep:", " (List.map branches ~f:string_of_branch)) (string_of_sstmt else_stmt)

  | SBlock(stmts) -> if List.length stmts = 0 then " { <no statements> } " else sprintf "{ %s }" (String.concat ~sep:"; " (List.map stmts ~f:string_of_sstmt))
  | SWhile(expr, stmt) -> sprintf "While(%s, %s)" (string_of_sexpr expr) (string_of_sstmt stmt)
  | SFor((id, _), l, stmt) -> sprintf "For(%s in %s, %s)" id (string_of_sexpr l) (string_of_sstmt stmt)
  | SDeclarations(sdecls) -> sprintf "SDeclaration(%s)" (String.concat ~sep:"; " (List.map sdecls ~f:string_of_sdecl))
  (* | _ -> "UNKNOWN_SSTMT" *)
and string_of_sdecl = function
  | SType(id, resolved_typename) -> sprintf "{SType(%s = %s)}" id (string_of_resolved_typename resolved_typename)
  | SVariable(id, typ, value) -> match value with
    | None -> sprintf "SVariable(%s: %s)" id (string_of_resolved_typename typ)
    | Some v -> sprintf "SVariable(%s: %s = %s)" id (string_of_resolved_typename typ) (string_of_sexpr v)
  (* | _ -> "UNKNOWN_SDECL" *)
and string_of_sprogram = function 
  | [] -> " { <No Declarations> } " 
  | sdecls -> sprintf "{ %s }" (String.concat ~sep:"; " (List.map sdecls ~f:string_of_sdecl))
