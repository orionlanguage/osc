open Printf;;


let _color code s =
  let reset = "\x1b[0m" in
  let color = sprintf "\x1b[%dm" code in
  color ^ s ^ reset
      
let bold = _color 1
let dim = _color 2

let black = _color 30
let red = _color 31
let green = _color 32
let yellow = _color 33
let blue = _color 34
let cyan = _color 36
