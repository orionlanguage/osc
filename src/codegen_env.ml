open Ast;;
module L = Llvm;;
open Printf;;
open Sast;;
open Symbol_table;;
open Semantic_checker_env;;


exception VariableNotFound of Ast.identifier
exception TypeNotFound of Ast.identifier


type llentry = {
  identifier: string;
  llvalue: L.llvalue;
  typename: resolved_typename;
  is_relocatable: bool;
}


type lltype = {
  identifier: string;
  lltype: L.lltype;
  typename: resolved_typename;
}


module LlentryMap = Hierarchical_map.Make(struct
    type t = llentry
    let key (v: llentry) = v.identifier
    let to_string (v: llentry) = v.identifier
  end)


module LltypeMap = Hierarchical_map.Make(struct
    type t = lltype
    let key (v: lltype) = v.identifier
    let to_string (v: lltype) = v.identifier
  end)


type scope_context = 
  | GlobalScope
  | FunctionScope of Sast.resolved_typename 
  | ActorScope of Sast.resolved_typename


(* All of the "context" that you need to know to codegen a given expr/stmt/decl.
 * *)
type t = {
  the_module: L.llmodule;
  context: L.llcontext;
  builder: L.llbuilder;
  variables: LlentryMap.t;
  types: LltypeMap.t;
  scope_context: scope_context;
}


let env_with_variables (env: t) (variables: LlentryMap.t): t =
  { env with  variables = variables }


let env_with_types (env: t) (types: LltypeMap.t): t =
  { env with types = types }


let env_with_builder (env: t) (builder: L.llbuilder) : t =
  { env with builder = builder }


let llstore_alloc (identifier: string) (llvalue: L.llvalue) (env: t) : L.llvalue =
  let lltype = L.type_of llvalue in
  let llalloca = L.build_alloca lltype identifier env.builder in
  let _ = L.build_store llvalue llalloca env.builder in
  llalloca


let define_variable ?is_relocatable:(is_relocatable=true) (identifier: string) (typename: resolved_typename) (llvalue: L.llvalue) (env: t) : t =
  let symbol = {is_relocatable=is_relocatable; identifier=identifier; llvalue=llvalue; typename=typename} in
  env_with_variables env (LlentryMap.add symbol env.variables)


let define_function (identifier: string) (typename: resolved_typename) (llfn: L.llvalue) (env: t) : t * L.llvalue =
  let ptr_identifier = identifier ^ "_ptr" in

  let llptr = match env.scope_context with
    | GlobalScope -> L.define_global ptr_identifier llfn env.the_module
    | _ -> 
      let l = llstore_alloc ptr_identifier llfn env in
      llstore_alloc identifier l env
  in

  define_variable ~is_relocatable:true identifier typename llptr env, llptr


let define_type (identifier: string) (typename: resolved_typename) (lltype: L.lltype) (env: t) : t =
  env_with_types env (LltypeMap.add {identifier=identifier; lltype=lltype; typename=typename} env.types)


let new_scope (env : t)  =
  env_with_variables env (LlentryMap.child env.variables)


let new_scope_context (scope_context: scope_context) (env : t) =
  {env with scope_context = scope_context}


let find_variable (identifier: string) (env: t) : llentry =
  try
    LlentryMap.find identifier env.variables
  with Not_found ->
    raise (VariableNotFound identifier)


let find_type (identifier: string) (env: t) : lltype =
  try
    LltypeMap.find identifier env.types
  with Not_found ->
    raise (TypeNotFound identifier)


let create () =
  let context    = L.global_context () in

  let the_module = L.create_module context "oscar" in

  { 
    the_module = the_module;
    context = context;
    builder = L.builder context; 
    variables = LlentryMap.empty;
    types = LltypeMap.empty;
    scope_context = GlobalScope
  }
