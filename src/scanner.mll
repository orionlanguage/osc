{ 
open Lexing
open Parser


exception SyntaxError of string


module StringMap = Map.Make(String)


let escapes =
  let escapes_list = [
        "\\n",  "\n";
        "\\e",  "\x1b";
        "\\r",  "\r";
        "\\t",  "\t";
        "\\\\", "\\";
        "\\\"" , "\"";
    ]
  in
  List.fold_left (fun map (k, v) -> StringMap.add k v map) StringMap.empty escapes_list



let next_line lexbuf =
  let pos = lexbuf.lex_curr_p in
  lexbuf.lex_curr_p <-
    { pos with pos_bol = lexbuf.lex_curr_pos;
               pos_lnum = pos.pos_lnum + 1
    }
}

let white = [' ' '\t']+
let newline = '\r' | '\n' | "\r\n"
let id = ['a'-'z' 'A'-'Z' '_'] ['a'-'z' 'A'-'Z' '0'-'9' '_']*

rule token =
    parse
        | white    { token lexbuf }
        | newline  { next_line lexbuf; token lexbuf }
        | "//"([^ '\n' ]+) { token lexbuf }
        | "let" { LET }
        | "fn" { FN }
        | "actor" { ACTOR }
        | '=' { EQUALS }
        | "->" { ARROW }
        | ',' { COMMA }
        | ':' { COLON }
        | ';' { SEMI }
        | '(' { LPAREN }
        | ')' { RPAREN }
        | '[' { LSQUARE }
        | ']' { RSQUARE }
        | '{' { LBRACE }
        | '}' { RBRACE }

        | "+" { PLUS }
        | "-" { MINUS }
        | "*" { TIMES }
        | "/" { DIVIDE }
        | "%" { MOD }

        | "/*" { scan_comment lexbuf }

        | ">" { GT }
        | ">=" { GEQ }
        | "<" { LT }
        | "<=" { LEQ }
        | "==" { EQ }
        | "!=" { NEQ }

        | "&&" { AND }
        | "||" { OR }
        | "!" { NOT }

        | "=>" { BIND }
        | "+=" { PLUSEQ }
        | "-=" { MINUSEQ }

        | "." { DOT }

        | "true" { BOOL_LITERAL(true) }
        | "false" { BOOL_LITERAL(false) }

        | "return" { RETURN }
        | "emit" { EMIT }
        | "if" { IF }
        | "else" { ELSE }
        | "elif" { ELIF }
        | "while" { WHILE }
        | "for" { FOR }
        | "in" { IN }
        | "struct" { STRUCT }
        | "type" { TYPE }
        | '"' { STRING_LITERAL (scan_string "" lexbuf) } 
        | ''' { BYTE_LITERAL (scan_byte "" lexbuf) } 
        | id as identifier { IDENTIFIER(identifier) }
        | ['0'-'9']+ as integer { INT_LITERAL(int_of_string integer) }
        | ['0'-'9']+'.'['0'-'9']* as f { FLOAT_LITERAL(float_of_string f) }
        | ['0'-'9']('.'['0'-'9']?)+ as f { raise (SyntaxError (Printf.sprintf "Invalid float literal '%s'" f)) }
        | eof { EOF }

(* idea taken from https://medium.com/@huund/recipes-for-ocamllex-bb4efa0afe53 *)
and scan_string s = parse 
        | [^'"' '\n' '\\']+ { scan_string (s ^ Lexing.lexeme lexbuf) lexbuf }
        | '\\' _ as escape  { 
            match StringMap.find_opt escape escapes with
            | Some(c) -> scan_string (s ^ c) lexbuf 
            | None -> raise (SyntaxError "Invalid string escape")
          }
        | '"'       { s }
        | '\n'      { raise (SyntaxError "Newline inside of a string literal")}
        | eof       { raise (SyntaxError "EOF inside of a string literal") }
        | _         { raise (SyntaxError "Invalid character inside a string literal") }

and scan_byte s = parse 
        | [^''' '\n' '\\']+ { scan_byte (s ^ Lexing.lexeme lexbuf) lexbuf }
        | '\\' _ as escape  { 
            match StringMap.find_opt escape escapes with
            | Some(c) -> scan_byte (s ^ c) lexbuf 
            | None -> raise (SyntaxError "Invalid byte escape")
          }
        | '''      { String.get s 0 }
        | '\n'      { raise (SyntaxError "Newline inside of a byte literal")}
        | eof       { raise (SyntaxError "EOF inside of a byte literal") }
        | _         { raise (SyntaxError "Invalid character inside a byte literal") }

and scan_comment = parse 
        | _         { scan_comment lexbuf }
        | "*/"       { token lexbuf}
        | eof       { raise (SyntaxError "EOF inside of a comment") }
