open Ast;;
open Sast;;
open Symbol_table;;
open Semantic_checker_env;;

open Printf;;
open Core;;
module Debug = Oscdebug;;


(* Should be thrown if the functionality is not yet implemented in the semantic checker. *)
exception NotImplemented

exception SemanticCheckerError of string

(* Should be thrown if actor tries to return *)
exception CantReturnFromActor
exception CantEmitFromFunction

exception ReturnTypeMismatch of sexpr * resolved_typename
exception UndeclaredFunction of sexpr

(* Type mismatch in expr, first and second typenames do not match *)
exception TypeMismatch of sexpr * resolved_typename

(* Thrown when attempting aritmetic operations on nun numeric numbers*)
exception ExpectedNumeric
exception NegationError
(* Thrown when attempting to test equality of non equatable types*)
exception ExpectedEquatable 
(* Thrown when attempting to compare non comparable types*)
exception ExpectedComparable 
exception ExpectedStructure
exception ExpectedStructureMember of expr
exception ExpectedBlock of sstmt
exception ExpectedBooleanExpression
exception NonIterable of sexpr
(* A bool was expected for this expr, but we got something else (e.g. If, While loops) *)
exception ExpectedBoolean of sexpr

exception CantIndexEmptyList
exception ExpectedList
exception ExpectedIntIndex

(* Expected us to be in a function, but we are not (see env.scope_context) *)
exception ExpectedFunction
exception ExpectedStatement

exception InvalidArguments of string


(* Expected us to be in an actor, but we are not (see env.scope_context) *)
exception ExpectedActor

exception NotADefinedType
exception NoStmtsAllowedAfterReturnStmt

(* TODO: format() is our only variadic; should not be assignable to/from variables *)


(* Checks that the provided sexpr has the expected type, and if not, raises
 * an appropriate TypeMismatch *)
let check_sexpr_matches_resolved_type (sexpr: sexpr) (resolved_typename: resolved_typename) (env: Semantic_checker_env.t) =
  let sexpr_type, _ = sexpr in
  if resolved_typename <> sexpr_type 
  then 
    match resolved_typename, sexpr_type with
    | SListType(_), SEmptyListType -> ()  (* special case: allow empty lists to be used for any list type *)
    | _ -> raise (TypeMismatch(sexpr, resolved_typename)) 
  else ()


let check_sexpr_matches_type (sexpr: sexpr) (typename: typename) (env: Semantic_checker_env.t) =
  let resolved_typename = Semantic_checker_env.resolve_typename typename env in
  check_sexpr_matches_resolved_type sexpr resolved_typename env


let generate_constructor (struct_t: resolved_typename) : sdecl =
  let struct_identifier, members = match struct_t with
    | SStructType(struct_identifier, members) -> struct_identifier, members
    | _ -> raise ExpectedStructure
  in

  let constructor_name = struct_identifier in

  let constructor_t = SFunctionType(members, struct_t) in

  let struct_variable_name = String.lowercase struct_identifier in
  let struct_variable = struct_t, SIdentifier(struct_variable_name) in

  (* declare the struct variable; let m: Message = <memory>; *)
  let sdecl_struct = SDeclarations([
    SVariable(struct_variable_name, struct_t, Some (struct_t, SAlloc))
  ]) in

  (* assign members using constructor arguments *)
  let assignment_stmt index (id, t) =
    let member = (t, SStructDereference(struct_variable, index, id)) in
    let argument = (t, SIdentifier(id)) in
    SExpr(t, SAssignment(member, argument))
  in

  let assignment_sstmts = List.mapi members ~f:assignment_stmt in

  let constructor = constructor_t, SFunctionLambda(SBlock(
    [sdecl_struct] @ 
    assignment_sstmts @ 
    [SReturn(struct_variable)]
  )) in

  SVariable(constructor_name, constructor_t, Some constructor)


let copy_constructor_name (struct_t: resolved_typename) : string = 
  let struct_identifier = match struct_t with
    | SStructType(struct_identifier, _) -> struct_identifier
    | _ -> raise ExpectedStructure
  in
  struct_identifier ^ "_copy"


let copy_constructor_type (struct_t: resolved_typename) : resolved_typename = 
  let () = match struct_t with
    | SStructType(_, _) -> ()
    | _ -> raise ExpectedStructure
  in
  SFunctionType([("original", struct_t)], struct_t)


let generate_copy_constructor (struct_t: resolved_typename) : sdecl =
  let struct_identifier, members = match struct_t with
    | SStructType(struct_identifier, members) -> struct_identifier, members
    | _ -> raise ExpectedStructure
  in

  let copy_constructor_name = copy_constructor_name struct_t in

  let original_name = "original" in
  let copy_name = "copy" in
  let copy_constructor_t = copy_constructor_type struct_t in

  let copy_variable = struct_t, SIdentifier(copy_name) in
  let original_variable = struct_t, SIdentifier(original_name) in

  (* declare the struct variable; let m: Message = <memory>; *)
  let sdecl_struct = SDeclarations([
    SVariable(copy_name, struct_t, Some (struct_t, SAlloc))
  ]) in

  (* assign members by dereferencing each member of the passed in *)
  let assignment_stmt index (id, t) =
    let member = (t, SStructDereference(copy_variable, index, id)) in
    let argument = (t, SStructDereference(original_variable, index, id)) in
    SExpr(t, SAssignment(member, argument))
  in

  let assignment_sstmts = List.mapi members ~f:assignment_stmt in

  let copy_constructor = copy_constructor_t, SFunctionLambda(SBlock(
    [sdecl_struct] @ 
    assignment_sstmts @ 
    [SReturn(copy_variable)]
  )) in

  SVariable(copy_constructor_name, copy_constructor_t, Some copy_constructor)


let rec _check_typename (env: Semantic_checker_env.t) (typename: Ast.typename) : (Semantic_checker_env.t * typename) = 
  let check_arguments (env: Semantic_checker_env.t) (arguments: Ast.argument list) : unit =
    List.iter arguments ~f:(fun (_, arg_type) -> ignore (_check_typename env arg_type))
  in
  let check_function (env: Semantic_checker_env.t) (arguments: Ast.argument list) (return_typename: Ast.typename) =
    let () = check_arguments env arguments in
    let _ = _check_typename env return_typename in ()
  in
  match typename with
  | UnitType -> env, typename
  | Typename(id) -> 
    let _ = try SymbolTable.find id env.type_table
            with SymbolTable.NotFound(s) -> raise (SymbolTable.UndeclaredIdentifier id) in
    env, Typename(id)
  | ListTypename(element_typename) -> 
    let _ = _check_typename env element_typename in 
    env, ListTypename(element_typename)
  | FunctionTypename(arguments, return_typename) ->
    let () = check_function env arguments return_typename in
    env, FunctionTypename(arguments, return_typename)
  | ActorTypename(arguments, return_typename) ->
    let () = check_function env arguments return_typename in 
    env, ActorTypename(arguments, return_typename)
  | StructTypename(id, arguments) ->
    let () = check_arguments env arguments in 
     (* TODO: check struct name not already used in this scope *)
    env, StructTypename(id, arguments)
  | _ -> raise NotImplemented

let rec _check_expr (env: Semantic_checker_env.t) (expr: Ast.expr) : (Semantic_checker_env.t * sexpr) = match expr with
| UnitLiteral -> env, (SUnitType, SUnitLiteral)
| IntLiteral(i) -> env, (SIntType, SIntLiteral(i))
| FileLiteral(i) -> env, (SFileType, SFileLiteral(i))
| FloatLiteral(f) -> env, (SFloatType, SFloatLiteral(f))
| ByteLiteral(b) -> env, (SByteType, SByteLiteral(b))
| BoolLiteral(b) -> env, (SBoolType, SBoolLiteral(b))
| StringLiteral(s) -> env, (SStringType, SStringLiteral(s))

| Identifier(id) -> 
  begin
    try
      let symbol = SymbolTable.find id env.symbol_table in
      env, (symbol.typename, SIdentifier(symbol.identifier))
    with SymbolTable.NotFound(s) -> raise (SymbolTable.UndeclaredIdentifier id)
  end

| FunctionCall(fn_expr, argument_exprs) ->
  (* TODO: actor call should be a stmt, not an expr! It has no value that makes sense. *)
  let env, fn_sexpr = _check_expr env fn_expr in
  let fn_identifier = match fn_expr with
  | Identifier(id) -> id
  | _ -> "<unkown>"
  in

  let check_arguments formals arguments = 
    let check_argument (formal_identifier, formal_typename) argument =
      let _, sexpr = _check_expr env argument in
      let typename, _ = sexpr in
      if typename <> formal_typename 
      then raise (InvalidArguments (sprintf "Wrong argument type to '%s'" fn_identifier))
      else sexpr
    in

    match fn_expr with
    | Identifier("len") ->
      let sarguments = List.map arguments ~f:(fun a -> snd (_check_expr env a)) in
      let _ = match sarguments with
      | [SListType(_), _] -> true
      | [SStringType, _] -> true
      | _ -> raise (Failure "len() expects a list as its only argument.")
      in
      sarguments

    | Identifier("format") ->
      let _ = check_argument (List.hd_exn formals) (List.hd_exn arguments) in
      List.map arguments ~f:(fun a -> snd (_check_expr env a))

    | Identifier("remove") ->
      let sarguments = List.map arguments ~f:(fun a -> snd (_check_expr env a)) in

      let list_argument, index_argument = match sarguments with
        | [list_argument; index_argument] -> list_argument, index_argument
        | _ -> raise (Failure "remove() expects two arguments.")
      in

      let () = 
        if (fst index_argument) <> SIntType 
        then raise (InvalidArguments "Wrong argument type to remove(): expected integer as second argument.") 
        else ()
      in

      let _ = match List.hd_exn sarguments with
      | SListType(_), _ -> true
      | _ -> raise (Failure "remove() expects a list as its first argument.")
      in

      sarguments

    | Identifier("append") ->
      let sarguments = List.map arguments ~f:(fun a -> snd (_check_expr env a)) in

      let list_argument, element_argument = match sarguments with
        | [list_argument; element_argument] -> list_argument, element_argument
        | _ -> raise (Failure "append() expects two arguments.")
      in

      let element_typename = match (fst list_argument) with
      | SStringType -> SByteType
      | SListType(element_typename) -> element_typename
      | SEmptyListType -> (raise NotImplemented)
      | _ -> raise (Failure "append() expects a list as its first argument.")
      in

      if element_typename <> (fst element_argument) then raise (InvalidArguments (sprintf "Wrong argument type to '%s'" fn_identifier)) else sarguments

    | _ ->
      begin
        let () = if List.length formals <> List.length arguments 
          then raise (InvalidArguments (sprintf "Wrong number of arguments to '%s': %d, expected %d." fn_identifier (List.length arguments) (List.length formals)))
          else ()
        in

        List.map2_exn formals arguments ~f:check_argument
      end
  in

  let formals, formal_return_type = match fst fn_sexpr with
  | SFunctionType(formals, formal_return_type) -> formals, formal_return_type
  | SActorType(formals, formal_return_type) -> formals, formal_return_type
  | _ -> 
    let _ = (printf "SFunctionCall expected %s to be a function" (Fmtlib.code_of_sexpr fn_sexpr)) in
    raise ExpectedFunction
  in

  let rec tail x = match x with 
                | [y] -> y
                | z::y -> tail y
                | _ -> raise (NotImplemented)
  in
  let head x = match x with 
                | z::y -> z
                | _ -> raise (NotImplemented)
  in
  let argument_sexprs = check_arguments formals argument_exprs in
  let call_sexpr_node = match fst fn_sexpr with
  | SFunctionType(_, _) -> SFunctionCall(fn_sexpr, argument_sexprs)
  | SActorType(_, _) -> SActorCall(fn_sexpr, argument_sexprs)
  | _ -> raise ExpectedFunction
  in

  env, (formal_return_type, call_sexpr_node)

| ActorLambda(t,s) -> 
  let args_with_this, emit_type = match t with
    | ActorTypename(arg_list, emit_type) -> arg_list, emit_type
    | _ -> raise ExpectedActor
  in

  let t = ActorTypename(args_with_this, emit_type) in

  let resolved_t = Semantic_checker_env.resolve_typename t env in

  let env = List.fold_left ~f:Semantic_checker_env.add_argument ~init:(env) args_with_this in 
  let n_env = Semantic_checker_env.new_scope (Semantic_checker_env.new_scope_context (ActorScope t) env) in
  let _, sblock = _check_stmt ~new_scope:false n_env s in
  env, (resolved_t, SActorLambda(sblock))

| FunctionLambda(t, s) -> 
  let arg_l = match t with
    | FunctionTypename(arg_list, _) -> arg_list
    | _ -> raise ExpectedFunction
  in

  let resolved_t = Semantic_checker_env.resolve_typename t env in
  let env = List.fold_left ~f:Semantic_checker_env.add_argument ~init:(Semantic_checker_env.new_scope env) arg_l in 
  let n_env = Semantic_checker_env.new_scope_context (FunctionScope t) env in
  let _, sblock = _check_stmt ~new_scope:false n_env s in
  (*Check to the end of the list for return *)
  env, (resolved_t, SFunctionLambda(sblock))

| ListLiteral(l) -> 
  let _check_lists_helper (env, sexprs) expr =
    let new_env, sexpr = _check_expr env expr in
    new_env, (sexprs @ [sexpr])
  in

  let env_after_exprs, sexprs = List.fold_left l ~f:_check_lists_helper ~init:(env, []) in 

  begin
    match List.hd sexprs with
    | None -> env, (SEmptyListType, SListLiteral([]))
    | Some(element_typename, _) -> 
      let () = List.iter ~f:(fun s -> check_sexpr_matches_resolved_type s element_typename env) sexprs in
      env_after_exprs, (SListType(element_typename), SListLiteral(sexprs))
  end

| ListIndex(list_expr, index_expr) ->
  let env, list_sexpr = _check_expr env list_expr in
  let env, index_sexpr = _check_expr env index_expr in
  let (list_typename: Sast.resolved_typename), _ = list_sexpr in

  let element_typename = match list_typename with
  | SListType(element_typename) -> element_typename
  | SStringType -> SByteType
  | _ -> raise ExpectedList
  in

  let index_typename, _ = index_sexpr in
  let () = if index_typename <> SIntType then raise ExpectedIntIndex else () in 

  env, (element_typename, SListIndex(list_sexpr, index_sexpr))

| Binop(e1, op, e2) ->
  begin
    match op with 
    | Dot -> 
      let env_after_lhs, lsexpr = _check_expr env e1 in 

      let lsexpr_type, _ = lsexpr in

      let expected_member_name = match e2 with
        | Identifier(name) -> name
        | _ -> raise (ExpectedStructureMember e2)
      in

      let members = match lsexpr_type with
      | SStructType(_, members) -> members
      | _ -> raise ExpectedStructure
      in

      (* Check that a struct member with name member_name exists, and get its index *)
      let matching_member_index_opt = List.findi members ~f:(fun i (s, t) -> s = expected_member_name) in
      let member_index, (member_name, member_type) = match matching_member_index_opt with
      | Some(index, member) -> index, member
      | None -> raise (ExpectedStructureMember e2)
      in

      env, (member_type, SStructDereference(lsexpr, member_index, member_name))

    | _ ->
      let is_addable n = match n with
        | SIntType    -> true
        | SFloatType  -> true
        | SStringType -> true
        | SListType(s) -> true
        | _ -> false
      in
      let is_numeric n = match n with
        | SIntType    -> true
        | SByteType   -> true
        | SFloatType  -> true
        | _ -> false
      in 
      let is_equatable n = match n with
        | SIntType    -> true
        | SFloatType  -> true
        | SByteType   -> true
        | SBoolType   -> true
        | SStringType -> true
        | SFileType   -> true
        | SUnitType   -> true
        | _ -> false
      in 
      let is_ListType n = match n with
        | SListType(s) -> true
        | _ -> false
        in
        let get_List_Item_Type n = match n with
        | SListType(s) -> s
        | _ -> raise (ExpectedList)
      in
      let is_bool =   function | SBoolType  -> true | _ -> false in
      let is_actor =  function | SActorType(_, _) -> true | _ -> false in
      let env1, (se1) = _check_expr env e1 in 
      let env2, (se2) = _check_expr env1 e2 in 
      let se1_type, _ = se1 in
      let se2_type, _ = se2 in
      let _env, _sBinop = match op with 
        | Bind | MagicBind | MagicUnbind -> 
          let se1_arguments = match se1_type with
            | SActorType(arguments, _) -> arguments
            | _ -> raise ExpectedActor
          in
          let se2_return_type = match se2_type with
            | SActorType(_, return_type) -> return_type
            | _ -> raise ExpectedActor
          in
          let actor_type = SActorType(se1_arguments, se2_return_type) in
          env2, (actor_type, SBinop(se1, op, se2))

        | _ ->
          begin
            let check_type_match =
              let () = (check_sexpr_matches_resolved_type se1 se1_type env) in
              let () = (check_sexpr_matches_resolved_type se2 se1_type env) in () 
            in
            match op with 
              | Add -> if is_addable se1_type then env, (se1_type, SBinop(se1, op, se2)) else raise (ExpectedNumeric)
              | Sub -> if is_numeric se1_type then env, (se1_type, SBinop(se1, op, se2)) else raise (ExpectedNumeric)
              | Mul -> if is_numeric se1_type then env, (se1_type, SBinop(se1, op, se2)) else raise (ExpectedNumeric)
              | Div -> if is_numeric se1_type then env, (se1_type, SBinop(se1, op, se2)) else raise (ExpectedNumeric)
              | Mod -> if is_numeric se1_type then env, (se1_type, SBinop(se1, op, se2)) else raise (ExpectedNumeric)
              | Ast.Equal -> if is_equatable se1_type then env, (SBoolType, SBinop(se1, op, se2)) else raise (ExpectedEquatable)
              | Neq ->  if is_equatable se1_type then env, (SBoolType, SBinop(se1, op, se2)) else raise (ExpectedEquatable)
              | Ast.Less ->  if is_numeric se1_type then env2, (SBoolType, SBinop(se1, op, se2)) else raise (ExpectedComparable)
              | Leq ->  if is_numeric se1_type then env2, (SBoolType, SBinop(se1, op, se2)) else raise (ExpectedComparable)
              | Dot ->  raise (SemanticCheckerError "Shouldn't have gotten here!")
              | Ast.Greater -> if is_numeric se1_type then env2, (SBoolType, SBinop(se1, op, se2)) else raise (ExpectedComparable)
              | Geq ->  if is_numeric se1_type then env2, (SBoolType, SBinop(se1, op, se2)) else raise (ExpectedComparable)
              | And ->  if is_bool se1_type then env2, (se1_type, SBinop(se1, op, se2)) else raise (ExpectedBoolean(se1))
              | Or ->  if is_bool se1_type then env2, (se1_type, SBinop(se1, op, se2)) else raise (ExpectedBoolean(se1))
              | _ -> raise (Failure "Shouldn't have gotten here.")
          end
      in _env, _sBinop
  end

| Unop(op, e1) -> 
  let is_numeric n = match n with
    | SIntType   -> true
    | SFloatType -> true
    | _ -> false
  in 
  let env1, se1 = _check_expr env e1 in
  let se1_type , _sex = se1  in 
  let res = 
  match op with 
    | Neg -> if is_numeric se1_type then env1, (se1_type,  SUnop(op, se1)) else raise (NegationError)
    | Not ->  let () = if se1_type <> SBoolType then raise (ExpectedBoolean(se1_type,_sex)) else () in
              env1, (se1_type, SUnop(op, se1)) 
  in res

| Assignment(lexpr, rexpr) ->    
  let env1, slexpr = _check_expr env lexpr in
  let _st1 , _ = slexpr in
  let env2, srexpr = _check_expr env1 rexpr in    
  let () = check_sexpr_matches_resolved_type srexpr _st1 env in 
  env2, (_st1, SAssignment(slexpr, srexpr)) 


and _check_stmt ?new_scope:(new_scope=true) (env: Semantic_checker_env.t) (stmt: Ast.stmt) : (Semantic_checker_env.t * sstmt) = match stmt with
| Return(expr) -> 
  let env_after_expr, sexpr = _check_expr env expr in 

  let expected_return_type = match env.scope_context with
  | FunctionScope(FunctionTypename(_, return_type)) -> return_type
  | ActorScope(_) -> raise CantReturnFromActor
  | _ -> raise ExpectedFunction
  in

  let () = check_sexpr_matches_type sexpr expected_return_type env_after_expr in

  env_after_expr, SReturn(sexpr)

| Emit(expr) -> 
  let env_after_expr, sexpr = _check_expr env expr in 

  let expected_emit_type = match env.scope_context with
  | ActorScope(ActorTypename(_, emit_type)) -> emit_type
  | FunctionScope(_) -> raise CantEmitFromFunction
  | _ -> raise ExpectedFunction
  in

  let () = check_sexpr_matches_type sexpr expected_emit_type env_after_expr in

  env_after_expr, SEmit(sexpr)

| Declaration(decl) -> 
  let env_after_decl, sdecl = _check_decl env decl in env_after_decl, SDeclarations(sdecl)

| Expr(expr) -> let env_after_expr, sexpr = _check_expr env expr in env_after_expr, SExpr(sexpr)

| For(arg, expr, stmt) ->  (* - argument * expr * stmt - *)
  let child_env = Semantic_checker_env.new_scope env in
  let id, argtype = arg in
  let resolved_argtype = Semantic_checker_env.resolve_typename argtype env in

  let env_with_arg, val_stmt =  
    let sdecl = Variable(id, argtype, None) in
    _check_decl child_env sdecl
  in
  let env1, sexpr = _check_expr env_with_arg expr in
  let _ , _type = arg in 
  let stype , _ = sexpr in
  let env2 , sstmt = _check_stmt env1 stmt in 

  let () = match stype with 
  | SListType(t) -> if resolved_argtype <> t then raise (TypeMismatch(sexpr, resolved_argtype)) else ()
  | SStringType -> if resolved_argtype <> SByteType then raise (TypeMismatch(sexpr, resolved_argtype)) else ()
  | _ -> raise (NonIterable sexpr)
  in 
  env2, SBlock([SDeclarations(val_stmt) ; SFor((id, resolved_argtype), sexpr, sstmt)])

| While(expr, stmt) -> 
  let env_after_expr, sexpr = _check_expr env expr in
  let () = if fst sexpr <> SBoolType then raise (ExpectedBoolean sexpr) else () in
  let env_after_stmt, sstmt = _check_stmt env_after_expr stmt in
  env_after_stmt, SWhile(sexpr, sstmt)

| Block(stmts) ->
  let _check_stmts_helper (_env, _stmts) stmt =
    let new_env, sstmt = _check_stmt _env stmt in
    let () = match sstmt with
      | SReturn(sexpr) -> if (List.length stmts) != (List.length _stmts)+1 then (raise NoStmtsAllowedAfterReturnStmt) else () 
      | _ -> ()
    in
    new_env, (_stmts @ [sstmt])
  in

  let child_env = if new_scope then Semantic_checker_env.new_scope env else  env in
  let _, sstmts = 
    List.fold_left stmts 
      ~f:_check_stmts_helper 
      ~init:(child_env, [])
  in
  env, SBlock(sstmts)

| If(st_list, else_stmt) -> 
  let () = if List.length st_list = 0 then raise ExpectedBooleanExpression else () in
  let _env , _sstmt = _check_stmt env else_stmt in 
  let check_stmt_list_helper (stmts) (_expr,stmt) = 
    let _env1, sexpr = _check_expr env _expr in
    let _env2, _sstmt = _check_stmt env stmt in 
    let () = match _sstmt with
              | SBlock(_) -> ()
              | _ -> raise (ExpectedBlock _sstmt)
    in

    let _type, _ = sexpr in
    let () = if fst sexpr <> SBoolType then raise (ExpectedBoolean(sexpr)) else () in
    (stmts @ [(sexpr, _sstmt)])
  in
  let sstmts = 
    List.fold_left st_list 
      ~f:check_stmt_list_helper 
      ~init:([]) 
  in 
  env, SIf(sstmts, _sstmt)


and _check_decl (env: Semantic_checker_env.t) (decl: Ast.decl) : (Semantic_checker_env.t * sdecl list) = match decl with
| Variable(id, typ, expr_opt) -> 
  let env_after_typename, _ = _check_typename env typ in
  let resolved_typename = Semantic_checker_env.resolve_typename typ env in
  let sexpr_opt = Core.Option.map expr_opt ~f:(fun e -> snd (_check_expr env_after_typename e)) in

  let sexpr_opt = match resolved_typename, sexpr_opt with
    (* Special case: if we have x: [int] = [], tag the [] with [int], so codegen knows element size *)
    | SListType(element_type), (Some (SEmptyListType, SListLiteral([]))) -> Some (SListType(element_type), SListLiteral([]))
    | _, _ -> 
      let _ = Core.Option.map sexpr_opt ~f:(fun s -> check_sexpr_matches_type s typ env) in 
      sexpr_opt
  in

  (*let () = print_endline "SVariable Check Passed"  in*)
  let resolved_type = Semantic_checker_env.resolve_typename typ env in

  let symbol = {identifier=id; typename=resolved_type; defined=Core.Option.is_some sexpr_opt} in
  let env = Semantic_checker_env.add_symbol symbol env in

  let env, result = match env.scope_context with
  | GlobalScope -> 
    begin
      match sexpr_opt with
      | None -> env, SVariable(id, resolved_type, None)
      | Some(sexpr_type, sexpr_node) -> 
        match sexpr_type with
        | SFunctionType(_, _) -> env, SVariable(id, resolved_type, Some (sexpr_type, sexpr_node))
        | _ -> Semantic_checker_env.add_initialization id (sexpr_type, sexpr_node) env, SVariable(id, resolved_type, None)
    end
  | _ -> env, SVariable(id, resolved_type, sexpr_opt)
  in
  (*let () = print_endline "leaving SVariable Check:--------------------"  in*)
  env, [result]

| Type(id, typ) ->
  let env_after_typename, typename = _check_typename env typ in
  let resolved_typename = Semantic_checker_env.resolve_typename typ env in
  let result = SType(id, resolved_typename) in
  let env_after_type = Semantic_checker_env.add_type {identifier=id; typename=resolved_typename; defined=true} env_after_typename in

  let result_env, sdecls = match resolved_typename with
    | SStructType(identifier, resolved_arguments) ->
      (* Now add constructor, same name as struct, to the symbol table *)
      let constructor_typename = SFunctionType(resolved_arguments, resolved_typename) in
      let constructor_symbol = {identifier=id; typename=constructor_typename; defined=true} in

      let env_after_constructor_symbol = Semantic_checker_env.add_symbol constructor_symbol env_after_type in
      env_after_constructor_symbol, [result;
                                     generate_constructor resolved_typename;
                                     generate_copy_constructor resolved_typename]
    | _ -> env_after_type, [result]
  in


  result_env, sdecls


let _check_program (env: Semantic_checker_env.t) (ast: Ast.program) : (Semantic_checker_env.t * Sast.sprogram) =
  let _check_program_helper (env, sprogram) decl =
    let new_env, sdecls = _check_decl env decl in
    new_env, (sprogram @ sdecls)
  in

  let env, sprogram = List.fold_left ast ~f:_check_program_helper ~init:(env, []) in

  let initialize_name = "__initialize__" in

  let main_fn_t = SFunctionType([], SIntType) in
  let void_fn_t = SFunctionType([], SUnitType) in

  let sprogram, env = match env.scope_context with
  | GlobalScope ->
    let stdfiles = ["stdout", 0; "stdin", 1; "stderr", 2] in

    let add_stdfile (sprogram, env) (filename, fd) =
      let file_make = (SFunctionType(["fd", SIntType], SFileType), SIdentifier("file_make")) in
      let variable = SVariable(filename, SFileType, None) in
      variable::sprogram, Semantic_checker_env.add_initialization filename (SFileType, SFunctionCall(file_make, [SIntType, SIntLiteral(fd)])) env 
    in

    List.fold ~f:add_stdfile ~init:(sprogram, env) stdfiles
  | _ -> sprogram, env
  in

  let initialize_t = void_fn_t in
  let initialize_lambda = SFunctionLambda(SBlock(env.initializations @ [SReturn (SUnitType, SUnitLiteral)])) in
  let initialize : sdecl = SVariable(initialize_name, initialize_t, Some (initialize_t, initialize_lambda)) in

  let insert_initialize_into_main sdecl =
    match sdecl with
    | SVariable("main", main_fn_t, Some (_, SFunctionLambda(SBlock(sstmts)))) ->
      let initialize_call = SExpr(SUnitType, SFunctionCall((void_fn_t, SIdentifier(initialize_name)), [])) in
      [
        initialize;
        SVariable("main", main_fn_t, Some (main_fn_t, SFunctionLambda(SBlock(initialize_call :: sstmts))))
      ]
    | _ -> [sdecl]
  in

  let sprogram = List.concat (List.map ~f:insert_initialize_into_main sprogram) in
  env, sprogram


let call_with_empty_env f = f Semantic_checker_env.empty


let check_typename (typename: Ast.typename) = (call_with_empty_env _check_typename) typename;;
let check_expr ?scope:(scope=(FunctionScope(UnitType))) (expr: Ast.expr) = _check_expr {Semantic_checker_env.empty with scope_context=scope} expr;;
let check_stmt ?scope:(scope=(FunctionScope(UnitType))) (stmt: Ast.stmt) = _check_stmt ~new_scope:false {Semantic_checker_env.empty with scope_context=scope} stmt;;
let check_decl ?scope:(scope=(FunctionScope(UnitType))) (decl: Ast.decl) = _check_decl {Semantic_checker_env.empty with scope_context=scope} decl;;
let check_program ?scope:(scope=(FunctionScope(UnitType))) (program: Ast.program) = _check_program {Semantic_checker_env.empty with scope_context=scope} program;;
