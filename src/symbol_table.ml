open Ast;;
open Printf;;
open Hierarchical_map;;

type symbol = {
  identifier : string;
  typename   : Sast.resolved_typename;
  defined    : bool;
};;


let string_of_symbol (symbol: symbol) : string =
  let defined_str = (if symbol.defined then " (defined)" else "") in
  sprintf "%s: %s%s" symbol.identifier (Oscdebug.string_of_resolved_typename symbol.typename) defined_str


module type Symbol = sig
  type t
  val identifier : t -> string
  val typename : t -> Sast.resolved_typename
  val defined : t -> bool
  val to_string : t -> string
end;;


module Make(SymbolType: Symbol) = struct 
  module SymbolMap = Hierarchical_map.Make(struct
      type t = SymbolType.t
      let key t = (SymbolType.identifier t)
      let to_string v = (SymbolType.to_string v)
    end)

  type symbol = SymbolType.t

  type t = SymbolMap.t


  exception UndeclaredIdentifier of string
  exception UndefinedIdentifier of symbol
  exception RedefinedIdentifier of string * symbol

  exception NotFound of string


  let empty = SymbolMap.empty


  let child (parent: t) = SymbolMap.child parent


  let depth (table: t) : int = SymbolMap.depth table


  let find (id: string) (table: t) : symbol = 
    try
      SymbolMap.find id table
    with Not_found -> raise (NotFound id)


  let add (s: symbol) (table: t) : t = 
    let identifier = SymbolType.identifier s in
    try
      let existing = SymbolMap.Map.find identifier table.values in
      let new_typename = SymbolType.typename s in
      let existing_typename = SymbolType.typename existing in

      if new_typename = existing_typename && (not (SymbolType.defined existing)) then
        SymbolMap.add s table
      else
        raise (RedefinedIdentifier (identifier, existing))

    with Not_found ->
      SymbolMap.add s table


  let to_string (table: t) : string = SymbolMap.to_string table


  let of_list (l: symbol list) : t = SymbolMap.of_list l
end;;
