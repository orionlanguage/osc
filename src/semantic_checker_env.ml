open Ast;;
open Sast;;
open Symbol_table;;
open Printf;;


exception NotImplemented of string


module SymbolTable = Symbol_table.Make(struct
    type t = Symbol_table.symbol
    let identifier t = t.identifier
    let typename t = t.typename
    let defined t = t.defined
    let to_string t = (Symbol_table.string_of_symbol t)
  end)


(* For use in return/emit statement checking - what kind of scope is this block
 * in? *)
type scope_context = 
  | GlobalScope
  | FunctionScope of typename 
  | ActorScope of typename


(* All of the "context" that you need to know to check a given expr/stmt/decl.
 * For example, to check an identifier, we need to know: what symbols are
 * defined at this point? *)
type t = {
  symbol_table: SymbolTable.t;
  type_table: SymbolTable.t;
  scope_context: scope_context;
  initializations: sstmt list;
};;


let builtin_type_symbols = [
      {identifier="str"; typename=SStringType; defined=true};
      {identifier="bool"; typename=SBoolType; defined=true};
      {identifier="byte"; typename=SByteType; defined=true};
      {identifier="int"; typename=SIntType; defined=true};
      {identifier="float"; typename=SFloatType; defined=true};
      {identifier="file"; typename=SFileType; defined=true};
    ]

let builtin_identifiers = [
      {identifier="stdin"; typename=SFileType; defined=true};
      {identifier="stdout"; typename=SFileType; defined=true};
      {identifier="stderr"; typename=SFileType; defined=true};

      {identifier="stoi"; typename=SFunctionType(["s", SStringType], SIntType); defined=true};
      {identifier="stof"; typename=SFunctionType(["s", SStringType], SFloatType); defined=true};

      {identifier="print"; typename=SFunctionType(["s", SStringType], SUnitType); defined=true};
      {identifier="format"; typename=SFunctionType(["format", SStringType], SStringType); defined=true}; (* arguments list is fake, but checking is skipped automatically *)
      
      {identifier="input"; typename=SFunctionType([], SStringType); defined=true};
      {identifier="clear"; typename=SFunctionType(["a", SListType(SIntType)], SUnitType); defined=true};
      {identifier="len"; typename=SFunctionType(["a", SListType(SIntType)], SIntType); defined=true};
      {identifier="range"; typename=SFunctionType(["exclusive_max", SIntType], SListType(SIntType)); defined=true};
      (* Magic, we need to just ignore incorrect subsequent arguments like format *)
      {identifier="append"; typename=SFunctionType(["list", SEmptyListType], SUnitType); defined=true};
      {identifier="remove"; typename=SFunctionType(["list", SEmptyListType; "index", SIntType], SUnitType); defined=true};

      {identifier="open"; typename=SFunctionType(["filename", SStringType; "mode", SStringType], SFileType); defined=true};
      {identifier="close"; typename=SFunctionType(["fileobj", SFileType], SIntType); defined=true};
      {identifier="read"; typename=SFunctionType(["fileobj", SFileType; "nbyte", SIntType; "buffer", SStringType], SIntType); defined=true};
      {identifier="write"; typename=SFunctionType(["fileobj", SFileType; "str", SStringType], SIntType); defined=true};
      {identifier="good"; typename=SFunctionType(["fileobj", SFileType], SBoolType); defined=true};

      {identifier="tcp_bind"; typename=SFunctionType(["port", SIntType], SFileType); defined=true};
      {identifier="tcp_accept"; typename=SFunctionType(["socket", SFileType], SFileType); defined=true};
      {identifier="tcp_select"; typename=SFunctionType(["sockets", SListType(SFileType); "timeout", SFloatType], SIntType); defined=true};
      {identifier="tcp_ready"; typename=SFunctionType(["socket", SFileType], SBoolType); defined=true};

      {identifier="tcp_connect"; typename=SFunctionType(["address", SStringType; "port", SIntType], SFileType); defined=true};
    ]


let empty =
  { symbol_table = SymbolTable.of_list builtin_identifiers; 
    type_table = SymbolTable.of_list builtin_type_symbols;
    scope_context = GlobalScope;
    initializations = [] }


(* Produces a new Semantic_checker_env.t whose symbol table contains the provided
 * symbol *)
let add_symbol (symbol: SymbolTable.symbol) (env : t) =
  {env with symbol_table=SymbolTable.add symbol env.symbol_table}


let rec resolve_typename (typename: typename) (env: t) : resolved_typename =
  let resolve_arguments arguments env = List.map (fun (i, t) -> i, resolve_typename t env) arguments in
  match typename with
  | UnitType -> SUnitType
  | Typename(id) -> let symbol = SymbolTable.find id env.type_table in symbol.typename
  | ListTypename(typename) -> SListType(resolve_typename typename env)
  | StructTypename(id, arguments) ->
    let resolved_arguments = resolve_arguments arguments env in
    SStructType(id, resolved_arguments)
  | FunctionTypename(arguments, return_type) ->
    let resolved_arguments = resolve_arguments arguments env in
    let resolved_return_type = resolve_typename return_type env in
    SFunctionType(resolved_arguments, resolved_return_type)
  | ActorTypename(arguments, return_type) ->
    let resolved_arguments = resolve_arguments arguments env in
    let resolved_return_type = resolve_typename return_type env in
    SActorType(resolved_arguments, resolved_return_type)
  | _ -> raise (NotImplemented (sprintf "Cannot resolve typename %s" (Oscdebug.string_of_typename typename)))


(* Produces a new Semantic_checker_env.t whose symbol table contains the
 * provided function argument *)
let add_argument (env : t) ((id, typ): Ast.argument) =
  let symbol = {identifier=id; typename=resolve_typename typ env; defined=true} in
  {env with symbol_table=SymbolTable.add symbol env.symbol_table} 


(* Produces a new Semantic_checker_env.t whose symbol table contains the provided
 * symbol *)
let add_type (type_symbol: SymbolTable.symbol) (env : t) =
  {env with type_table=SymbolTable.add type_symbol env.type_table}


let add_initialization (identifier: identifier) (value: sexpr) (env : t) =
  let symbol = SymbolTable.find identifier env.symbol_table in
  let assignment = SExpr(symbol.typename, SAssignment((symbol.typename, SIdentifier(identifier)), value)) in
  {env with initializations=env.initializations @ [assignment]}


(* Produces a new Semantic_checker_env.t whose symbol table is empty, but with the
 * current symbol table as parent *)
let new_scope (env : t)  =
  {env with symbol_table=SymbolTable.child env.symbol_table;
   type_table=SymbolTable.child env.type_table}


let new_scope_context (scope_context: scope_context) (env : t) =
  {env with scope_context=scope_context}


let to_string (env: t) : string =
  "symbols:\n" ^ SymbolTable.to_string env.symbol_table
