OSCMAIN=osc
OSCFMT=oscfmt
OSCTOP=osctop

TEST=osctest

EXECUTABLES=$(OSCMAIN) $(OSCFMT) $(OSCTOP)
EXECUTABLES_SRC=$(foreach e, $(EXECUTABLES), executables/$e/$emain.ml)

BUILDFLAGS=-cflag -color=always -cflag -w=@a-26-3-4-6-7-9-27-29-32..42-44-45-48-50-60  # Treat most warnings as errors
BUILD=corebuild ${BUILDFLAGS}


BIN=append
$(BIN): osc ./examples/$(BIN).os
	./osc ./examples/$(BIN).os
	./$(BIN)

default: osc


all: $(EXECUTABLES) $(TEST)


./support/build/libactors.a:
	make -C support/


PARSER_TESTS=test/parser/struct_tests.ml \
	     test/parser/declaration_tests.ml \
			 test/parser/expression_tests.ml \
	     test/parser/literals_tests.ml \
	     test/parser/type_tests.ml \
	     test/parser/function_tests.ml \
	     test/parser/program_tests.ml \
	     test/parser/parser_test_utils.ml \
	     test/parser/parser_tests.ml

SEMANTIC_CHECKER_TESTS=test/semantic_checker/literals_sc_tests.ml \
		       test/semantic_checker/symbol_table_tests.ml \
		       test/semantic_checker/sstmt_tests.ml \
		       test/semantic_checker/check_struct_tests.ml \
		       test/semantic_checker/variable_tests.ml \
		       test/semantic_checker/semantic_checker_test_utils.ml \
		       test/semantic_checker/function_sc_tests.ml \
		       test/semantic_checker/actor_sc_tests.ml \
		       test/semantic_checker/semantic_checker_tests.ml

INTEGRATION_TESTS=test/integration/integration_actor_tests.ml \
									test/integration/integration_test_utils.ml \
									test/integration/integration_tests.ml \
									test/integration/integration_list_tests.ml \
	                test/integration/integration_simple_tests.ml

TESTSRC=$(PARSER_TESTS) \
	$(SEMANTIC_CHECKER_TESTS) \
	$(INTEGRATION_TESTS) \
	test/osctest.ml

SRC=src/ast.mli \
    src/sast.mli \
    src/parser.mly \
    src/scanner.mll \
    src/hierarchical_map.ml \
    src/semantic_checker.ml \
    src/semantic_checker_env.ml \
    src/symbol_table.ml \
    src/codegen.ml \
    src/codegen_env.ml \
    src/oscdebug.ml \
    src/color.ml \
    src/fmtlib.ml \
    src/osc.ml

test: $(TEST)
	@./$(TEST)

$(TEST): $(TESTSRC) $(SRC) ./support/build/libactors.a
	$(BUILD) -package oUnit -package llvm $(TEST).native -I src/ -I test/ -I test/parser/ -I test/semantic_checker/ -I test/integration/
	mv $(TEST).native $(TEST)


$(EXECUTABLES): $(SRC) ./support/build/libactors.a $(EXECUTABLES_SRC)
	$(BUILD) -package oUnit -package llvm $@main.native -I src/ -I executables/$@/
	mv $@main.native $@

rebuild: clean $(EXECUTABLES)

clean:
	rm -rf _build
	rm -f $(TEST)
	rm -f $(EXECUTABLES)
	make -C support/ clean

deliverable:
	zip -R oscar.zip src/ examples/ test/ Makefile README.md requirements.sh

.PHONY: clean rebuild test deliverable test default all
