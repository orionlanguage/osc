#!/bin/bash

rm .git/hooks/*
cp .githooks/pre-commit .git/hooks
chmod +x .git/hooks/pre-commit
