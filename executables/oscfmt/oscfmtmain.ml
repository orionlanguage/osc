(* oscfmtmain.ml
 * The main function for the executable oscfmt, which semantically checks, then
 * formats one or more osc files (in color) and prints to stdout *)
open Core;;

let parse_buffer ?print_error:(print_error=true) parse lexbuf = 
    parse Scanner.token lexbuf

let from_channel ?print_error:(print_error=true) parse channel = 
  let lexbuf = Lexing.from_channel channel in
  parse_buffer ~print_error:print_error parse lexbuf

let from_filepath ?print_error:(print_error=true) parse path = from_channel ~print_error:print_error parse (In_channel.create path)

let program_of_filepath ?print_error:(print_error=true) = from_filepath ~print_error:print_error Parser.program


let check_and_fmt filepath =
  let program = program_of_filepath filepath in
  let _, sast = Semantic_checker.check_program program in
  print_endline (Fmtlib.code_of_sprogram sast)


let fmtmain () =
  for i = 1 to Array.length Sys.argv - 1 do
    check_and_fmt Sys.argv.(i)
  done;;

let _ =
  for i = 1 to Array.length Sys.argv - 1 do
    check_and_fmt Sys.argv.(i)
  done;;
