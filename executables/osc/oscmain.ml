(* oscmain.ml
 * The main function for the executable osc, which is the compiler program. *)

let () =
	let arguments = List.tl (Array.to_list Sys.argv) in (* skip argv[0] *)

	let arguments, libactors = match arguments with
	| "-L"::libactors::tl -> 
		print_endline (Printf.sprintf "Loading libactors from %s." libactors);
		tl, libactors
	| _ -> arguments, "./support/build/"
	in

	List.iter (Osc.compile_and_link ~libactors:libactors) arguments
