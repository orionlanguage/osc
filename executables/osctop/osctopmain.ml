(* osctopmain.ml
 * The main function for the executable osctop, which is a toplevel to the compiler
 * which allows you to input lines of Oscar code, one at a time, and then see the
 * total program produced.
 *
 * All provided lines are stuck into a main() function, since obviously you can't do
 * most things at the global level. *)

open Core
open Sast


let _ =
  let flush () = Out_channel.flush Out_channel.stdout in
  let checker_env = Semantic_checker_env.empty in

  let rec handle_line (senv: Semantic_checker_env.t) sstmts =
    let cenv = Codegen_env.create () in
    printf ">>> "; flush ();
    match In_channel.input_line In_channel.stdin with
    | None -> ()
    | Some s -> 
      let stmt = Osc.stmt_of_string s in
      let cenv, senv, sstmts = 
        try
          let senv, sstmt = Semantic_checker._check_stmt senv stmt in
          let () = print_endline (Fmtlib.code_of_sstmt sstmt) in

          let program_type = SFunctionType([], SIntType) in
          let sstmts = sstmts @ [sstmt] in
          let program = [SVariable("main", program_type, Some (program_type, SFunctionLambda(SBlock(sstmts))))] in

          let cenv = Codegen.translate_sprogram program in
          let module_str = Llvm.string_of_llmodule cenv.the_module in
          print_endline "";
          print_endline "";
          print_endline module_str;

          cenv, senv, sstmts

        with e -> 
          print_endline "Error."; cenv, senv, sstmts
      in

      handle_line senv sstmts
  in

  handle_line checker_env []
