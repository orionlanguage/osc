fn substring(s: str, start: int, end: int) -> str {
  let result: str = "";
  let i: int = start;
  while i < end && i < len(s) {
    append(result, s[i]);
    i = i + 1;
  }
  return result;
}


fn split(s: str, delim: str, max_matches: int) -> [str] {
  if len(delim) > 1 {
    print("split() expects single character strings.");
    return [s];
  }

  if max_matches == 0 {
    return [s];
  }

  let result: [str] = [];

  let last_match: int = 0;
  for i: int in range(len(s)) {
    if s[i] == delim[0] {
      let subs: str = substring(s, last_match, i);
      append(result, subs);
      last_match = i + 1;

      if len(result) >= max_matches && last_match < len(s) {
        let remainder: str = substring(s, last_match, len(s));
        append(result, remainder);
        return result;
      }
    }
  }

  let remainder: str = substring(s, last_match, len(s));
  append(result, remainder);
  return result;
}


let sock: file;
let name: str;


fn format_message(text: str, kind: str) -> str {
  return format("%s|%s|%s", name, kind, text);
}


fn send_connection_message(sock: file) -> () {
  write(sock, format_message("Connected.", "CONNECT"));
  return ();
}


actor send_message(s: str) -> () {
  write(sock, format_message(s, "MESSAGE"));
}


fn main() -> int {
  write(stdout, "Your name: ");
  name = input();

  print(format("Your name is '%s'. Connecting...", name));
  sock = tcp_connect("localhost", 3000);
  print("Connected!");

  if !good(sock) {
    print("Failed to connect.");
    return 1;
  }

  send_connection_message(sock);

  let files: [file] = [stdin, sock];
  let timeout: float = 1.0; // seconds

  write(stdout, ">>> ");
  while true {
    let files_ready: int = tcp_select(files, timeout);

    if files_ready > 0 {
      if tcp_ready(stdin) {
        let buffer: str = input();

        if buffer == "" {
          print("Closing...");
          close(sock);
          print("Closed.");
          return 0;
        } else {
          send_message(buffer);
          write(stdout, ">>> ");
        }
      }

      if tcp_ready(sock) {
        let buffer: str = "";
        let bytes_read: int = read(sock, 4096, buffer);

        let parts: [str] = split(buffer, "|", 3);
        if len(parts) == 3 {
          write(stdout, format("\r[%24s]   %s", parts[0], parts[2]));
          write(stdout, ">>> ");
        } else {
          print("Server said something weird:");
          print(buffer);
          write(stdout, ">>> ");
        }
      }
    }
  }

  return 0;
}
