fn myprint(s: str) -> int {
  print(s);
  return 0;
}

fn main() -> int {
  let s: str = "Hello, world!";
  print(s);
  return myprint(s);
}
