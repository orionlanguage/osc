fn main() -> int {
  let clients: [file] = [];
  let socket: file = tcp_bind(3000);
  append(clients, socket);

  let buffer: str = "";

  while true {
    print(format("Waiting on %d sockets.", len(clients)));
    let ready_count: int = tcp_select(clients, 1.0);

    print("Woke up.");
    if ready_count == 0 {
      print("Timed out.");
    } else {
      let i: int = 0;
      let new_client: file;
      let new_client_added: bool = false;
      let removed: [int] = [];

      for s: file in clients {
        if i == 0 && tcp_ready(s) {
          new_client = tcp_accept(socket);
          new_client_added = true;
        } elif tcp_ready(s) {
          let bytes_read: int = read(s, 4096, buffer);

          if bytes_read == 0 {
            print(format("Client %d disconnected.", i, buffer));
            append(removed, i);
          } else {
            print(format("Client %d said: %s", i, buffer));

            for client: file in clients {
              write(client, buffer);
            }
          }
        }
        i = i + 1;
      }

      for index: int in removed {
        remove(clients, index);
        print(format("Removed client %d", index));
      }

      if new_client_added {
        print("Added new client.");
        append(clients, new_client);
      }
    }
  }

  return 0;
}
