type Movie = struct(year: int, title: str);


fn main() -> int {
  let m: Movie = Movie(1942, "Casablanca");
  print(format("%d: %s", m.year, m.title));

  return 0;
}
