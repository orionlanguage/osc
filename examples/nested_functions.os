fn main() -> int {
  let s: str = "Hello, world!";
  print(s);

  fn myprint(s: str) -> int {
    print(s);
    return 0;
  }

  return myprint(s);
}
