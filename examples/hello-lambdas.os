fn main() -> int {
  fn id(i: int) -> int {
    return i;
  }

  print(format("id(%d) = %d", 3, id(3)));

  return 0;
}
