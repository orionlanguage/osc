let running: bool = true;


fn substring(s: str, start: int, end: int) -> str {
  let result: str = "";
  let i: int = start;
  while i < end && i < len(s) {
    append(result, s[i]);
    i = i + 1;
  }
  return result;
}


fn split(s: str, delim: str, max_matches: int) -> [str] {
  if len(delim) > 1 {
    print("split() expects single character strings.");
    return [s];
  }

  if max_matches == 0 {
    return [s];
  }

  let result: [str] = [];

  let last_match: int = 0;
  for i: int in range(len(s)) {
    if s[i] == delim[0] {
      let subs: str = substring(s, last_match, i);
      append(result, subs);
      last_match = i + 1;

      if len(result) >= max_matches && last_match < len(s) {
        let remainder: str = substring(s, last_match, len(s));
        append(result, remainder);
        return result;
      }
    }
  }

  let remainder: str = substring(s, last_match, len(s));
  append(result, remainder);
  return result;
}


fn read_to_eof(f: file) -> str {
  let total: str = "";
  let buffer: str = "";

  let ready: bool = true;
  while ready {
    let bytes_read: int = read(f, 4096, buffer);

    if bytes_read == 0 {
      ready = false;
    } else {
      total = total + buffer;
    }
  }

  return total;
}


type HttpResponse = struct(body: str, success: bool);


fn http_request(url: str, route: str) -> HttpResponse {
  let socket: file = tcp_connect(url, 80);

  if !good(socket) {
    print("");
    return HttpResponse(format("Failed to lookup host named %s", url), false);
  }

  let request: str = format("GET /%s HTTP/1.0\r\n\r\n", route);
  write(socket, request);
  return HttpResponse(read_to_eof(socket), true);
}


fn main() -> int {
  print("Type a URL and then hit <Enter> to make a request.");
  print("When you're done, hit <Ctrl-D> to exit.");
  while running {
    write(stdout, ">>> GET http://");
    let url: str = input();

    if len(url) == 0 {
      print("");
      return 0;
    }

    let parts: [str] = split(url, "/", 1);

    let baseurl: str = parts[0];

    let route: str = "";
    if len(parts) > 1 {
      route = parts[-1];
    }

    let response: HttpResponse = http_request(baseurl, route);
    if response.success {
      print("\e[32mHTTP RESPONSE:\e[0m");
      print("-------------------------------------------");
      print(response.body);
      print("-------------------------------------------");
    } else {
      print("\e[31mHTTP ERROR:\e[0m");
      print(response.body);
    }
  }

  return 0;
}
