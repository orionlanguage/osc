type User = struct(socket: file, name: str, color: int, connected: bool);
let next_color: int = 31;
let users: [User] = [];


fn substring(s: str, start: int, end: int) -> str {
  let result: str = "";
  let i: int = start;
  while i < end && i < len(s) {
    append(result, s[i]);
    i = i + 1;
  }
  return result;
}


fn split(s: str, delim: str, max_matches: int) -> [str] {
  if len(delim) > 1 {
    print("split() expects single character strings.");
    return [s];
  }

  if max_matches == 0 {
    return [s];
  }

  let result: [str] = [];

  let last_match: int = 0;
  for i: int in range(len(s)) {
    if s[i] == delim[0] {
      let subs: str = substring(s, last_match, i);
      append(result, subs);
      last_match = i + 1;

      if len(result) >= max_matches && last_match < len(s) {
        let remainder: str = substring(s, last_match, len(s));
        append(result, remainder);
        return result;
      }
    }
  }

  let remainder: str = substring(s, last_match, len(s));
  append(result, remainder);
  return result;
}


type Message = struct(text: str, sender: User);
type MessageForUser = struct(message: Message, recipient: User);


actor package_message_for_all_users(message: Message) -> MessageForUser {
  print("Packaging message.");
  for user: User in users {
    if user.name != "Server" {
      if message.sender.name != user.name {
        print(format("Sending %s's message to %s...", message.sender.name, user.name));
        emit MessageForUser(message, user);
      } else {
        print(format("Not sending message back to %s, they sent it!", message.sender.name));
      }
    }
  }
}


actor send_user(message_for_user: MessageForUser) -> () {
  write(message_for_user.recipient.socket, message_for_user.message.text);
}


fn format_message(text: str, sender: User) -> Message {
  return Message(format("\e[%dm%s\e[0m|MESSAGE|%s\n", sender.color, sender.name, text), sender);
}


actor filter_broadcast_message(message: Message) -> Message {
  let parts: [str] = split(message.text, "|", 3);

  if len(parts) != 3 {
    print(format("Received invalid message from %s", message.sender.name));
  } elif parts[1] == "MESSAGE" {
    print(format("Received broadcast from %s: '%s'", message.sender.name, parts[2]));
    emit format_message(parts[2], message.sender);
  }
}


fn server_message(text: str) -> Message {
  return format_message(text, users[0]);
}


actor filter_connection_message(message: Message) -> Message {
  let parts: [str] = split(message.text, "|", 3);

  if len(parts) != 3 {
    print(format("Received invalid message from %s", message.sender.name));
  } elif parts[1] == "CONNECT" {
    print(format("%s has connected.", parts[0]));
    message.sender.name = parts[0];
    message.sender.connected = true;
    emit server_message(format("%s has joined the chat room.", message.sender.name));
  }
}


actor server(port: int) -> Message {
  let sockets: [file] = [];
  let socket: file = tcp_bind(port);
  append(sockets, socket);
  append(users, User(socket, "Server", 37, true));

  let buffer: str = "";
  while true {
    print(format("Waiting on %d sockets.", len(sockets)));
    let timeout: float = 10.0;
    let ready_count: int = tcp_select(sockets, timeout);

    if ready_count == 0 {
      print("Timed out.");
    } else {
      let i: int = 0;
      let new_client: file;
      let new_client_added: bool = false;
      let removed: [int] = [];

      for s: file in sockets {
        if i == 0 && tcp_ready(s) {
          new_client = tcp_accept(socket);
          new_client_added = true;
        } elif tcp_ready(s) {
          let bytes_read: int = read(s, 4096, buffer);

          if bytes_read == 0 {
            print(format("%s disconnected.", users[i].name, buffer));
            append(removed, i);
          } else {
            print(format("%s said: %s", users[i].name, buffer));

            emit Message(buffer, users[i]);
          }
        }
        i = i + 1;
      }

      for index: int in removed {
        print(format("Removing client %s", users[index].name));
        remove(sockets, index);
        remove(users, index);
      }

      if new_client_added {
        print("Added new client.");
        append(sockets, new_client);
        append(users, User(new_client, "<New>", next_color, false));
        next_color = next_color + 1;
      }
    }
  }
}


fn main() -> int {
  server += (filter_connection_message => package_message_for_all_users => send_user);
  server += (filter_broadcast_message => package_message_for_all_users => send_user);
  server(3000);
  return 0;
}
