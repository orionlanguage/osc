open OUnit2;;

open Literals_sc_tests;;
open Symbol_table_tests;;
open Variable_tests;;
open Sstmt_tests;;
open Check_struct_tests;;
open Function_sc_tests;;
open Actor_sc_tests;;
open Aritmetic_sc_tests;;


let run_semantic_checker_tests () =
  print_endline "Running literals suite...";
  run_test_tt_main literals_suite;

  print_endline "Running symbol table suite...";
  run_test_tt_main symbol_table_suite;

  print_endline "Running variable suite...";
  run_test_tt_main variable_suite;

  print_endline "Running sstmts suite...";
  run_test_tt_main sstmts_suite;

  print_endline "Running struct definitions suite...";
  run_test_tt_main structs_suite;

  print_endline "Running functions suite...";
  run_test_tt_main function_suite;

  print_endline "Running actors suite...";
  run_test_tt_main actor_suite;

  print_endline "Running Aritmetic suite...";
  run_test_tt_main aritmetic_suite;
  
;;
