open OUnit2;;
open Osc;;
open Ast;;
open Sast;;
open Symbol_table;;
open Semantic_checker_env;;
open Semantic_checker_test_utils;;


let test_declare_unit_literal_variable _test_ctxt = 
  let expected: sdecl = SVariable("x", SUnitType, None)
  and _, actual = Osc.check_decl (Osc.decl_of_string "let x: ();") in
  assert_sdecls_equal [expected] actual;;


let test_define_unit_literal_variable _test_ctxt = 
  let expected: sdecl = SVariable("x", SUnitType, Some (SUnitType, SUnitLiteral))
  and _, actual = Osc.check_decl ~scope:(FunctionScope(UnitType)) (Osc.decl_of_string "let x: () = ();") in
  assert_sdecls_equal [expected] actual;;


(* Checks that if declared type and defined type differ, a TypeMismatch is raised. *)
let test_define_wrong_type_triggers_error _test_ctxt = 
  let raises_exception () = Osc.check_decl (Osc.decl_of_string "let x: () = 9;") in
  assert_raises (Semantic_checker.TypeMismatch ((SIntType, SIntLiteral 9), SUnitType)) raises_exception


let test_redeclare_variable_with_different_type_fails _test_ctxt = 
  let already_declared_type = SIntType in
  let already_declared_symbol: SymbolTable.symbol = {identifier="x";typename=already_declared_type; defined=false} in
  let env = Semantic_checker_env.add_symbol already_declared_symbol Semantic_checker_env.empty in
  let raises_exception () = Semantic_checker._check_decl env (Osc.decl_of_string "let x: ();") in
  assert_raises (SymbolTable.RedefinedIdentifier ("x", already_declared_symbol)) raises_exception


let test_declare_two_variables_program _test_ctxt = 
  let expected: sprogram = [SVariable("x", SUnitType, Some (SUnitType, SUnitLiteral));
                            SVariable("y", SUnitType, Some (SUnitType, SUnitLiteral))]
  and _, actual = Osc.check_program ~scope:(FunctionScope(UnitType)) (Osc.program_of_string "let x: () = (); let y: () = ();") in
  assert_sprogram_equal expected actual;;


let test_program_redeclare_variable_type_fails _test_ctxt = 
  let already_defined_symbol: SymbolTable.symbol = {identifier="x";typename=SUnitType;defined=false} in
  let expected_error = (SymbolTable.RedefinedIdentifier ("x", already_defined_symbol)) in
  assert_raises expected_error (fun () -> Osc.check_program (Osc.program_of_string "let x: (); let x: int;"))


let test_program_redefine_variable_fails _test_ctxt = 
  let already_defined_symbol: SymbolTable.symbol = {identifier="x";typename=SIntType;defined=true} in
  let expected_error = (SymbolTable.RedefinedIdentifier ("x", already_defined_symbol)) in
  assert_raises expected_error (fun () -> Osc.check_program (Osc.program_of_string "let x: int; let x: int = 3; let x: int = 9;"))


let test_program_redefine_variable_type_fails _test_ctxt = 
  let already_defined_symbol: SymbolTable.symbol = {identifier="x";typename=SIntType;defined=true} in
  let expected_error = (SymbolTable.RedefinedIdentifier ("x", already_defined_symbol)) in
  assert_raises expected_error (fun () -> Osc.check_program (Osc.program_of_string "let x: int = 3; let x: float = 9.0;"))


let test_program_redeclare_variable_type_works _test_ctxt = 
  let expected: sprogram = [SVariable("x", SUnitType, None);
                            SVariable("x", SUnitType, None)]
  and _, actual = Osc.check_program (Osc.program_of_string "let x: (); let x: ();") in
  assert_sprogram_equal expected actual;;


let test_variable_defined_in_child_scope_not_available_in_parent _test_ctxt = 
  let block = "{ \
    {let x: int = 9;} \
    x; \
  }" in
  let raises_error () = Osc.check_stmt (Osc.stmt_of_string block) in
  assert_raises (SymbolTable.UndeclaredIdentifier "x") raises_error


let variable_suite =
  "variable_suite">:::
  [
    "test_declare_unit_literal_variable">:: test_declare_unit_literal_variable;
    "test_define_unit_literal_variable">:: test_define_unit_literal_variable;
    "test_define_wrong_type_triggers_error">:: test_define_wrong_type_triggers_error;
    "test_redeclare_variable_with_different_type_fails">:: test_redeclare_variable_with_different_type_fails;
    "test_declare_two_variables_program">:: test_declare_two_variables_program;
    "test_program_redeclare_variable_type_fails">:: test_program_redeclare_variable_type_fails;
    "test_program_redeclare_variable_type_works">:: test_program_redeclare_variable_type_works;
    "test_program_redefine_variable_fails">:: test_program_redefine_variable_fails;
    "test_program_redefine_variable_type_fails">:: test_program_redefine_variable_type_fails;
    "test_variable_defined_in_child_scope_not_available_in_parent">:: test_variable_defined_in_child_scope_not_available_in_parent;
  ]
;;
