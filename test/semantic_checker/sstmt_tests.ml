open OUnit2;;
open Osc;;
open Ast;;
open Sast;;
open Semantic_checker_env;;
open Semantic_checker_test_utils;;


let test_block_checks_statements _test_ctxt = 
  let expected = SBlock([SExpr(SUnitType, SUnitLiteral); 
                         SExpr(SIntType, SIntLiteral 9)])
  and _, actual = Osc.check_stmt (Osc.stmt_of_string "{ (); 9; }") in
  assert_sstmt_equal expected actual;;


(* Checks that a block introduces a new scope, so variable names used
 * in enclosing scope can be reused. *)
let test_block_introduces_new_scope _test_ctxt = 
  let expected = SBlock([
                         SDeclarations([SVariable("x", SIntType, Some(SIntType, SIntLiteral 9))]); 
                         SBlock([
                           SDeclarations([SVariable("x", SBoolType, Some(SBoolType, SBoolLiteral true))]);
                         ])
                        ])
  and _, actual = Osc.check_stmt (Osc.stmt_of_string "{let x: int = 9; { let x: bool = true; }}") in
  assert_sstmt_equal expected actual;;


(* Checks that a block introduces a new scope, so variable names defined
 * in enclosing scope can be reused. *)
let test_child_scope_use_parent_identifiers_works _test_ctxt = 
  let expected = SBlock([
                         SDeclarations([SVariable("x", SIntType, Some(SIntType, SIntLiteral 9))]); 
                         SBlock([
                           SExpr(SIntType, SIdentifier("x"));
                         ])
                        ])
  and _, actual = Osc.check_stmt (Osc.stmt_of_string "{let x: int = 9; { x; }}") in
  assert_sstmt_equal expected actual;;


let test_child_scope_use_undefined_identifiers_raises_exception _test_ctxt = 
  let raises_error () = Osc.check_stmt (Osc.stmt_of_string "{let x: int = 9; { a; }}") in
  assert_raises (SymbolTable.UndeclaredIdentifier("a")) raises_error

let test_non_boolexpres_raises_exception _test_ctxt = 
  let raises_error () = Osc.check_stmt (Osc.stmt_of_string "{ let x: int = 9; if 7 != 3 { x = 4; } elif 5 { x = 4; }  }") in
  assert_raises (Semantic_checker.ExpectedBoolean(SIntType, SIntLiteral(5))) raises_error

let test_typemismatch_for_loop_raises_exception _test_ctxt = 
  let num_type = (SListType(SIntType), SIdentifier("nums")) in
  let raises_error () = Osc.check_stmt (Osc.stmt_of_string "{ let nums: [int] = [1]; for x: float in nums { } }") in
  assert_raises (Semantic_checker.TypeMismatch(num_type, SFloatType )) raises_error

let test_thru_string_with_char _test_ctxt = 
  let expected = SFor(("x", SByteType), (SStringType, SStringLiteral("nums")), SBlock([])) 
  and _, actual = Osc.check_stmt (Osc.stmt_of_string "for x: byte in \"nums\" { }") in
  assert_sstmt_equal expected actual;;

let sstmts_suite =
  "sstmts_suite">:::
  [
    "test_block_checks_statements">:: test_block_checks_statements;
    "test_block_introduces_new_scope">:: test_block_introduces_new_scope;
    "test_child_scope_use_parent_identifiers_works">:: test_child_scope_use_parent_identifiers_works;
    "test_child_scope_use_undefined_identifiers_raises_exception">:: test_child_scope_use_undefined_identifiers_raises_exception;
    "test_non_boolexpres_raises_exception">:: test_non_boolexpres_raises_exception;
    "test_typemismatch_for_loop_raises_exception">:: test_typemismatch_for_loop_raises_exception;
    (*"test_thru_string_with_char">:: test_thru_string_with_char;*)
  ]
;;
