open OUnit2;;
open Osc;;
open Ast;;
open Sast;;
open Semantic_checker_env;;
open Semantic_checker_test_utils;;


(* Checks that the builtin types are defined in the Semantic_checker_env by *
 * default. *)
let test_builtin_types_are_defined _test_ctxt = 
  let env = Semantic_checker_env.empty in
  ignore (SymbolTable.find "str" env.type_table) (* can raise Not_found *)


(* Checks that we can declare the person struct. *)
let test_declare_person_struct _test_ctxt = 
  let struct_type = SStructType("Person",
                                [("first_name", SStringType);
                                 ("last_name", SStringType)]) in
  let expected: sdecl list = [SType("Person", struct_type);
                              Semantic_checker.generate_constructor struct_type;
                              Semantic_checker.generate_copy_constructor struct_type]
  and _, actual = Osc.check_decl (Osc.decl_of_string "type Person = struct(first_name: str, last_name: str);") in
  assert_sdecls_equal expected actual;;


(* Checks that struct args must be previously defined typenames. *)
let test_declare_struct_undeclared_arguments_fails _test_ctxt = 
  let raises_error () = Osc.check_decl (Osc.decl_of_string "type Person = struct(dog: Dog);") in
  assert_raises (SymbolTable.UndeclaredIdentifier("Dog")) raises_error


let test_rename_declaration _test_ctxt = 
  let expected: sdecl = SType("number", SIntType)
  and _, actual = Osc.check_decl (Osc.decl_of_string "type number = int;") in
  assert_sdecls_equal [expected] actual;;


(* Checks that we can use a previously defined struct in a new struct. *)
let test_nested_struct_declaration _test_ctxt = 
  let name_type = SStructType("Name", 
                              [("first_name", SStringType);
                               ("last_name", SStringType)]) in
  let name_type_decl = SType("Name", name_type) in
  let person_type = SStructType("Person",
                                [("name", name_type);
                                 ("age", SIntType)]) in
  let person_type_decl = SType("Person", person_type) in
  let expected: sprogram = [name_type_decl; 
                            Semantic_checker.generate_constructor name_type; 
                            Semantic_checker.generate_copy_constructor name_type; 
                            person_type_decl;
                            Semantic_checker.generate_constructor person_type;
                            Semantic_checker.generate_copy_constructor person_type;
                           ] in
  let program = "type Name = struct(first_name: str, last_name: str); 
                 type Person = struct(name: Name, age: int);" in
  let _, actual = Osc.check_program (Osc.program_of_string program) in
  assert_sprogram_equal expected actual;;


let test_defined_types_available_in_child_scopes _test_ctxt = 
  let name_type = SStructType("Name",
                              [("first_name", SStringType);
                               ("last_name", SStringType)]) in
  let name_type_decl = SType("Name", name_type) in

  let expected: sstmt = SBlock([
    SDeclarations([name_type_decl; 
                   Semantic_checker.generate_constructor name_type;
                   Semantic_checker.generate_copy_constructor name_type;
                  ]);
    SBlock([
      SDeclarations([SVariable("name", name_type, None)])
    ])
  ]) in

  let block = "{ 
                  type Name = struct(first_name: str, last_name: str); 
                  {
                    let name: Name;
                  }
                 }" in
  let _, actual = Osc.check_stmt (Osc.stmt_of_string block) in
  assert_sstmt_equal expected actual;;


let test_undeclared_type_variable_declarations_fail _test_ctxt = 
  let raises_undeclared () = Osc.check_decl (Osc.decl_of_string "let name: Name;") in
  assert_raises (SymbolTable.UndeclaredIdentifier "Name") raises_undeclared


let test_defined_types_not_available_in_parent_scopes _test_ctxt = 
  let block = "{
    { type Name = struct(first_name: str, last_name: str); }
    let name : Name;
  }" in
  let raises_undeclared () = Osc.check_stmt (Osc.stmt_of_string block) in
  assert_raises (SymbolTable.UndeclaredIdentifier "Name") raises_undeclared


let test_use_dereferenced_member _test_ctxt = 
  let name_args = [("first_name", SStringType);
                   ("last_name", SStringType)] in
  let name_type = SStructType("Name", name_args) in
  let name_type_decl = SType("Name", name_type) in

  let constructor_type =
    SFunctionType(name_args, name_type)
  in

  let constructor_call = name_type, SFunctionCall(
    (constructor_type, SIdentifier("Name")),
    [(SStringType, SStringLiteral("Leonardo"));
     (SStringType, SStringLiteral("DiCaprio"))]
  ) in

  let print_type = SFunctionType(["s", SStringType], SUnitType), SIdentifier("print") in
  let print_call = SFunctionCall(print_type, 
        [SStringType, SStructDereference((name_type, SIdentifier("name")), 0, "first_name")])
  in

  let expected: sstmt = SBlock([
    SDeclarations([name_type_decl; 
                   Semantic_checker.generate_constructor name_type;
                   Semantic_checker.generate_copy_constructor name_type;
                  ]);
    SDeclarations([SVariable("name", name_type, Some constructor_call)]);
    SExpr(SUnitType, print_call)
  ]) in

  let block = "{
    type Name = struct(first_name: str, last_name: str);
    let name : Name = Name(\"Leonardo\", \"DiCaprio\");
    print(name.first_name);
  }" in

  let _, actual = Osc.check_stmt (Osc.stmt_of_string block) in
  assert_sblocks_equal expected actual


let structs_suite =
  "structs_suite">:::
  [
    "test_declare_person_struct">:: test_declare_person_struct;
    "test_declare_struct_undeclared_arguments_fails">:: test_declare_struct_undeclared_arguments_fails;
    "test_builtin_types_are_defined">:: test_builtin_types_are_defined;
    "test_rename_declaration">:: test_rename_declaration;
    "test_nested_struct_declaration">:: test_nested_struct_declaration;
    "test_undeclared_type_variable_declarations_fail">:: test_undeclared_type_variable_declarations_fail;
    "test_defined_types_available_in_child_scopes">:: test_defined_types_available_in_child_scopes;
    "test_defined_types_not_available_in_parent_scopes">:: test_defined_types_not_available_in_parent_scopes;
    "test_use_dereferenced_member">:: test_use_dereferenced_member;
  ]
;;
