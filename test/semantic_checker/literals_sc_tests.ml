open OUnit2;;
open Osc;;
open Ast;;
open Sast;;
open Semantic_checker_test_utils;;


let test_unit_literal _test_ctxt = 
  let expected = SUnitType, SUnitLiteral 
  and _, actual = Osc.check_expr UnitLiteral in
  assert_sexpr_equal expected actual;;

(*
let test_multiple_item_list _test_ctxt = 
  let expected = ListLiteral([IntLiteral(3); IntLiteral(4); IntLiteral(5)])
  and actual = Osc.expr_of_string "[3, 4, 5]" in
  assert_expr_equal expected actual;;
*)
let test_string_literal _test_ctxt = 
  let expected = SStringType, SStringLiteral("x")
  and _, actual = Osc.check_expr (Osc.expr_of_string "x") in
  assert_sexpr_equal expected actual;;

let test_float_literal _test_ctxt = 
  let expected = SFloatType, SFloatLiteral(9.11) 
  and _, actual = Osc.check_expr (Osc.expr_of_string "9.11") in
  assert_sexpr_equal expected actual;;

let test_bool_literal _test_ctxt = 
  let expected = SBoolType, SBoolLiteral(false)
  and _, actual = Osc.check_expr (Osc.expr_of_string "false") in
  assert_sexpr_equal expected actual;;

let test_int_literal _test_ctxt = 
  let expected = SIntType, SIntLiteral(1) 
  and _, actual = Osc.check_expr (Osc.expr_of_string "1") in
  assert_sexpr_equal expected actual;;

let test_list_literal _test_ctxt = 
  let expected = SListType(SIntType), SListLiteral([(SIntType,SIntLiteral(1))])
  and _, actual = Osc.check_expr (Osc.expr_of_string "[1]") in
  assert_sexpr_equal expected actual;;

let test_empty_list_literal _test_ctxt = 
let expected = SEmptyListType, SListLiteral([])
and _, actual = Osc.check_expr (Osc.expr_of_string "[]") in
assert_sexpr_equal expected actual;;

let literals_suite =
  "literals_suite">:::
  [
    "test_unit_literal">:: test_unit_literal;
    "test_string_literal">:: test_unit_literal;
    "test_float_literal">:: test_float_literal;
    "test_bool_literal">:: test_bool_literal;
    "test_int_literal">:: test_int_literal;
    "test_list_literal">:: test_list_literal;
    "test_empty_list_literal">:: test_empty_list_literal;
  ]
;;
