open OUnit2;;
open Osc;;
open Ast;;
open Sast;;
open Semantic_checker_test_utils;;
open Semantic_checker;;

(* Tests simple function return
*)
let test_match_Function_Lambda_Return_Type _test_ctxt = 
  let expected_type =  SFunctionType(["i", SIntType], SIntType) in
  let expected = SVariable("id", expected_type,Some( expected_type, SFunctionLambda(SBlock([ SReturn( SIntType, SIntLiteral(1)) ]))))
  and _, actual = Osc.check_decl (Osc.decl_of_string "fn id(i: int) -> int { return 1; }") in
  assert_sdecls_equal [expected] actual;;

let neg_test_match_Function_Lambda_Return_Type _test_ctxt = 
  let raises_error () = Osc.check_decl (Osc.decl_of_string "fn id(i: int) -> int { return 1.1; }") in
  assert_raises (Semantic_checker.TypeMismatch((SFloatType, SFloatLiteral(1.1)), SIntType)) raises_error

(* Checks that function arguments can be used in the body of the function. *)
let test_lambda_arguments_used_in_body _test_ctxt = 
  let expected_type =  SFunctionType(["a", SIntType], SIntType) in
  let expected = SVariable("id", 
                           expected_type,
                           Some(expected_type,
                                SFunctionLambda(SBlock([ SReturn( SIntType, SIdentifier("a")) ]))))
  and _, actual = Osc.check_decl (Osc.decl_of_string "fn id(a: int) -> int { return a; }") in
  assert_sdecls_equal [expected] actual;;

(* Checks that redefining an argument in the body of a function is an error. *)
let test_lambda_arguments_cannot_be_redefined _test_ctxt = 
  let raises_error () = Osc.check_decl (Osc.decl_of_string "fn id(i: int) -> int { let i: () = (); }") in
  assert_raises (Semantic_checker_env.SymbolTable.RedefinedIdentifier("i", {identifier="i"; typename=SIntType; defined=true})) raises_error


let neg_test_emit_Function_Lambda _test_ctxt = 
  let raises_error () = Osc.check_decl (Osc.decl_of_string "fn id(i: int) -> int { emit 1.1; }") in
  assert_raises (Semantic_checker.CantEmitFromFunction) raises_error


let test_call_with_int_works _test_ctxt = 
  let env, fntype = declare_function_of_string "fn id(i: int) -> int { return i; }"  in
  let call = Osc.expr_of_string "id(3)" in
  let _, actual  = Semantic_checker._check_expr env call in
  let expected : sexpr = SIntType, SFunctionCall((fntype, SIdentifier("id")), [SIntType, SIntLiteral(3)]) in
  assert_sexpr_equal expected actual


let test_call_with_str_works _test_ctxt = 
  let env, fntype = declare_function_of_string "fn id(i: str) -> str { return i; }"  in
  let call = Osc.expr_of_string "id(\"Hello\")" in
  let _, actual  = Semantic_checker._check_expr env call in
  let expected : sexpr = SStringType, SFunctionCall((fntype, SIdentifier("id")), [SStringType, SStringLiteral("Hello")]) in
  assert_sexpr_equal expected actual


let test_call_str_argument_with_int_fails _test_ctxt = 
  let env, fntype = declare_function_of_string "fn id(i: str) -> str { return i; }"  in
  let call = Osc.expr_of_string "id(3)" in
  let raises_error () = Semantic_checker._check_expr env call in
  assert_raises (Semantic_checker.InvalidArguments "Wrong argument type to 'id'") raises_error


let test_call_str_argument_with_two_strings_fails _test_ctxt = 
  let env, fntype = declare_function_of_string "fn id(i: str) -> str { return i; }"  in
  let call = Osc.expr_of_string "id(\"Hello\", \"World\")" in
  let raises_error () = Semantic_checker._check_expr env call in
  assert_raises (Semantic_checker.InvalidArguments "Wrong number of arguments to 'id': 2, expected 1.") raises_error


let format_typename = SFunctionType(["format", SStringType], SStringType)


let test_call_format_with_arbitrary_arguments_works _test_ctxt = 
  let call = Osc.expr_of_string "format(\"Hello\", \"World\")" in
  let arguments = [SStringType, SStringLiteral("Hello");
                   SStringType, SStringLiteral("World")] in
  let _, actual = Semantic_checker.check_expr call in
  let expected : sexpr = SStringType, SFunctionCall((format_typename, SIdentifier("format")), arguments) in
  assert_sexpr_equal expected actual


let test_call_format_with_int_first_fails _test_ctxt = 
  let call = Osc.expr_of_string "format(3)" in
  let raises_error () = Semantic_checker.check_expr call in
  assert_raises (Semantic_checker.InvalidArguments "Wrong argument type to 'format'") raises_error


let test_NoStmtsAllowedAfterReturnStmt_fails _test_ctxt = 
  let raises_error () = Osc.check_decl (Osc.decl_of_string "fn id(a: int) -> int { return a; a = a + 2; }") in 
  assert_raises (Semantic_checker.NoStmtsAllowedAfterReturnStmt) raises_error

let test_return_wrong_type_fails _test_ctxt = 
  let raises_error () = Osc.check_decl (Osc.decl_of_string "fn i(p: int) -> bool { p = p + 1; return 5; }") in 
  assert_raises (Semantic_checker.TypeMismatch((SIntType, SIntLiteral(5)), SBoolType)) raises_error

let test_return_body_semantic_check_fails _test_ctxt = 
  let raises_error () = Osc.check_decl (Osc.decl_of_string "fn i(p: int) -> int { p = p + true; return 5; }") in 
  assert_raises (Semantic_checker.TypeMismatch((SBoolType, SBoolLiteral(true)), SIntType)) raises_error

let test_thing_being_called_is_function_semantic_check_fails _test_ctxt = 
  let raises_error () = Osc.check_decl (Osc.decl_of_string "fn i(p: int) -> int { let yn: int = 8; p = yn(p); return 5; }") in 
  assert_raises (Semantic_checker.ExpectedFunction) raises_error

let test_check_All_Args_match_the_types_and_positions_fails _test_ctxt = 
  let env, fntype = declare_function_of_string "fn id(i: bool,x: int) -> str { return \"Hi\"; }"  in
  let call = Osc.expr_of_string "id(true,true)" in
  let raises_error () = Semantic_checker._check_expr env call in
  assert_raises (Semantic_checker.InvalidArguments "Wrong argument type to 'id'") raises_error
  
let test_undefined_fun_arg_fails _test_ctxt = 
  let raises_error () = Osc.check_decl (Osc.decl_of_string "fn i(p: int, x: jkl) -> int { return 5; }") in 
  assert_raises (Semantic_checker_env.SymbolTable.UndeclaredIdentifier("jkl")) raises_error

let function_suite =
  "function_suite">:::
  [
    "test_match_Function_Lambda_Return_Type">:: test_match_Function_Lambda_Return_Type;
    "neg_test_match_Function_Lambda_Return_Type">:: neg_test_match_Function_Lambda_Return_Type;
    "test_lambda_arguments_used_in_body">:: test_lambda_arguments_used_in_body;
    "test_lambda_arguments_cannot_be_redefined">:: test_lambda_arguments_cannot_be_redefined;
    "neg_test_emit_Function_Lambda">:: neg_test_emit_Function_Lambda;

    "test_call_with_int_works">:: test_call_with_int_works;
    "test_call_with_str_works">:: test_call_with_str_works;
    "test_call_str_argument_with_int_fails">:: test_call_str_argument_with_int_fails;
    "test_call_str_argument_with_two_strings_fails">:: test_call_str_argument_with_two_strings_fails;

    "test_call_format_with_arbitrary_arguments_works">:: test_call_format_with_arbitrary_arguments_works;
    "test_call_format_with_int_first_fails">:: test_call_format_with_int_first_fails;

    "test_return_wrong_type_fails">:: test_return_wrong_type_fails;

    "test_NoStmtsAllowedAfterReturnStmt_fails">:: test_NoStmtsAllowedAfterReturnStmt_fails;
    "test_return_body_semantic_check_fails">:: test_return_body_semantic_check_fails;

    "test_thing_being_called_is_function_semantic_check_fails">:: test_thing_being_called_is_function_semantic_check_fails;
    "test_undefined_fun_arg_fails">:: test_undefined_fun_arg_fails;
    "test_check_All_Args_match_the_types_and_positions_fails">:: test_check_All_Args_match_the_types_and_positions_fails;
  ]
;;
