open OUnit2;;
open Osc;;
open Ast;;
open Sast;;
open Semantic_checker_test_utils;;


let test_unit_literal _test_ctxt = 
  let expected = SIntType ,SBinop((SIntType, SIntLiteral(9)), Add ,(SIntType , SIntLiteral(4)))
  and _, actual = Osc.check_expr (Osc.expr_of_string "9 + 4") in
  assert_sexpr_equal expected actual;;

let test_string_concatenation _test_ctxt = 
  let expected = SStringType ,SBinop((SStringType, SStringLiteral("Hello ")), Add ,(SStringType , SStringLiteral(" world")))
  and _, actual = Osc.check_expr (Osc.expr_of_string "\"Hello \" + \" world\"") in
  assert_sexpr_equal expected actual;;

let neg_test_string_int_concatenation _test_ctxt = 
  let raises_error () = Osc.check_expr (Osc.expr_of_string "\"Hello\" + 9") in
  assert_raises (Semantic_checker.TypeMismatch((SIntType, SIntLiteral(9)), SStringType)) raises_error

let test_throw_non_numeric _test_ctxt = 
  let raises_error () = Osc.check_expr (Osc.expr_of_string "false + false") in
  assert_raises (Semantic_checker.ExpectedNumeric) raises_error

let test_throw_non_comparable _test_ctxt = 
  let raises_error () = Osc.check_expr (Osc.expr_of_string "false > false") in
  assert_raises (Semantic_checker.ExpectedComparable) raises_error

let test_throw_negate_false _test_ctxt = 
  let raises_error () = Osc.check_expr (Osc.expr_of_string "-false") in
  assert_raises (Semantic_checker.NegationError) raises_error

let test_logical_negation_pos _test_ctxt = 
  let expected = SBoolType ,SUnop(Not,(SBoolType , SBoolLiteral(false)))
  and _, actual = Osc.check_expr (Osc.expr_of_string "!false") in
  assert_sexpr_equal expected actual;;

let test_logical_negation_neg _test_ctxt = 
  let raises_error () = Osc.check_expr (Osc.expr_of_string "!4") in
  assert_raises (Semantic_checker.ExpectedBoolean( (SIntType, SIntLiteral(4)))) raises_error

let test_assignemnt_neg _test_ctxt = 
  let raises_error () = Osc.check_stmt (Osc.stmt_of_string "let x: int = (3 + true);") in
  assert_raises (Semantic_checker.TypeMismatch( (SBoolType, SBoolLiteral(true)), SIntType )) raises_error

let test_reference_argument_neg _test_ctxt = 
  let raises_error () = Osc.check_stmt (Osc.stmt_of_string "let x: int = false;") in
  assert_raises  (Semantic_checker.TypeMismatch( (SBoolType, SBoolLiteral(false)), SIntType )) raises_error

let aritmetic_suite =
  "aritmetic_suite">:::
  [
    "test_unit_literal">:: test_unit_literal;
    "test_throw_non_comparable">:: test_throw_non_comparable;
    "test_string_concatenation">:: test_string_concatenation;
    "neg_test_string_int_concatenation">:: neg_test_string_int_concatenation;
    "test_throw_non_numeric">:: test_throw_non_numeric;
    "test_throw_negate_false">::  test_throw_negate_false;
    "test_logical_negation_pos">:: test_logical_negation_pos;
    "test_logical_negation_neg">:: test_logical_negation_neg;
    "test_assignemnt_neg">:: test_assignemnt_neg;
    "test_reference_argument_neg">:: test_reference_argument_neg;
  ]
;;
