open OUnit2;;
open Core;;
open Osc;;
open Ast;;
open Sast;;

exception ExpectedSBlock


let assert_sprogram_equal =
  assert_equal ~printer:Fmtlib.code_of_sprogram

let assert_sexpr_equal =
  assert_equal ~printer:Fmtlib.code_of_sexpr

let assert_sdecl_equal =
  assert_equal ~printer:Fmtlib.code_of_sdecl

let assert_sdecls_equal = assert_sprogram_equal

let assert_sstmt_equal =
  assert_equal ~printer:Fmtlib.code_of_sstmt


let rec assert_sblocks_equal lsblock rsblock =
  let lstmts = match lsblock with 
    | SBlock(sstmts) -> sstmts 
    | _ -> raise ExpectedSBlock
  in

  let rstmts = match rsblock with 
    | SBlock(sstmts) -> sstmts 
    | _ -> raise ExpectedSBlock
  in

  let _ = List.iter2 lstmts rstmts ~f:(fun l r -> assert_sstmt_equal l r) in
  assert_equal true true


let declare_function_of_string ?env (fn_src: string) : (Semantic_checker_env.t * resolved_typename) =
  let env = match env with
  | Some(env) -> env
  | None -> 
    let scope = (Semantic_checker_env.FunctionScope(Ast.UnitType)) in
    Semantic_checker_env.new_scope Semantic_checker_env.empty
  in

  let fn = Osc.decl_of_string fn_src in
  let env, fn_sdecls = Semantic_checker._check_decl env fn in
  let fntype = match fn_sdecls with
    | [SVariable(_, fntype, _)] -> fntype
    | _ -> raise (Failure "Wrong type matched.")
  in
  env, fntype


let declare_actor_of_string ?env (actor_src: string) : (Semantic_checker_env.t * resolved_typename) =
  declare_function_of_string ?env actor_src
