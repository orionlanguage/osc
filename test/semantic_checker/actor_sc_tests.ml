open OUnit2;;
open Osc;;
open Ast;;
open Sast;;
open Semantic_checker_test_utils;;
open Semantic_checker;;


(*Actor Lambdas*)
let test_match_Actor_Lambda_Return_Type _test_ctxt = 
  let expected_type =  SActorType(["i", SIntType], SIntType) in
  let expected = SVariable("success_actor", expected_type, Some(expected_type, SActorLambda(SBlock([ SEmit( SIntType, SIntLiteral(1)) ]))))
  and _, actual = Osc.check_decl (Osc.decl_of_string "actor success_actor(i : int) -> int { emit 1; }") in
  assert_sdecls_equal [expected] actual;;

let neg_test_match_Actor_Lambda_Return_Type _test_ctxt = 
  let raises_error () = Osc.check_decl (Osc.decl_of_string "actor success_actor(i : int) -> int { emit 1.1; }") in
  assert_raises (Semantic_checker.TypeMismatch((SFloatType, SFloatLiteral(1.1)), SIntType)) raises_error

let neg_test_return_Actor_Lambda _test_ctxt = 
  let raises_error () = Osc.check_decl (Osc.decl_of_string "actor success_actor(i : int) -> int { return 1.1; }") in
  assert_raises (Semantic_checker.CantReturnFromActor) raises_error


let test_call_actor_with_correct_argument_works _test_ctxt = 
  let env, actor_t = declare_actor_of_string "actor id(i: int) -> int { emit i; }" in
  let call = Osc.expr_of_string "id(42)" in
  let _, actual = Semantic_checker._check_expr env call in
  let expected : sexpr = SIntType, SActorCall((actor_t, SIdentifier("id")), [SIntType, SIntLiteral(42)]) in
  assert_sexpr_equal expected actual


let test_call_actor_with_wrong_argument_type_fails _test_ctxt = 
  let env, actor_t = declare_actor_of_string "actor id(i: int) -> int { emit i; }" in
  let call = Osc.expr_of_string "id(\"Hello\")" in
  let raises_error () = Semantic_checker._check_expr env call in
  assert_raises (Semantic_checker.InvalidArguments "Wrong argument type to 'id'") raises_error


let actor_suite =
  "actor_suite">:::
  [
    "test_match_Actor_Lambda_Return_Type">:: test_match_Actor_Lambda_Return_Type;
    "neg_test_match_Actor_Lambda_Return_Type">:: neg_test_match_Actor_Lambda_Return_Type;
    "neg_test_return_Actor_Lambda">:: neg_test_return_Actor_Lambda;

    "test_call_actor_with_correct_argument_works">:: test_call_actor_with_correct_argument_works;
    "test_call_actor_with_wrong_argument_type_fails">:: test_call_actor_with_wrong_argument_type_fails;
  ]
;;
