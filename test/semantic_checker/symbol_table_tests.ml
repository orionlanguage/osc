open OUnit2;;
open Osc;;
open Ast;;
open Sast;;
open Semantic_checker_env;;
open Semantic_checker_test_utils;;

module StringMap = Semantic_checker_env.SymbolTable.SymbolMap.Map;;
open SymbolTable;;


let assert_symbol_table_equal =
    assert_equal ~printer:SymbolTable.to_string


let make_symbol_table (l : ((string * resolved_typename) list)) : SymbolTable.t =
  let add_symbol map (i, t) = StringMap.add i ({identifier=i;typename=t; defined=false}: symbol) map in
  let items = List.fold_left add_symbol StringMap.empty l in
  {parent=None; values=items}


let test_empty_constructor _test_ctxt = 
  let expected: SymbolTable.t = {parent=None; values=StringMap.empty}
  and actual = SymbolTable.empty in
  assert_symbol_table_equal expected actual;;


let test_add_works_with_empty_table _test_ctxt = 
  let table = SymbolTable.empty in
  let symbol: SymbolTable.symbol = {identifier="x"; typename=SUnitType; defined=false} in
  let expected = (make_symbol_table [("x", SUnitType)])
  and actual = SymbolTable.add symbol table in
  assert_symbol_table_equal expected actual;;


let test_add_twice_works_fine_if_same_type _test_ctxt = 
  let table = SymbolTable.empty in
  let symbol: SymbolTable.symbol = {identifier="x"; typename=SUnitType; defined=false} in
  let expected = (make_symbol_table [("x", SUnitType)])
  and actual = SymbolTable.add symbol (SymbolTable.add symbol table) in
  assert_symbol_table_equal expected actual;;


let test_add_twice_fails_with_different_type _test_ctxt = 
  let table = SymbolTable.empty in
  let symbol1: SymbolTable.symbol = {identifier="x"; typename=SUnitType; defined=false} in
  let symbol2: SymbolTable.symbol = {identifier="x"; typename=SIntType; defined=false} in
  assert_raises (SymbolTable.RedefinedIdentifier ("x", symbol1)) (fun () -> SymbolTable.add symbol2 (SymbolTable.add symbol1 table))


let symbol_table_suite =
  "symbol_table_suite">:::
  [
    "test_empty_constructor">::test_empty_constructor;
    "test_add_works_with_empty_table">::test_add_works_with_empty_table;
    "test_add_twice_works_fine_if_same_type">::test_add_twice_works_fine_if_same_type;
    "test_add_twice_fails_with_different_type">::test_add_twice_fails_with_different_type;
  ]
;;
