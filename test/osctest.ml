open Parser_tests;;
open Semantic_checker_tests;;
open Integration_tests;;


let main () =
  print_endline "Running parser tests...";
  run_parser_tests ();

  print_endline "Running semantic checker tests...";
  run_semantic_checker_tests ();

  print_endline "Running integration tests...";
  run_integration_tests ();
;;

main ();;
