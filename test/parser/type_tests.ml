open OUnit2;;
open Osc;;
open Ast;;
open Parser_test_utils;;


let test_type_alias_identifier _test_ctxt =
  let expected = Type("Dude", Typename("Person"))
  and actual = Osc.decl_of_string "type Dude = Person;" in
  assert_decl_equal expected actual


let test_alias_function_typename _test_ctxt =
  let arguments = [("s", Typename("str"))] in
  let expected = Type("string_predicate", FunctionTypename(arguments, Typename("bool")))
  and actual = Osc.decl_of_string "type string_predicate = fn(s: str) -> bool;" in
  assert_decl_equal expected actual


let test_struct_declarations _test_ctxt =
  let arguments = [
    ("first_name", Typename("str"));
    ("last_name", Typename("str"));
  ] in
  let expected = Type("Person", StructTypename("Person", arguments))
  and actual = Osc.decl_of_string "type Person = struct(first_name: str, last_name: str);" in
  assert_decl_equal expected actual


let types_suite =
  "types_suite">:::
  [
    "test_type_alias_identifier">:: test_type_alias_identifier;
    "test_alias_function_typename">:: test_alias_function_typename;
    "test_struct_declarations">:: test_struct_declarations;
  ]
;;
