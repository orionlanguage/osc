open OUnit2;;
open Osc;;
open Ast;;
open Parser_test_utils;;


let no_arg_call _test_ctxt =
  let expected = FunctionCall(Identifier("print"), [])
  and actual = Osc.expr_of_string "print()" in
  assert_expr_equal expected actual


let one_arg_call _test_ctxt =
  let expected = FunctionCall(Identifier("double"), [IntLiteral 3])
  and actual = Osc.expr_of_string "double(3)" in
  assert_expr_equal expected actual


let two_arg_call _test_ctxt =
  let expected = FunctionCall(Identifier("add"), [IntLiteral 3; IntLiteral 4])
  and actual = Osc.expr_of_string "add(3, 4)" in
  assert_expr_equal expected actual


let test_call_that_returns_function_parses _test_ctxt =
  let expected = FunctionCall(FunctionCall(Identifier("add"), [IntLiteral 3; IntLiteral 4]), [IntLiteral 5])
  and actual = Osc.expr_of_string "add(3, 4)(5)" in
  assert_expr_equal expected actual


let expressions_suite =
  "expressions_suite">:::
  [
    "no_arg_call">:: no_arg_call;
    "one_arg_call">:: one_arg_call;
    "two_arg_call">:: two_arg_call;
    "test_call_that_returns_function_parses">:: test_call_that_returns_function_parses;
  ]
;;
