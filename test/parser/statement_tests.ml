open OUnit2;;
open Osc;;
open Ast;;
open Parser_test_utils;;


let test_if_statement _test_ctxt = 
  let expected = If([IntLiteral(3), Block([])], Block([]))
  and actual = Osc.stmt_of_string "if 3 {}" in
  assert_stmt_equal expected actual;;

let test_if_statement_body _test_ctxt = 
  let expected = If([IntLiteral(3), Block([Expr(IntLiteral(4))])], Block([]))
  and actual = Osc.stmt_of_string "if 3 { 4; }" in
  assert_stmt_equal expected actual;;

let test_if_else_statement_body _test_ctxt = 
  let expected = If([IntLiteral(3), Block([Expr(IntLiteral(4))])], Block([Expr(IntLiteral(6))]))
  and actual = Osc.stmt_of_string "if 3 { 4; } else { 6; }" in
  assert_stmt_equal expected actual;;

let test_if_elif_statement_body _test_ctxt = 
  let make_branch cond stmt = (IntLiteral(cond), Block([Expr(IntLiteral(stmt))])) in
  let expected = If([(make_branch 3 4); (make_branch 5 6)], Block([]))
  and actual = Osc.stmt_of_string "if 3 { 4; } elif 5 { 6; }" in
  assert_stmt_equal expected actual;;

let test_if_else_if_statement_body _test_ctxt = 
  let make_branch cond stmt = (IntLiteral(cond), Block([Expr(IntLiteral(stmt))])) in
  let expected = If([(make_branch 3 4); (make_branch 5 6)], 
                    Block([Expr(IntLiteral(7))]))
  and actual = Osc.stmt_of_string "if 3 { 4; } elif 5 { 6; } else { 7; }" in
  assert_stmt_equal expected actual;;

let test_if_elif_elif_else_statement_body _test_ctxt = 
  let make_branch cond stmt = (IntLiteral(cond), Block([Expr(IntLiteral(stmt))])) in
  let expected = If([(make_branch 1 2); (make_branch 3 4); (make_branch 5 6)], 
                    Block([Expr(IntLiteral(7))]))
  and actual = Osc.stmt_of_string "if 1 { 2; } elif 3 { 4; } elif 5 { 6; } else { 7; }" in
  assert_stmt_equal expected actual;;

let test_if_if_else_statement_body _test_ctxt = 
  assert_raises ~msg:"Should raise Parser error." (Parsing.Parse_error) (fun () -> Osc.stmt_of_string ~print_error:false "if 3 if 5 { 6; } else { 7; }");;

let test_while_statement _test_ctxt = 
  let expected = While(IntLiteral(5), Block([]))
  and actual = Osc.stmt_of_string "while 5 {}" in
  assert_stmt_equal expected actual;;

let test_for_statement _test_ctxt = 
  let expected = For(("x", Typename("int")), Identifier("list"), Block([]))
  and actual = Osc.stmt_of_string "for x: int in list {}" in
  assert_stmt_equal expected actual;;


let statements_suite =
  "statements_suite">:::
  [
    "test_if_statement">:: test_if_statement;
    "test_if_statement_body">:: test_if_statement_body;
    "test_if_else_statement_body">:: test_if_else_statement_body;
    "test_if_elif_statement_body">:: test_if_elif_statement_body;
    "test_if_else_if_statement_body">:: test_if_else_if_statement_body;
    "test_if_elif_elif_else_statement_body">:: test_if_elif_elif_else_statement_body;
    "test_if_if_else_statement_body">:: test_if_if_else_statement_body;

    "test_while_statement">:: test_while_statement;
    "test_for_statement">:: test_for_statement;
  ]
;;
