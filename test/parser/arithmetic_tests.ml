open OUnit2;;
open Osc;;
open Ast;;
open Parser_test_utils;;


let test_addition _test_ctxt = 
  let expected = Binop(IntLiteral(3), Add, IntLiteral(4))
  and actual = Osc.expr_of_string "3 + 4" in
  assert_expr_equal expected actual;;


let test_left_associative _test_ctxt = 
  let expected = Binop(Binop(IntLiteral(3), Add, IntLiteral(4)), Sub, IntLiteral(5))
  and actual = Osc.expr_of_string "3 + 4 - 5" in
  assert_expr_equal expected actual;;


let test_multiplication_precedence _test_ctxt = 
  let expected = Binop(Binop(IntLiteral(5), Mul, IntLiteral(4)), Sub, Binop(IntLiteral(5), Mul, IntLiteral(3)))
  and actual = Osc.expr_of_string "5 * 4 - 5 * 3" in
  assert_expr_equal expected actual;;


let test_parenthesis_precedence _test_ctxt = 
  let expected = Binop(IntLiteral(5), Mul, Binop(IntLiteral(3), Add, IntLiteral(4)))
  and actual = Osc.expr_of_string "5 * (3 + 4)" in
  assert_expr_equal expected actual;;


let test_parenthesis_precedence2 _test_ctxt = 
  let expected = Binop(Binop(IntLiteral(5), Mul, IntLiteral(3)), Add, IntLiteral(4))
  and actual = Osc.expr_of_string "5 * 3 + 4" in
  assert_expr_equal expected actual;;

let test_unary_minus _test_ctxt = 
  let expected = Binop(IntLiteral(5), Mul, Unop(Neg, IntLiteral(3)))
  and actual = Osc.expr_of_string "5 * -3" in
  assert_expr_equal expected actual;;

let test_unary_minus2 _test_ctxt = 
  let expected = Binop(IntLiteral(3), Add, Unop(Neg, IntLiteral(3)))
  and actual = Osc.expr_of_string "3 + (-3)" in
  assert_expr_equal expected actual;;

let test_bind_operator _test_ctxt = 
  let expected = Binop(Identifier("a"), Bind, Binop(Identifier("b"), Bind, Identifier("c")))
  and actual = Osc.expr_of_string "a => b => c" in
  assert_expr_equal expected actual;;


let arithmetic_suite =
  "arithmetic_suite">:::
  [
    "test_addition">:: test_addition;
    "test_left_associative">:: test_left_associative;
    "test_multiplication_precedence">:: test_multiplication_precedence;
    "test_parenthesis_precedence">:: test_parenthesis_precedence;
    "test_parenthesis_precedence2">:: test_parenthesis_precedence2;
    "test_unary_minus">:: test_unary_minus;
    "test_unary_minus2">:: test_unary_minus2;
    "test_bind_operator">:: test_bind_operator;
  ]
;;
