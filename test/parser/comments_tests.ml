open OUnit2;;
open Osc;;
open Ast;;
open Parser_test_utils;;


let test_comment_after_semicolon _test_ctxt =
  let expected = Type("Dude", Typename("Person"))
  and actual = Osc.decl_of_string "type Dude = Person; // some comment" in
  assert_decl_equal expected actual

let test_comment_on_one_line _test_ctxt =
  let expected = Block([Expr(IntLiteral(3)); Expr(IntLiteral(4))])
  and actual = Osc.stmt_of_string "{ 3; // comment \n 4; }" in
  assert_stmt_equal expected actual


let test_inline_comment _test_ctxt =
  let expected = Block([Expr(IntLiteral(3)); Expr(IntLiteral(4))])
  and actual = Osc.stmt_of_string "{ 3; /* comment */ 4; }" in
  assert_stmt_equal expected actual


let comments_suite =
  "comments_suite">:::
  [
    "test_comment_after_semicolon">:: test_comment_after_semicolon;
    "test_comment_on_one_line">:: test_comment_on_one_line;
    "test_inline_comment">:: test_inline_comment;
  ]
;;
