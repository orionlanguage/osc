open OUnit2;;

open Type_tests;;
open Literals_tests;;
open Declaration_tests;;
open Expression_tests;;
open Statement_tests;;
open Arithmetic_tests;;
open Program_tests;;
open Struct_tests;;
open Comments_tests;;
open Function_tests;;


let run_parser_tests () =
  print_endline "Running literals suite...";
  run_test_tt_main literals_suite;

  print_endline "Running declarations suite...";
  run_test_tt_main declarations_suite;

  print_endline "Running expressions suite...";
  run_test_tt_main expressions_suite;

  print_endline "Running statements suite...";
  run_test_tt_main statements_suite;

  print_endline "Running types suite...";
  run_test_tt_main types_suite;

  print_endline "Running comments suite...";
  run_test_tt_main comments_suite;

  print_endline "Running structs suite...";
  run_test_tt_main structs_suite;

  print_endline "Running functions suite...";
  run_test_tt_main functions_suite;

  print_endline "Running program suite...";
  run_test_tt_main programs_suite;
;;
