/*Sample program to demonstrate the binding of actors, and actor lamda returing functions: */

let filter_minimum: fn(min: int) -> actor(i: int) -> int = fn(min: int) -> actor(i: int) -> int {
    return actor(i: int) -> int {
    	if i < min { 
    		emit i; 
    	} 
    	else {
    		emit -1; 
    	}
    };
};

fn not_two(i: int) -> bool{
	return i != 3; 
}


let iter_emit_first_success_actor: actor() -> int = actor() -> int{
	let nums: [int] = [1,2,3,4,5,6,7];

	for x: int in nums {
		let should_emit: bool = not_two(x);

		if should_emit == true {
			emit x;
		}
	}
};

actor success_actor(i : int) -> str {
	if i != -1 { emit "Success"; } 
	else { emit "Failure"; }
}

fn main(args: [str]) -> () {
	iter_emit_first_success_actor => filter_minimum => success_actor; 
}