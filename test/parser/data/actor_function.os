fn filter_minimum(min: int) -> actor(i: int) -> int {
    return actor(i: int) -> int {
    	if i < min { 
    		emit i; 
    	} 
    	else {
    		emit -1; 
    	}
    };
}