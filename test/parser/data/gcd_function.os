fn gcd(a: int, b: int) -> int{
	let c: int;

	while a != 0 {
		c = a;
		a = b % a;
		b = c;
	}
	return b;
} 