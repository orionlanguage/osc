open OUnit2;;
open Osc;;
open Ast;;
open Parser_test_utils;;


let test_struct_dot_operator_works_with_identifiers _test_ctxt =
  let expected = Binop(Identifier("deniro"), Dot, Identifier("name"))
  and actual = Osc.expr_of_string "deniro.name" in
  assert_expr_equal expected actual


let test_nested_struct_dereference _test_ctxt =
  let expected = Binop(Binop(Identifier("deniro"), Dot, Identifier("name")), Dot, Identifier("first"))
  and actual = Osc.expr_of_string "deniro.name.first" in
  assert_expr_equal expected actual


let test_struct_dot_operator_works_with_function_calls _test_ctxt =
  let expected = Binop(FunctionCall(Identifier("Person"), []), Dot, Identifier("name"))
  and actual = Osc.expr_of_string "Person().name" in
  assert_expr_equal expected actual


let test_struct_dot_operator_works_with_function_calls_after_the_dot _test_ctxt =
  let expected = FunctionCall(Binop(FunctionCall(Identifier("Person"), []), Dot, Identifier("speak")), [])
  and actual = Osc.expr_of_string "Person().speak()" in
  assert_expr_equal expected actual


let test_struct_dot_operator_assignment _test_ctxt =
  let expected = Assignment(Binop(Identifier("deniro"), Dot, Identifier("name")), StringLiteral("Robert DeNiro"))
  and actual = Osc.expr_of_string "deniro.name = \"Robert DeNiro\"" in
  assert_expr_equal expected actual


let structs_suite =
  "structs_suite">:::
  [
    "test_struct_dot_operator_works_with_identifiers">:: test_struct_dot_operator_works_with_identifiers;
    "test_nested_struct_dereference">:: test_nested_struct_dereference;
    "test_struct_dot_operator_works_with_function_calls">:: test_struct_dot_operator_works_with_function_calls;
    "test_struct_dot_operator_works_with_function_calls_after_the_dot">:: test_struct_dot_operator_works_with_function_calls_after_the_dot;
    "test_struct_dot_operator_assignment">:: test_struct_dot_operator_assignment;
  ]
;;
