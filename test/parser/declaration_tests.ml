open OUnit2;;
open Osc;;
open Ast;;
open Parser_test_utils;;


let test_declare_unit _test_ctxt = 
  let expected = Variable("x", UnitType, None)
  and actual = (Osc.decl_of_string "let x: ();") in
  assert_decl_equal expected actual;;

let test_declare_assign_unit _test_ctxt = 
  let expected = Variable("x", UnitType, Some UnitLiteral)
  and actual = (Osc.decl_of_string "let x: () = ();") in
  assert_decl_equal expected actual;;


let test_declare_int _test_ctxt = 
  let expected = Variable("x", Typename("int"), None)
  and actual = (Osc.decl_of_string "let x: int;") in
  assert_decl_equal expected actual;;

let test_declare_assign_int _test_ctxt = 
  let expected = Variable("x", Typename("int"), Some(IntLiteral(9)))
  and actual = Osc.decl_of_string "let x: int = 9;" in
  assert_decl_equal expected actual;;


let test_declare_int_list _test_ctxt = 
  let expected = Variable("l", ListTypename(Typename("int")), None)
  and actual = Osc.decl_of_string "let l: [int];" in
  assert_decl_equal expected actual;;

let test_declare_nested_int_list _test_ctxt = 
  let expected = Variable("l", ListTypename(ListTypename(Typename("int"))), None)
  and actual = Osc.decl_of_string "let l: [[int]];" in
  assert_decl_equal expected actual;;

let test_declare_empty_list_typename_fails _test_ctxt = 
  let should_raise = fun () -> Osc.decl_of_string ~print_error:false "let l: [];" in
  assert_raises ~msg:"Should raise Parser error." (Parsing.Parse_error) should_raise;;


let test_declare_no_argument_function _test_ctxt = 
  let expected = Variable("nothing", FunctionTypename([], Typename("int")), None)
  and actual = Osc.decl_of_string "fn nothing() -> int;" in
  assert_decl_equal expected actual;;

let test_declare_single_argument_function _test_ctxt = 
  let expected = Variable("id", FunctionTypename(["i", Typename("int")], Typename("int")), None)
  and actual = Osc.decl_of_string "fn id(i: int) -> int;" in
  assert_decl_equal expected actual;;

let test_declare_single_argument_function_without_argname _test_ctxt = 
  let expected = Variable("id", FunctionTypename([("", Typename("int")); ("", Typename("int"))], Typename("int")), None)
  and actual = Osc.decl_of_string "let id: fn(int, int) -> int;" in
  assert_decl_equal expected actual;;

let test_declare_function_that_takes_another_function _test_ctxt = 
  let inner = FunctionTypename(["", Typename("str")], Typename("bool")) in
  let expected = Variable("filter", FunctionTypename(["f", inner], Typename("bool")), None)
  and actual = Osc.decl_of_string "fn filter(f: fn(str) -> bool) -> bool;" in
  assert_decl_equal expected actual;;

let test_declare_nothing_function _test_ctxt = 
  let expected_type = FunctionTypename([], UnitType) in
  let expected = Variable("nothing", 
                          expected_type,
                          Some(FunctionLambda(expected_type, 
                                              Block([]))))
  and actual = Osc.decl_of_string "fn nothing() -> () {}" in
  assert_decl_equal expected actual;;

let test_declare_nothing_actor _test_ctxt = 
  let expected_type = ActorTypename([], UnitType) in
  let expected = Variable("nothing", 
                          expected_type,
                          Some(ActorLambda(expected_type, 
                                              Block([]))))
  and actual = Osc.decl_of_string "actor nothing() -> () {}" in
  assert_decl_equal expected actual;;

let test_declare_lambda _test_ctxt = 
  let expected = Variable("nothing", FunctionTypename([], UnitType), None)
  and actual = Osc.decl_of_string "let nothing: fn() -> ();" in
  assert_decl_equal expected actual;;

let test_declare_assign_lambda _test_ctxt = 
  let expected = Variable("nothing",
                          FunctionTypename([], UnitType),
                          Some(FunctionLambda(FunctionTypename([], UnitType),
                                              Block([]))))
  and actual = Osc.decl_of_string "let nothing: fn() -> () = fn() -> () {};" in
  assert_decl_equal expected actual;;

let test_declare_assign_lambda_with_return _test_ctxt = 
  let expected = Variable("nothing", 
                          FunctionTypename([], UnitType), 
                          Some(FunctionLambda(FunctionTypename([], UnitType),
                                              Block([Return(UnitLiteral)]))))
  and actual = Osc.decl_of_string "let nothing: fn() -> () = fn() -> () { return (); };" in
  assert_decl_equal expected actual;;

let test_lambda_declaration_same_as_fn _test_ctxt = 
  let lambda = Osc.decl_of_string "fn nothing() -> () {}"
  and fn = Osc.decl_of_string "let nothing: fn() -> () = fn() -> () {};" in
  assert_decl_equal lambda fn

let test_declare_functions_order_preserved _test_ctxt = 
  let expected_type = FunctionTypename([("x", Typename("int")); ("y", Typename("int"))], Typename("int")) in
  let op name = Variable(name, expected_type, None) in
  let expected = [op "add"; op "sub"; op "mul"; op "div"] in
  assert_equal expected (Osc.program_of_filepath "./test/parser/data/decls.os")


let declarations_suite =
  "declarations_suite">:::
  [
    "test_declare_unit">:: test_declare_unit;
    "test_declare_assign_unit">:: test_declare_assign_unit;

    "test_declare_int">:: test_declare_int;
    "test_declare_assign_int">:: test_declare_assign_int;
    "test_declare_no_argument_function">:: test_declare_no_argument_function;
    "test_declare_single_argument_function">:: test_declare_single_argument_function;
    "test_declare_single_argument_function_without_argname">:: test_declare_single_argument_function_without_argname;
    "test_declare_function_that_takes_another_function">:: test_declare_function_that_takes_another_function;
    "test_declare_nothing_function">:: test_declare_nothing_function;
    "test_declare_nothing_actor">:: test_declare_nothing_actor;

    "test_declare_int_list">:: test_declare_int_list;
    "test_declare_nested_int_list">:: test_declare_nested_int_list;
    "test_declare_empty_list_typename_fails">:: test_declare_empty_list_typename_fails;

    "test_declare_lambda">:: test_declare_lambda;
    "test_declare_assign_lambda">:: test_declare_assign_lambda;
    "test_declare_assign_lambda_with_return">:: test_declare_assign_lambda_with_return;
    "test_lambda_declaration_same_as_fn">:: test_lambda_declaration_same_as_fn;
    "test_declare_functions_order_preserved">:: test_declare_functions_order_preserved;
  ]
;;


