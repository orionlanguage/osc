open OUnit2;;
open Osc;;
open Ast;;
open Parser_test_utils;;


(* Literal Tests *)

(* Unit literal *)
let test_unit_literal _test_ctxt = 
  let expected = UnitLiteral
  and actual = Osc.expr_of_string "()" in
  assert_expr_equal expected actual;;


(* Boolean literal *)
let test_true_literal _test_ctxt = 
  let expected = BoolLiteral(true)
  and actual = Osc.expr_of_string "true" in
  assert_expr_equal expected actual;;

let test_false_literal _test_ctxt = 
  let expected = BoolLiteral(false)
  and actual = Osc.expr_of_string "false" in
  assert_expr_equal expected actual;;

(* Integer literals *)
let test_int_literal _test_ctxt = 
  let expected = IntLiteral(9)
  and actual = Osc.expr_of_string "9" in
  assert_expr_equal expected actual;;

let test_large_int_literal _test_ctxt = 
  let expected = IntLiteral(323493420)
  and actual = Osc.expr_of_string "323493420" in
  assert_expr_equal expected actual;;

let test_large_int_literal_file _test_ctxt = 
  let expected = IntLiteral(1234567890)
  and actual = Osc.expr_of_filepath "./test/parser/data/literal.os" in
  assert_expr_equal expected actual;;


(* Floating point literals *)
let test_float_literal_with_only_decimal _test_ctxt = 
  let expected = FloatLiteral(9.0)
  and actual = Osc.expr_of_string "9." in
  assert_expr_equal expected actual;;

let test_float_literal _test_ctxt = 
  let expected = FloatLiteral(9.1)
  and actual = Osc.expr_of_string "9.1" in
  assert_expr_equal expected actual;;

let test_two_dots_invalid _test_ctxt = 
  assert_raises ~msg:"Should raise Scanner error." (Scanner.SyntaxError("Invalid float literal '9.1.1'")) (fun () -> Osc.expr_of_string "9.1.1");;

let test_two_dots_no_number_invalid _test_ctxt = 
  assert_raises ~msg:"Should raise Scanner error." (Scanner.SyntaxError("Invalid float literal '9..1'")) (fun () -> Osc.expr_of_string "9..1");;


(* String literals *)
let test_empty_string_literal _test_ctxt = 
  let expected = StringLiteral("")
  and actual = Osc.expr_of_string "\"\"" in
  assert_expr_equal expected actual;;

let test_hello_world_literal _test_ctxt = 
  let expected = StringLiteral("Hello, world!")
  and actual = Osc.expr_of_string "\"Hello, world!\"" in
  assert_expr_equal expected actual;;

let test_simple_escapes_string_literal _test_ctxt = 
  let expected = StringLiteral("Hello\n\\world!")
  and actual = Osc.expr_of_string "\"Hello\\n\\\\world!\"" in
  assert_expr_equal expected actual;;

let test_quote_escapes_string_literal _test_ctxt = 
  let expected = StringLiteral("Hello, \"John\"!")
  and actual = Osc.expr_of_string "\"Hello, \\\"John\\\"!\"" in
  assert_expr_equal expected actual;;

let test_newline_inside_string_raises_error _test_ctxt = 
  assert_raises ~msg:"Should raise Scanner error." (Scanner.SyntaxError("Newline inside of a string literal")) (fun () -> Osc.expr_of_string "\"Hello\n, World!\"");;

let test_eof_inside_string_raises_error _test_ctxt = 
  assert_raises ~msg:"Should raise Scanner error." (Scanner.SyntaxError("EOF inside of a string literal")) (fun () -> Osc.expr_of_string "\"Hello, ");;


(* Byte literals *)
let test_byte_literal_a _test_ctxt = 
  let expected = ByteLiteral('a')
  and actual = Osc.expr_of_string "'a'" in
  assert_expr_equal expected actual;;

let test_byte_literal_A _test_ctxt = 
  let expected = ByteLiteral('A')
  and actual = Osc.expr_of_string "'A'" in
  assert_expr_equal expected actual;;

let test_escaped_byte_literal_newline _test_ctxt = 
  let expected = ByteLiteral('\n')
  and actual = Osc.expr_of_string "'\\n'" in
  assert_expr_equal expected actual;;

(* List literals *)
let test_empty_list _test_ctxt = 
  let expected = ListLiteral([])
  and actual = Osc.expr_of_string "[]" in
  assert_expr_equal expected actual;;

let test_single_item_list _test_ctxt = 
  let expected = ListLiteral([IntLiteral(3)])
  and actual = Osc.expr_of_string "[3]" in
  assert_expr_equal expected actual;;

let test_multiple_item_list _test_ctxt = 
  let expected = ListLiteral([IntLiteral(3); IntLiteral(4); IntLiteral(5)])
  and actual = Osc.expr_of_string "[3, 4, 5]" in
  assert_expr_equal expected actual;;

(* Function literals *)
let test_trivial_lambda_function _test_ctxt = 
  let expected = FunctionLambda(FunctionTypename([], UnitType), Block([]))
  and actual = Osc.expr_of_string "fn() -> () {}" in
  assert_expr_equal expected actual;;

let test_trivial_lambda_actor _test_ctxt = 
  let expected = ActorLambda(ActorTypename([], UnitType), Block([]))
  and actual = Osc.expr_of_string "actor() -> () {}" in
  assert_expr_equal expected actual;;


let literals_suite =
  "literals_suite">:::
  [
    "test_unit_literal">:: test_unit_literal;

    "test_true_literal">:: test_true_literal;
    "test_false_literal">:: test_false_literal;

    "test_int_literal">:: test_int_literal;
    "test_large_int_literal">:: test_large_int_literal;
    "test_large_int_literal_file">:: test_large_int_literal_file;

    "test_trivial_lambda_function">:: test_trivial_lambda_function;

    "test_trivial_lambda_actor">:: test_trivial_lambda_actor;

    "test_float_literal_with_only_decimal">:: test_float_literal_with_only_decimal;
    "test_two_dots_invalid">:: test_two_dots_invalid;
    "test_two_dots_no_number_invalid">:: test_two_dots_no_number_invalid;

    "test_empty_string_literal">:: test_empty_string_literal;
    "test_hello_world_literal">:: test_hello_world_literal;
    "test_simple_escapes_string_literal">:: test_simple_escapes_string_literal;
    "test_quote_escapes_string_literal">:: test_quote_escapes_string_literal;
    "test_newline_inside_string_raises_error">:: test_newline_inside_string_raises_error;
    "test_eof_inside_string_raises_error">:: test_eof_inside_string_raises_error;

    "test_empty_list">:: test_empty_list;
    "test_single_item_list">:: test_single_item_list;
    "test_multiple_item_list">:: test_multiple_item_list;

    "test_byte_literal_a">:: test_byte_literal_a;
    "test_byte_literal_A">:: test_byte_literal_A;
    "test_escaped_byte_literal_newline">:: test_escaped_byte_literal_newline;
  ]
;;
