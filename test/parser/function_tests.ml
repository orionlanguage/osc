open OUnit2;;
open Osc;;
open Ast;;
open Parser_test_utils;;

(* Tests simple function return*)
let test_declare_define_single_argument_function_return_INT_LITERAL _test_ctxt = 
  let expected_type = FunctionTypename(["i", Typename("int")], Typename("int")) in
  let expected = Variable("id", expected_type,Some(FunctionLambda(expected_type, Block([ Return(IntLiteral(1)) ]))))
  and actual = Osc.decl_of_string "fn id(i: int) -> int { return 1; }" in
  assert_decl_equal expected actual;;


let test_function_call_that_returns_function_parses _test_ctxt = 
  let expected_type = FunctionTypename(["i", Typename("int")], Typename("int")) in
  let expected = Variable("id", expected_type,Some(FunctionLambda(expected_type, Block([ Return(IntLiteral(1)) ]))))
  and actual = Osc.decl_of_string "fn id(i: int) -> int { return 1; }" in
  assert_decl_equal expected actual;;

(* Takes two arguments: a string and an int
fn filter(f: fn(s: str) -> bool, i: int) -> bool{
  return true;
}*)
let test_declare_define_two_argument_function_return_BOOL_LITERAL _test_ctxt = 
  let inner = FunctionTypename(["s", Typename("str")], Typename("bool")) in
    let expected = Variable("filter", FunctionTypename(["f", inner; "i", Typename("int")],Typename("bool")), Some(FunctionLambda((FunctionTypename(["f", inner; "i", Typename("int")],Typename("bool")), Block( [Return(BoolLiteral(true))] )))))
    and actual = Osc.decl_of_string "fn filter(f: fn(s: str) -> bool, i: int) -> bool { return true; }" in
    assert_decl_equal expected actual;;

(* Takes two arguments: a string and a string, then returns f(s) !!Will comment back in when the shift conflict is resolved!!
fn filter(f: fn(s: str) -> bool, st: str) -> bool{
  return f(st);
}*)

let test_declare_define_two_argument_function_return_Function_Call _test_ctxt = 
  let inner = FunctionTypename(["s", Typename("str")], Typename("bool")) in
    let expected = Variable("filter", FunctionTypename(["f", inner; "st", Typename("str")],Typename("bool")), Some(FunctionLambda((FunctionTypename(["f", inner; "st", Typename("str")],Typename("bool")), Block( [ Return(FunctionCall(Identifier("f"),[Identifier("st")])) ])))))
    and actual = Osc.decl_of_string "fn filter(f: fn(s: str) -> bool, st: str) -> bool { return f(st); }" in 
    assert_decl_equal expected actual;;

let test_gcd_FUNCTION _test_ctxt = 
  let c_variable = Declaration(Variable("c", Typename("int"), None)) in 
  let gcd_funtion_type = FunctionTypename(["a", Typename("int"); "b", Typename("int")],Typename("int")) in
  let c_a_dec = Expr(Assignment(Identifier("c"), Identifier("a"))) in 
  let a_mod_dec = Expr(Assignment(Identifier("a"),Binop(Identifier("b"),Mod, Identifier("a")))) in 
  let c_to_a = Expr(Assignment(Identifier("b"),Identifier("c"))) in
  let while_loop = While(Binop(Identifier("a"), Neq, IntLiteral(0)), Block([c_a_dec;a_mod_dec;c_to_a])) in
    let expected = Variable("gcd", gcd_funtion_type, Some(FunctionLambda((gcd_funtion_type, Block( [c_variable; while_loop; Return(Identifier("b"))] )))))
    and actual = Osc.decl_of_filepath "./test/parser/data/gcd_function.os" in
    assert_decl_equal expected actual;;

(*Tests returning Anonyomous Actor from function*)
let test_actor_FUNCTION _test_ctxt = 
  let actor_funtion_type = FunctionTypename(["min", Typename("int")],ActorTypename(["i",Typename("int")], Typename("int"))) in
  let if_else_st = If([(Binop(Identifier("i"), Less, Identifier("min")), Block([Emit(Identifier("i"))]))],Block([Emit(Unop(Neg,IntLiteral(1)))])) in
  let actor_return = ActorLambda(ActorTypename(["i",Typename("int")], Typename("int")), Block([if_else_st])) in
    let expected = Variable("filter_minimum", actor_funtion_type, Some(FunctionLambda((actor_funtion_type, Block( [ Return(actor_return)] )))))
    and actual = Osc.decl_of_filepath "./test/parser/data/actor_function.os" in
    assert_decl_equal expected actual;;

(*Negative function tests: *)

let test_unclosed_actor_lambda _test_ctxt = 
  assert_raises ~msg:"Should raise Parser error." (Parsing.Parse_error) (fun () -> Osc.decl_of_string ~print_error:false "fn filter_minimum(min: int) -> actor(i: int) -> int { return actor(i: int) -> int { if i < min { emit i; } else { emit -1; }}}");;

let test_incorrect_fn_order _test_ctxt = 
  assert_raises ~msg:"Should raise Parser error." (Parsing.Parse_error) (fun () -> Osc.decl_of_string ~print_error:false "let fn filter_minimum(min: int) -> actor(i: int) -> int { return actor(i: int) -> int { if i < min { emit i; } else { emit -1; }};}");;

let test_no_argument_type _test_ctxt = 
  assert_raises ~msg:"Should raise Parser error." (Parsing.Parse_error) (fun () -> Osc.decl_of_string ~print_error:false "fn filter_minimum(min) -> actor(i: int) -> int { return actor(i: int) -> int { if i < min { emit i; } else { emit -1; }};}");;


let functions_suite =
  "functions_suite">:::
  [
    "test_declare_define_single_argument_function_return_INT_LITERAL">:: test_declare_define_single_argument_function_return_INT_LITERAL;
    "test_declare_define_two_argument_function_return_BOOL_LITERAL">:: test_declare_define_two_argument_function_return_BOOL_LITERAL; 
    "test_gcd_FUNCTION">:: test_gcd_FUNCTION;
    "test_actor_FUNCTION">::test_actor_FUNCTION;
    "test_unclosed_actor_lambda">::test_unclosed_actor_lambda;
    "test_incorrect_fn_order">::test_incorrect_fn_order;
    "test_no_argument_type">::test_no_argument_type;
    "test_declare_define_two_argument_function_return_Function_Call">::test_declare_define_two_argument_function_return_Function_Call;
  ]
;;
