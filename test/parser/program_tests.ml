open OUnit2;;
open Osc;;
open Ast;;
open Parser_test_utils;;

let test_actor_FUNCTION _test_ctxt = 
	let actor_funtion_type = FunctionTypename(["min", Typename("int")],ActorTypename(["i",Typename("int")], Typename("int"))) in
	let if_else_st = If([(Binop(Identifier("i"), Less, Identifier("min")), Block([Emit(Identifier("i"))]))],Block([Emit(Unop(Neg,IntLiteral(1)))])) in
	let actor_return = ActorLambda(ActorTypename(["i",Typename("int")], Typename("int")), Block([if_else_st])) in
	let filter_minimum_function = Variable("filter_minimum", actor_funtion_type, Some(FunctionLambda((actor_funtion_type, Block( [ Return(actor_return)] ))))) in
	let not_two_function = Variable("not_two", FunctionTypename(["i", Typename("int")],Typename("bool")), Some(FunctionLambda((FunctionTypename(["i", Typename("int")],Typename("bool")), Block( [ Return(Binop(Identifier("i"), Neq, IntLiteral(3)))] ))))) in
    (*Iter actor*)
    let actor_funtion_type = ActorTypename([], Typename("int")) in
    let num_list = Declaration(Variable("nums", ListTypename(Typename("int")),Some(ListLiteral([IntLiteral(1);IntLiteral(2);IntLiteral(3);IntLiteral(4);IntLiteral(5);IntLiteral(6);IntLiteral(7)])))) in
    (*For Loop and if statement*)
	let should_emit_var = Declaration(Variable("should_emit", Typename("bool"),Some(FunctionCall(Identifier("not_two"),[Identifier("x")])))) in
	let if_branch = If(([(Binop(Identifier("should_emit"), Equal, BoolLiteral(true)),Block([Emit(Identifier("x"))]))], Block([]))) in 
	let for_loop = For(("x", Typename("int")), Identifier("nums"), Block([ should_emit_var;if_branch])) in 
	(*Actor Lambda*)
	let actor_lambda = ActorLambda(actor_funtion_type, Block([ num_list; for_loop])) in
	let iter_emit_first_success_actor_function = Variable("iter_emit_first_success_actor", actor_funtion_type, Some(actor_lambda)) in
	(*Success Actor*)
	let suc_if_branch = If(([(Binop(Identifier("i"),Neq,Unop(Neg,IntLiteral(1))),Block([Emit(StringLiteral("Success"))]))], Block([Emit(StringLiteral("Failure"))]))) in 
	let success_actor = Variable("success_actor", ActorTypename(["i",Typename("int")], Typename("str")), Some(ActorLambda(ActorTypename(["i",Typename("int")],Typename("str")),Block([suc_if_branch])))) in
	(*Main Program*) 
	let main_function_type = FunctionTypename(["args",ListTypename(Typename("str"))],UnitType) in
	let bind = Expr(Binop(Identifier("iter_emit_first_success_actor"),Bind,(Binop(Identifier("filter_minimum"),Bind,Identifier("success_actor"))))) in
	let main = Variable("main", main_function_type, Some(FunctionLambda(main_function_type, Block([bind])))) in
    	let expected = [filter_minimum_function;not_two_function;iter_emit_first_success_actor_function;success_actor;main]
    	and actual = Osc.program_of_filepath "./test/parser/data/program_test.os" in
    	assert_program_equal expected actual;;

let programs_suite =
  "programs_suite">:::
  [
    "test_actor_FUNCTION">:: test_actor_FUNCTION;
  ]
;;
