open OUnit2;;
open Osc;;
open Ast;;


let assert_expr_equal =
  assert_equal ~printer:Osc.Debug.string_of_expr

let assert_decl_equal =
  assert_equal ~printer:Osc.Debug.string_of_decl

let assert_stmt_equal =
  assert_equal ~printer:Osc.Debug.string_of_stmt

let assert_program_equal =
  assert_equal ~printer:Osc.Debug.string_of_program
