open OUnit2;;

open Integration_simple_tests;;
open Integration_list_tests;;
open Integration_actor_tests;;
open Integration_file_tests;;


let run_integration_tests () =
  print_endline "Running simple suite...";
  run_test_tt_main simple_suite;

  print_endline "Running list suite...";
  run_test_tt_main list_suite;

  print_endline "Running actor suite...";
  run_test_tt_main actor_suite;

  print_endline "Running file suite...";
  run_test_tt_main file_suite;
;;
