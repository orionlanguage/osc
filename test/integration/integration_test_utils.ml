open Core;;
open OUnit2;;
open Osc;;
open Ast;;


let stdout_of_cmd (cmd: string) : string =
  let process = Unix.open_process_in cmd in
  let lines = In_channel.input_lines process in
  let () = In_channel.close process in
  String.concat ~sep:"\n" lines


let compile_and_link_temp compile = 
  let llvm_tempfile = Filename.temp_file "oscar" ".ll" in
  let exe_tempfile = Filename.temp_file "oscar" ".out" in
  let () = compile llvm_tempfile in
  let () = Osc.link_llvm llvm_tempfile exe_tempfile in
  exe_tempfile


let assert_oscar_output (program_source: string) (expected: string) =
  let exe_tempfile = compile_and_link_temp (Osc.compile_oscar_of_string program_source) in
  let stdout = stdout_of_cmd exe_tempfile in
  assert_equal stdout expected


let assert_oscar_filepath_output (program_filename: string) (expected: string) =
  let program_filepath = "./test/integration/data/" ^ program_filename in
  let exe_tempfile = compile_and_link_temp (Osc.compile_oscar_of_filepath program_filepath) in
  let stdout = stdout_of_cmd exe_tempfile in
  assert_equal ~printer:(fun s -> s) expected stdout 


let assert_oscar_filepath_raises (program_filename: string) (error: exn) =
  let program_filepath = "./test/integration/data/" ^ program_filename in
  let raises_error () = compile_and_link_temp (Osc.compile_oscar_of_filepath program_filepath) in
  assert_raises error raises_error
