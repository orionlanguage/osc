type Message = struct(s: str);


fn main() -> int {
  let m: Message = Message("Hello, world!");
  print(m.s);
  return 0;
}
