actor is_even(i: int) -> bool {
  emit (i / 2) * 2 == i;
}

actor printer(b: bool) -> () {
  if b {
    print("True!");
  } else {
    print("False!");
  }
}


fn main() -> int {
  let pipeline: actor(i: int) -> () = is_even => printer;
  pipeline(3);
  pipeline(4);
  pipeline(5);
  return 0;
}
