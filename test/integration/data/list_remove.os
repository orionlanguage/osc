fn main() -> int { 
	let s: [int] = [0,1,2,3,4]; 
	remove(s, 0);
	remove(s, -1);

  for i: int in s {
    print(format("%d", i));
  }

	return 0; 
}
