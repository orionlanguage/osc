type InnerMessage = struct(s2: str);
type Message = struct(s: str, im: InnerMessage);


fn main() -> int {
  let m: Message = Message("Hello, world!", InnerMessage("A different one"));
  m.s = "Actually, this message instead.";
  print(format("%s; %s", m.s, m.im.s2));
  return 0;
}
