fn main() -> int 
{
	let t: bool = true;
	let f: bool = false;

	if t && t {
	   print("true and true");
	}
	if t && f {
	   print("true and false");
	}
	
	elif f && f {
	   print("false and false");
	}

	elif f || f {
	   print("false or false");
	}
	
	elif f || t {
	   print("false or true");
	}
	
	return 0;
}