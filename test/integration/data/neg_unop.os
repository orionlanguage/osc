fn main() -> int {
  let i: int = 3;
  let f: float = 5.0;
  
  print(format("Negative %i is %i, Negative %.0f is %.0f", i, -i, f, -f));

  return 0;
}
