fn main() -> int 
{ 
	if 3 < 4 {
	   print("3 is less than 4");
	}
	
	if 3 > 4 {
	   print("3 is more than 4?");
	}
	
	elif 5 == 10 {
	   print("5 is equal to 10 ?");
	}

	elif 5 != 10 {
	   print("5 is not equal to 10");
	}
	
	return 0;
}