let s: str = "Hello" + " world!";
let i: int = 3 * 30;

fn main() -> int {
  print(format("%s, %d", s, i));
  return 0;
}
