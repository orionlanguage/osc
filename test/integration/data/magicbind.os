actor double(i: int) -> int {
  print(format("In double(%d)", i));
  emit i * 2;
}


actor is_even(i: int) -> bool {
  print(format("In is_even(%d)", i));
  emit (i / 2) * 2 == i;
}


actor printer(b: bool) -> () {
  if b {
    print("True!");
  } else {
    print("False!");
  }
}


fn main() -> int {
	let quadruple: actor(i: int) -> int = double;
  let checker: actor(i: int) -> () = is_even => printer;
	quadruple += checker;
	quadruple += checker;
  print("quadruple(5):");
	quadruple(5);
  return 0;
}
