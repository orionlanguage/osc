fn main() -> int {
  let i: int = 3;
  
  if !(i > 10) {
    print("Less than or equal to 10!");
  }

  return 0;
}
