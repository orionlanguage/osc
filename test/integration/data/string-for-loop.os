fn main() -> int { 
	let a: str = "abc";

	for x: byte in a {
		print(format("%c", x));
	}

	return 0;
}
