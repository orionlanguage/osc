fn hello1() -> int {
  print("Hello from 1!");
  return 1;
}

fn hello2() -> int {
  print("Hello from 2!");
  return 2;
}


fn main() -> int {

  let m: fn() -> int = hello1;
  m;
  m();
  fn hello3() -> int {
    print("Hello from 3!");
    return 3;
  }
  m = hello2;
  m();
  m = hello3;
  m();
  return 0;
}
