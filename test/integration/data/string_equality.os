fn main() -> int {
  let s : str = "Hello, world!";

  if s == "Hello, world!" {
    print("== works");
  }

  if s != "Goodbyte, moon!" {
    print("!= works");
  }

  return 0;
}
