type BestPicture = struct(year: int, title: str);


fn main() -> int {
  let b: BestPicture = BestPicture(1994, "The Shawshank Redemption");
  b.title = "Forrest Gump";
  print(format("%d: %s", b.year, b.title));
  return 0;
}
