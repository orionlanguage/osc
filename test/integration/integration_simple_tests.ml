open OUnit2;;
open Integration_test_utils;;

let test_hello_world_output _test_ctxt = 
  let program_source = "fn main() -> int { print(\"Hello, world!\"); return 0; }" in
  assert_oscar_output program_source "Hello, world!"


let test_hello_world_variables_output _test_ctxt = 
  let program_source = "fn main() -> int { let s: str = \"Hello, world!\"; print(s); return 0; }" in
  assert_oscar_output program_source "Hello, world!"


let test_hello_function_calls _test_ctxt = 
  assert_oscar_filepath_output "call.os" "Hello, world!\nHello, world!"


let test_nested_function_calls _test_ctxt = 
  assert_oscar_filepath_output "nested_functions.os" "Hello, world!\nHello, world!"


let test_struct_hello_world_output _test_ctxt = 
  assert_oscar_filepath_output "hellostruct.os" "Hello, world!"


let test_struct_member_assignment_output _test_ctxt = 
  assert_oscar_filepath_output "struct_member_assignment.os" "1994: Forrest Gump"


let test_nested_structs _test_ctxt = 
  assert_oscar_filepath_output "nested_structs.os" "Actually, this message instead.; A different one"


let test_function_pointers _test_ctxt = 
  assert_oscar_filepath_output "hello-function-pointers.os" "Hello from 1!\nHello from 2!\nHello from 3!"

let test_list_literal_output _test_ctxt = 
  assert_oscar_filepath_output "list.os" "4"

let test_add_output _test_ctxt = 
  assert_oscar_filepath_output "list_ind.os" "2"

let test_if_stmt _test_ctxt = 
  assert_oscar_filepath_output "if_stmt.os" "else"

let test_if_stmt_one_if _test_ctxt = 
  assert_oscar_filepath_output "if_stmt_one_if.os" "else"

let test_if_no_else _test_ctxt = 
  assert_oscar_filepath_output "if_stmt_no_else.os" "First elif"

let test_for _test_ctxt = 
  assert_oscar_filepath_output "for_loop.os" "1111"


(* these are the tests for the submission *)
let test_string_lit _test_ctxt = 
  assert_oscar_filepath_output "1string_lit.os" "Hello World!"


let test_struct _test_ctxt = 
  assert_oscar_filepath_output "3struct.os" "Hello, world!"

let test_int_comp _test_ctxt = 
  assert_oscar_filepath_output "4int_comp.os" "3 is less than 4\n5 is not equal to 10"

let test_str_comp _test_ctxt = 
  assert_oscar_filepath_output "5str_comp.os" "hello == hello"

let test_boolop _test_ctxt = 
  assert_oscar_filepath_output "6boolop.os" "true and true\nfalse or true"

let test_funcall _test_ctxt = 
  assert_oscar_filepath_output "7_funcall.os" "Hello, world!\nHello, world!"

let test_neg_comp _test_ctxt = 
  assert_oscar_filepath_raises "8_comp.os" (Semantic_checker.TypeMismatch((SStringType, SStringLiteral("hello")), SIntType))

let test_neg_struct _test_ctxt = 
  assert_oscar_filepath_raises "10_struct.os" (Semantic_checker.ExpectedStructureMember(Identifier("f")))

let test_string_for_loop _test_ctxt = 
  assert_oscar_filepath_output "string-for-loop.os" "a\nb\nc"

let test_global_initialization _test_ctxt = 
  assert_oscar_filepath_output "global-initialization.os" "Hello world!, 90"

let test_not_operator _test_ctxt = 
  assert_oscar_filepath_output "not_operator.os" "Less than or equal to 10!"

let test_neg_operator _test_ctxt = 
  assert_oscar_filepath_output "neg_unop.os" "Negative 3 is -3, Negative 5 is -5"

let test_string_equality _test_ctxt = 
  assert_oscar_filepath_output "string_equality.os" "== works\n!= works"

let test_file_equality _test_ctxt = 
  assert_oscar_filepath_output "file_equality.os" "== works\n!= works"

let simple_suite =
  "simple_suite">:::
  [
    "test_string_lit">:: test_string_lit;
    "test_struct">:: test_struct;
    "test_int_comp">:: test_int_comp;
    "test_str_comp">:: test_str_comp;
    "test_boolop">:: test_boolop;
    "test_funcall">:: test_funcall;
    "test_neg_comp">:: test_neg_comp;
    "test_neg_struct">:: test_neg_struct;

    "test_hello_world_output">:: test_hello_world_output;
    "test_hello_world_variables_output">:: test_hello_world_variables_output;
    "test_hello_function_calls">:: test_hello_function_calls;
    "test_struct_hello_world_output">:: test_struct_hello_world_output;
    "test_nested_function_calls">:: test_nested_function_calls;
    "test_nested_structs">:: test_nested_structs;
    "test_function_pointers">:: test_function_pointers;
    "test_add_output">:: test_add_output;
    "test_if_stmt">:: test_if_stmt;
    "test_if_stmt_one_if">:: test_if_stmt_one_if;
    "test_if_no_else">:: test_if_no_else;

    "test_string_for_loop">:: test_string_for_loop;
    "test_global_initialization">:: test_global_initialization;
    "test_not_operator">:: test_not_operator;
    "test_neg_operator">:: test_neg_operator;
    "test_string_equality">:: test_string_equality;
    "test_file_equality">:: test_file_equality;
  ]
