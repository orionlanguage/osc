open OUnit2;;
open Integration_test_utils;;


let test_file_open _test_ctxt = 
  assert_oscar_filepath_output "file_open.os" "osc - file"


let file_suite =
  "file_suite">:::
  [
    "test_file_open">:: test_file_open;
  ]
