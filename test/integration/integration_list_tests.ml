open OUnit2;;
open Integration_test_utils;;


let test_list_lit _test_ctxt = 
  assert_oscar_filepath_output "2list_lit.os" "1\n1"


let test_neg_list _test_ctxt = 
  assert_oscar_filepath_raises "9_list.os" (Semantic_checker.ExpectedIntIndex)


let test_intlist_assignment _test_ctxt = 
  assert_oscar_filepath_output "intlist_assignment.os" "1\n42"

let test_strlist_assignment _test_ctxt = 
  assert_oscar_filepath_output "strlist_assignment.os" "Joe Pesci\nTom Hanks"

let test_clear_and_len_test _test_ctxt = 
  assert_oscar_filepath_output "list_clear.os" "0"

let test_list_append_fun_test _test_ctxt = 
  assert_oscar_filepath_output "list_append.os" "4"

let test_list_append_bool_test _test_ctxt = 
  assert_oscar_filepath_output "list_append_bool_list.os" "1\n0\n1\n0"

let test_list_append_float_test _test_ctxt = 
  assert_oscar_filepath_output "list_append_float_list.os" "4"

let test_list_append_string_test _test_ctxt = 
  assert_oscar_filepath_output "list_append_string_list.os" "Elijah Wood"

let test_list_asign _test_ctxt = 
  assert_oscar_filepath_output "list_assign.os" "2" 

let test_list_remove _test_ctxt = 
  assert_oscar_filepath_output "list_remove.os" "1\n2\n3" 

let test_empty_list _test_ctxt = 
  assert_oscar_filepath_output "empty_list.os" "6" 

let test_empty_list_append_len _test_ctxt = 
  assert_oscar_filepath_output "empty_list_append_len.os" "2" 

let list_suite =
  "list_suite">:::
  [
    "test_list_lit">:: test_list_lit;
    "test_neg_list">:: test_neg_list;
    "test_intlist_assignment">:: test_intlist_assignment;
    "test_strlist_assignment">:: test_strlist_assignment;
    "test_clear_and_len_test">:: test_clear_and_len_test;
    "test_list_append_fun_test">:: test_list_append_fun_test;
    "test_list_append_float_test">:: test_list_append_float_test;
    "test_list_append_bool_test">:: test_list_append_bool_test;
    "test_list_append_string_test">:: test_list_append_string_test;
    "test_list_asign">:: test_list_asign;
    "test_list_remove">:: test_list_remove;
    "test_empty_list">:: test_empty_list;
    "test_empty_list_append_len">:: test_empty_list_append_len;
  ]
