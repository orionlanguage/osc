open OUnit2;;
open Integration_test_utils;;


let test_actor_pipeline _test_ctxt = 
  assert_oscar_filepath_output "actor_is_even_pipeline.os" "False!\nTrue!\nFalse!"


let test_magic_bind _test_ctxt = 
  assert_oscar_filepath_output "magicbind.os" "quadruple(5):\nIn double(5)\nIn is_even(10)\nTrue!\nIn is_even(10)\nTrue!"


let actor_suite =
  "actor_suite">:::
  [
    "test_actor_pipeline">:: test_actor_pipeline;
    "test_magic_bind">:: test_magic_bind;
  ]
