Osc
===
The compiler for the Oscar programming language.


## First Steps
1. Install [Opam](https://github.com/realworldocaml/book/wiki/Installation-Instructions#getting-opam)
2. Install [LLVM](http://llvm.org/)
3. Install Core: `opam install core_extended core_bench`
4. Install OCaml LLVM bindings: `opam install llvm`
5. Install OUnit2: `opam install ounit`
6. Install git hooks: `bash .githooks/install.sh`


## Git Flow
To achieve enlightenment with Git, you must use branches intelligently.
Remember - the main purpose of a VCS is to save your ass when you screw up.
Committing broken builds into the `master` branch is breaking the only thing
saving your ass.

Consequently, we use branches. Specifically, the `master` branch should always
be compiling, and its tests should always be green. This is enforced by the
pre-commit script mentioned below. The `development` branch should always be
compiling, but it does not need to be green.

When working on a feature, let's say `closures`, you should do the following:
1. Get the most recent version of the `development` branch with `git pull
   origin development`.
2. Check out the `development` branch with `git checkout development`.
3. Create your own feature branch, e.g. `git checkout -b closures`
4. Write the code and the tests, with frequent commits and meaningful messages
5. Merge your branch into `development`: `git checkout master && git merge
   closures`
6. Update the remote with your changes: `git push origin development`


## Git Hooks
To enforce good behavior, we have some Git hooks set up to force tests to be
run when merging into the `development`/`master` branches. The source for these hooks is
found in the `.githooks` directory.

To install these hooks on your local machine, you have to have a working
/bin/bash, then run

```console
$ bash .githooks/install.sh
```

Now, whenever you commit a change to the `master` directory, you should
see the tests run. If any tests fail, the commit will be rejected. 

Similarly, whenever you commit changes to the `development` directory, a full
rebuild will occur. If the build fails, the commit will be rejected. Tests will
be run for your info, but they are allowed to fail and the commit will still be
accepted.
