" Vim syntax file
" Language: Oscar
" Maintainer: Kevin Lauder
" Latest Revision: 26 April 2008

if exists("b:current_syntax")
  finish
endif

let b:current_syntax = "oscar"

syn keyword oscarKeyword let fn actor state struct
syn keyword oscarKeyword if for while in else elif
syn keyword oscarKeyword type
syn keyword oscarReturn emit return
syn keyword   oscarType int byte str bool float
syn keyword   oscarBoolean     true false
syntax region oscarString start=/\v"/ skip=/\v\\./ end=/\v"/
syn match     oscarFuncName    "\%([^[:cntrl:][:space:][:punct:][:digit:]]\|_\)\%([^[:cntrl:][:punct:][:space:]]\|_\)*" display contained
syn region oscarCommentLine                                                  start="//"                      end="$"
syn region oscarCommentLineDoc                                               start="//\%(//\@!\|!\)"         end="$" 
syn region oscarCommentLineDocError                                          start="//\%(//\@!\|!\)"         end="$"    contained
syn region oscarCommentBlock             matchgroup=oscarCommentBlock         start="/\*\%(!\|\*[*/]\@!\)\@!" end="\*/" 
syn region oscarCommentBlockDoc          matchgroup=oscarCommentBlockDoc      start="/\*\%(!\|\*[*/]\@!\)"    end="\*/"
syn region oscarCommentBlockDocError     matchgroup=oscarCommentBlockDocError start="/\*\%(!\|\*[*/]\@!\)"    end="\*/"
syn region oscarCommentBlockNest         matchgroup=oscarCommentBlock         start="/\*"                     end="\*/"
syn region oscarCommentBlockDocNest      matchgroup=oscarCommentBlockDoc      start="/\*"                     end="\*/"
syn region oscarCommentBlockDocNestError matchgroup=oscarCommentBlockDocError start="/\*"                     end="\*/"
syn match     oscarDecNumber   display "\<[0-9][0-9_]*\%([iu]\%(size\|8\|16\|32\|64\|128\)\)\="
syn match     oscarFuncCall    "\w\(\w\)*("he=e-1,me=e-1
syntax match oscarOperator "\v\=\>"



hi def link oscarDecNumber       oscarNumber
hi def link oscarNumber        Number
hi def link oscarCommentLine   Comment
hi def link oscarCommentLineDoc SpecialComment
hi def link oscarCommentLineDocError Error
hi def link oscarCommentBlock  oscarCommentLine
hi def link oscarCommentBlockDoc oscarCommentLineDoc
hi def link oscarCommentBlockDocError Error
hi def link oscarString        String
hi def link oscarKeyword Keyword
hi def link oscarType          Type
hi def link oscarBoolean       Boolean

hi def link oscarFuncName      Function
hi def link oscarFuncCall      Function

hi def link oscarOperator Operator

hi oscarReturn ctermfg=9 ctermbg=NONE cterm=bold 
