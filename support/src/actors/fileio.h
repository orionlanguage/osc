#pragma once

#include "string.h"
#include "memory.h"
#include <stdio.h>

/*
for file I/O, open, read, write, close (take strings and file descriptor) 
*/

struct file_ {
  int fd;
};

typedef struct file_ file;

file* file_make(int fd);

file* oopen(string* filename, string* mode);
int oclose(file* f);

int oread(file* f, size_t nbyte, string* s);
int owrite(file* f, string* str);

bool file_good(const file* file); // checks if the provided file is valid.
bool file_equal(const file* l, const file* r); // checks if the provided file is valid.
