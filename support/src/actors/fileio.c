#include "fileio.h"
#include "string.h"
#include "memory.h"
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>


file* file_make(int fd) {
  file* result = mmalloc(sizeof(file));
  result->fd = fd;
  return result;
}


file* oopen(string* filename, string* mode) {
  file* my_fp = mmalloc(sizeof(my_fp));
  int fd;

  int fdmode;
  if (strcmp(string_get(mode), "w") == 0) {
    fdmode = O_WRONLY | O_CREAT;
  } else if (strcmp(string_get(mode), "r") == 0) {
    fdmode = O_RDONLY;
  } else if (strcmp(string_get(mode), "a") == 0) {
    fdmode = O_APPEND | O_CREAT;
  } else {
    ERROR("open(%s) failed: unrecognized mode '%s'\n", string_get(filename), string_get(mode));
    return NULL;
  }

  fd = open(string_get(filename), fdmode, 0777);
  if (fd == -1) {
    ERROR("open(%s) failed: %s\n", string_get(filename), strerror(errno));
    return NULL;
  }

  my_fp -> fd = fd;
  return my_fp;
}


int oclose(file* f) {
  int res;
  res = close(f->fd);
  mfree(f);
  return res;
}


int owrite(file* f, string* str) {
  return write(f->fd, string_get(str), str->size);
}


int oread(file* f, size_t nbyte, string* s) {
  vector_reserve(s, nbyte);
  int bytes_read = read(f->fd, s->data, nbyte);

  int nul_index = bytes_read < 0 ? 0 : bytes_read;
  ((char*)s->data)[nul_index] = '\0';
  s->size = nul_index;

  return bytes_read;
}
  

bool file_good(const file* file) {
  return file != NULL;
}


bool file_equal(const file* l, const file* r) {
  return l == r || l->fd == r->fd;
}
