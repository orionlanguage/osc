#include "memory.h"

#include <stdio.h>


bool debug = false;


void* mcalloc(size_t count, size_t size) {
  void* result = calloc(count, size);
  if (debug) {
    printf("calloc(%zu, %zu bytes) -> %p\n", count, size, result);
  }
  return result;
}


void mfree(void* ptr) {
  if (debug) {
    printf("free(%p)\n", ptr);
  }
  free(ptr);
}


void* mmalloc(size_t size) {
  void* result = malloc(size);
  if (debug) {
    printf("malloc(%zu) -> %p\n", size, result);
  }
  return result;
}


void* mrealloc(void *ptr, size_t size) {
  void* result = realloc(ptr, size);
  if (debug) {
    printf("mrealloc(%p, %zu bytes) -> %p\n", ptr, size, result);
  }
  return result;
}
