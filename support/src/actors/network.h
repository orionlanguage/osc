#pragma once

#include "string.h"
#include "fileio.h"


typedef int port_t;


/* Server Socket */
file* tcp_bind(port_t port);          // bind()s and listen()s, returns server socket.
file* tcp_accept(const file* sock); // blocks, accepts new client, returns connection socket.

/* Client Socket */
file* tcp_connect(const string* url, port_t port);

/* Socket IO Helpers */
int tcp_select(const vector* sockets, double timeout_seconds);
bool tcp_ready(const file* sock);
