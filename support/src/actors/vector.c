#include <stdio.h>
#include <string.h>
#include <assert.h>

#include "memory.h"
#include "vector.h"
#include <stdarg.h>


static bool VECTOR_DEBUG = false;


static const size_t vector_default_capacity = 10;
static const size_t vector_realloc_factor = 2;


void vector_destroy_elements(vector* vector) {
  if (vector->element_destructor) {
    VECTOR_FOREACH(void*, element, vector) {
      vector->element_destructor(element);
    }
  }
}


void vector_realloc(vector* vector, size_t new_capacity) {
  vector->data = mrealloc(vector->data, new_capacity * vector->element_size);
  vector->capacity = new_capacity;
  if (vector->data == NULL) {
    ERROR("Failed to allocate %zu elements for vector at %p!", new_capacity, (void*)vector);
  }
}


void vector_reserve(vector* vector, size_t capacity) {
  vector_realloc(vector, capacity);
}


void vector_init(vector* vector, size_t element_size) {
  vector->data = NULL;
  vector->size = 0;
  vector->element_size = element_size;
  vector->element_destructor = NULL;
  vector_realloc(vector, vector_default_capacity);
}


void vector_append(vector* vector, const void* element) {
  if (vector->size == vector->capacity) {
    size_t new_capacity = vector->capacity * vector_realloc_factor;
    if (VECTOR_DEBUG) {
      printf("Reallocating vector from %zu -> %zu (element size %zu, realloc_factor) \n", vector->capacity, new_capacity, vector->element_size);
    }
    vector_realloc(vector, new_capacity);
  }
  vector->size += 1;
  //printf("New Size +=: %u\n", vector->size);
  vector_set(vector, vector->size - 1, element);
  //printf("New Size After: %u\n", vector->size);
}



void* vector_get(const vector* vector, int index) {
  size_t adjusted_index = index < 0 ? (size_t)((int)vector->size + index) : (size_t)index;

  if (adjusted_index > vector->size) {
    ERROR("vector_get: out of range (%d >= size %zu)", index, vector->size);
    return NULL;
  }
  //printf("Vector Get: - found_value: %d\n",*((char*)vector->data) + (adjusted_index * vector->element_size));
  return ((char*)vector->data) + (adjusted_index * vector->element_size);
}


void vector_remove(vector* vector, int index) {
  size_t adjusted_index = index < 0 ? (size_t)((int)vector->size + index) : (size_t)index;

  if (adjusted_index >= vector->size) {
    return;
  }

  if (vector->element_destructor) {
    vector->element_destructor(vector_get(vector, index));
  }
  size_t bytes_to_copy = (vector->size - (adjusted_index + 1)) * vector->element_size;

  void* dst = vector_get(vector, adjusted_index);
  void* src = vector_get(vector, adjusted_index + 1);
  memmove(dst, src, bytes_to_copy);

  vector->size -= 1;
}


int vector_find(vector* vector, void* value) {
  int result = -1;

  for (int i = 0; i < vector_size(vector); ++i) {
    if (memcmp(vector_get(vector, i), value, vector->element_size) == 0) {
      return i;
    }
  }

  return result;
}


void vector_clear(vector* _vector) {
  vector_destroy_elements(_vector);
  _vector->size = 0;
}

int vector_size(const vector* vector) {
  return (int)vector->size;
}

int vector_len(int a, ...){
  int res = -1;
  va_list args;

  va_start(args,a);

  res = vector_size(va_arg(args, vector*));

  va_end(args);

  return res;
}

void _vector_clear(int a, ... ){

  va_list args;

  va_start(args,a);

  vector_clear(va_arg(args, vector*));

  va_end(args);
}

void* vector_set(vector* vector, int index, const void* element) {
  size_t adjusted_index = index < 0 ? (size_t)((int)vector->size + index) : (size_t)index;
  if (adjusted_index > vector->size) {
    ERROR("vector_set: out of range (%d >= size %zu)", index, vector->size);
  }

  void* dst = ((char*)vector->data + (vector->element_size * adjusted_index));

  memcpy(dst, element, vector->element_size);
  return dst;
}


void vector_free(vector* vector) {
  vector_destroy_elements(vector);
  mfree(vector->data);
  vector->data = NULL;
  vector->size = 0;
  vector->capacity = 0;
}


vector* vector_make(size_t element_size) {
  vector* result = mmalloc(sizeof(vector));
  vector_init(result, element_size);
  return result;
}


void vector_destroy(vector** vector) {
  if (vector == NULL) {
    return;
  }

  vector_free(*vector);
  free(*vector);
  *vector = NULL;
}


vector* vector_copy(const vector* other) {
  vector* result = vector_make(other->element_size);
  result->element_copy = other->element_copy;
  result->element_destructor = other->element_destructor;
  vector_realloc(result, other->capacity);
  memcpy(result->data, other->data, other->element_size * other->size);
  return result;
}


void vector_deepcopy_into(const vector* this, vector* result) {
  vector_realloc(result, this->capacity);

  result->element_copy = this->element_copy;
  result->element_destructor = this->element_destructor;

  for (size_t i = 0; i < this->size; ++i) {
    void* dst = vector_get(result, i);
    const void* src = vector_get(this, i);
    this->element_copy(dst, src);
    result->size = i + 1;
  }
}


vector* vector_deepcopy(const vector* this) {
  vector* result = vector_make(this->element_size);
  vector_deepcopy_into(this, result);
  return result;
}


void vector_pop_back(vector* vector) {
  if (vector->size == 0) {
    return;
  }

  if (vector->element_destructor) {
    void* element = vector_get(vector, vector->size - 1);
    vector->element_destructor(element);
  }

  vector->size -= 1;
}


vector* vector_range(int exclusive_max) {
	vector* result = vector_make(sizeof(int));

	for (int i = 0; i < exclusive_max; ++i) {
		vector_append(result, &i);
	}

	return result;
}
