#pragma once

#include <stdbool.h>
#include <stdlib.h>


extern bool debug;


void* mcalloc(size_t count, size_t size);
void  mfree(void* ptr);
void* mmalloc(size_t size);
void* mrealloc(void* ptr, size_t size);
