#include "string.h"

#include <stdarg.h>
#include <stdio.h>

#include "vector.h"


// We need to have null-terminated strings,
// but the NULL byte should not be part of the
// string length, otherwise this isn't handled
// like a vector in e.g. for loops.
//
// Solution: add then pop a NULL byte, so it's always part
// of the cstring.
void string_ensure_nul(string* s) {
  char nul = '\0';
  vector_append(s, &nul);
  vector_pop_back(s);
}


string* string_make(const char* src) {
  string* result = vector_make(sizeof(char));
  while (*src) {
    vector_append(result, src);
    src += 1;
  }
  string_ensure_nul(result);
  return result;
}


void string_destroy(string** string) {
  vector_destroy(string);
}


string* string_concatenate(const string* l, const string* r) {
  string* result = string_copy(l);

  const char* rdata = r->data;
  for (size_t i = 0; i < r->size; ++i, ++rdata) {
    vector_append(result, rdata);
  }

  string_ensure_nul(result);

  return result;
}


bool string_equal(const string* l, const string* r) {
  if (l->size != r->size) {
    return false;
  }

  for (size_t i = 0; i < l->size; ++i) {
    char lchar = ((const char*)l->data)[i];
    char rchar = ((const char*)r->data)[i];
    if (lchar != rchar) {
      return false;
    }
  }

  return true;
}


string* string_copy(const string* s) {
  return string_make((const char*)s->data);
}


string* string_format(const string* format, ...) {
  static const size_t maxlen = 4096;
  string* result = string_make("");
  vector_reserve(result, maxlen);

  va_list args;
  va_start(args, format);
  int length = vsnprintf(result->data, maxlen, string_get(format), args);

  if (length <= 0) {
    ERROR("Failed to format string!");
    return result;
  }

  result->size = length;
  va_end(args);

  return result;
}


void string_print(const string* s) {
  printf("%s\n", s->data);
}


const char* string_get(const string* s) {
  return (const char*)s->data;
}


string* input(){
  ssize_t bytes_read;
  size_t nbytes = 4096;
  char *in_string;
  string* result;

  in_string = (char *) malloc (nbytes);
  bytes_read = getline (&in_string, &nbytes, stdin);

  if (bytes_read == -1) {
    free(in_string);
    return string_make("");
  } else {
    result = string_make(in_string);
    vector_pop_back(result);
    string_ensure_nul(result);
    free(in_string);
    return result;
  }
}


void string_append(string* s, const void* character) {
  vector_append(s, character);
  string_ensure_nul(s);
}


int string_stoi(const string* s) {
  return atoi(string_get(s));
}


double string_stof(const string* s) {
  return atof(string_get(s));
}
