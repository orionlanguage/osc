#pragma once

#include "vector.h"


struct actor_;
typedef void(*actor_fn)(struct actor_*, void*);
typedef struct actor_ {
  const char* name;
  actor_fn f;
  vector* subscribers;
} actor;


void    actor_init(actor* actor, actor_fn f);
void    actor_free(actor* actor);

actor*  actor_make(actor_fn f);
void    actor_destroy(actor** actor);

void    actor_start(actor* actor, void* value);
void    actor_emit(actor* actor, void* value);
actor*  actor_subscribe(actor* me, const actor* subscriber);
void    actor_unsubscribe(actor* me, const actor* subscriber);
actor*  actor_subscribe_last(actor* me, const actor* subscriber);
actor*  actor_copy_subscribe(const actor* l, const actor* r);
actor*  actor_copy_subscribe_last(const actor* l, const actor* r);
actor*  actor_copy(const actor* other);
void    actor_copy_into(const actor* other, actor* result);
void    actor_copy_fn(void* dst, const void* src);
