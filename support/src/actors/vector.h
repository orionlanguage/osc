#pragma once

#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>

#define ERROR(...) fprintf(stderr, __VA_ARGS__)


#define VECTOR_FOREACH(TYPE, ELEMENT, V) \
  TYPE* ELEMENT = (TYPE*)(V)->data; \
  for(size_t __i = 0; __i < (V)->size; ++__i, ELEMENT = (TYPE*)(((char*)(V)->data) + (__i * (V)->element_size)))


typedef void(*element_destructor)(void*);
typedef void(*element_copy)(void*, const void*);


struct vector_ {
    void* data;
    size_t size;
    size_t capacity;
    size_t element_size;
    element_destructor element_destructor;
    element_copy element_copy;
};

typedef struct vector_ vector;


void    vector_init(vector* vector, size_t element_size);
void    vector_free(vector* vector);

vector* vector_make(size_t element_size);
void    vector_destroy(vector** vector);
vector* vector_copy(const vector* other);
void    vector_reserve(vector* vector, size_t capacity);

vector* vector_deepcopy(const vector* me);
void vector_deepcopy_into(const vector* me, vector* result);

void   vector_append(vector*, const void*);
void   vector_pop_back(vector*);
void*  vector_set(vector*, int, const void*);
void*  vector_get(const vector*, int);
void   vector_remove(vector*, int index);
int    vector_find(vector*, void* value);
void   vector_clear(vector*);
int    vector_size(const vector*);
int    vector_size(const vector*);

void    _vector_clear(int a, ...);
int    vector_len(int a, ...);

vector* vector_range(int exclusive_max);
