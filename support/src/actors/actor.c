#include "actor.h"
#include "memory.h"

#include <stdio.h>


void actor_destructor(void* actor) {
  actor_free(actor);
}


void actor_init(actor* actor, actor_fn f) {
  actor->f = f;
  actor->subscribers = vector_make(sizeof(struct actor_*));
  actor->subscribers->element_destructor = &actor_destructor;
  actor->subscribers->element_copy = &actor_copy_fn;
}


void actor_free(actor* actor) {
  vector_destroy(&actor->subscribers);
  actor->f = NULL;
}


actor* actor_make(actor_fn f) {
  actor* result = mmalloc(sizeof(actor));
  actor_init(result, f);
  return result;
}


void actor_destroy(actor** actor) {
  if (actor == NULL || *actor == NULL) {
    return;
  }

  actor_free(*actor);
  free(*actor);
  *actor = NULL;
}


void actor_start(actor* actor, void* value) {
  actor->f(actor, value);
}


void actor_emit(actor* this, void* value) {
  VECTOR_FOREACH(actor*, iter, this->subscribers) {
    (*iter)->f(*iter, value);
  }
}


actor* actor_subscribe(actor* this, const actor* subscriber) {
  vector_append(this->subscribers, &subscriber);
  return this;
}


void actor_unsubscribe(actor* me, const actor* subscriber) {
  int index = vector_find(me->subscribers, subscriber);

  if (index == -1) {
    return;
  }

  vector_remove(me->subscribers, index);
}


actor* actor_subscribe_last(actor* me, const actor* subscriber) {
  if (me->subscribers->size == 0) {
    return actor_subscribe(me, subscriber);
  }

  actor* first_child = *(actor**)vector_get(me->subscribers, 0);
  actor_subscribe_last(first_child, subscriber);
  return me; // do not return actor_subscribe_last
}


actor* actor_copy_subscribe(const actor* l, const actor* r) {
  return actor_subscribe(actor_copy(l), r);
}


actor* actor_copy_subscribe_last(const actor* l, const actor* r) {
  return actor_subscribe_last(actor_copy(l), r);
}


void actor_copy_fn(void* vdst, const void* vsrc) {
  actor** dst = vdst;
  const actor** src = (const actor**) vsrc;
  *dst = actor_make(NULL);
  actor_copy_into(*src, *dst);
}


actor* actor_copy(const actor* this) {
  actor* result = actor_make(this->f);
  actor_copy_into(this, result);
  return result;
}


void actor_copy_into(const actor* this, actor* result) {
  result->name = this->name;
  result->f = this->f;
  result->subscribers = vector_deepcopy(this->subscribers);
}
