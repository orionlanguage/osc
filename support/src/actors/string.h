#pragma once

#include <stdbool.h>

#include "vector.h"


typedef vector string;


string*     string_make(const char* src);
string*     string_concatenate(const string* l, const string* r);
string*     string_copy(const string* s);
bool        string_equal(const string* l, const string* r);
void        string_destroy(string** string);
string*     string_format(const string* format, ...);
void        string_print(const string* s);
const char* string_get(const string* s);
string* input();

void        string_append(string* s, const void* character);
int         string_stoi(const string* s);
double      string_stof(const string* s);
