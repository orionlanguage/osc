#include "network.h"

#include <strings.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#include <math.h>
#include <errno.h>
#include <stdio.h>

#include <string.h>

#include "memory.h"
#include "vector.h"


file* tcp_connect(const string* url, port_t port) {
  struct sockaddr_in serveraddr;

  int sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if (sockfd < 0) {
		ERROR("socket() returned error.");
    return NULL;
  }

  struct hostent* server = gethostbyname(string_get(url));
  if (server == NULL) {
		ERROR("gethostbyname() failed to resolve host.");
    return NULL;
  }

  bzero((char *)&serveraddr, sizeof(serveraddr));
  serveraddr.sin_family = AF_INET;

  bcopy((char *)server->h_addr, 
        (char *)&serveraddr.sin_addr.s_addr, 
        server->h_length);
  serveraddr.sin_port = htons(port);

  if (connect(sockfd, (const struct sockaddr*)&serveraddr, sizeof(serveraddr)) < 0) {
		ERROR("connect() returned error.");
    return NULL;
  }

  return file_make(sockfd);
}


file* tcp_bind(port_t port) {
  struct sockaddr_in serv_addr;

  int sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if (sockfd < 0) {
    return NULL;
  }

  int reuse = true;
  setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &reuse, sizeof(reuse));

  bzero((char*)&serv_addr, sizeof(serv_addr));

  serv_addr.sin_family = AF_INET;
  serv_addr.sin_addr.s_addr = INADDR_ANY;
  serv_addr.sin_port = htons(port);

  if (bind(sockfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0) {
    ERROR("bind(%d) to port %d failed: %s\n", sockfd, port, strerror(errno));
    return NULL;
  }

  if (listen(sockfd, 5) < 0) {
    ERROR("listen(%d) failed\n", sockfd);
    return NULL;
  }

  return file_make(sockfd);
}


file* tcp_accept(const file* sock) {
  struct sockaddr_in cli_addr;
  socklen_t clilen = sizeof(cli_addr);
  int client_sockfd = accept(sock->fd, (struct sockaddr*)&cli_addr, &clilen);

  if (client_sockfd < 0) {
    return NULL;
  }

  return file_make(client_sockfd);
}


struct timeval make_timeout(double timeout_seconds) {
  float seconds_part = floor(timeout_seconds);
  float fractional_part = timeout_seconds - seconds_part;
  return (struct timeval) {
    .tv_sec  = (long)seconds_part,
    .tv_usec = (int)(fractional_part * 1000000.0)
  };
}


fd_set fds = {0};


int tcp_select(const vector* sockets, double timeout_seconds) {
  FD_ZERO(&fds);

  VECTOR_FOREACH(file*, f, sockets) {
    FD_SET((*f)->fd, &fds);
  }

  struct timeval timeout = make_timeout(timeout_seconds);
  return select(FD_SETSIZE, &fds, NULL, NULL, timeout_seconds > 0.0f ? &timeout : NULL);
}


bool tcp_ready(const file* f) {
  return FD_ISSET(f->fd, &fds);
}
