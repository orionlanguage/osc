cmake_minimum_required(VERSION 3.0)
project(actors)


add_library(${PROJECT_NAME}
  src/actors/actor.h
  src/actors/actor.c

  src/actors/memory.h
  src/actors/memory.c

  src/actors/vector.h
  src/actors/vector.c

  src/actors/string.h
  src/actors/string.c

  src/actors/network.h
  src/actors/network.c

  src/actors/fileio.h
  src/actors/fileio.c
  )


target_include_directories(${PROJECT_NAME} PUBLIC
  src/
  )

if (EXISTS vendor/googletest)
  set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wall -Wextra -Wpedantic -g")
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -Wpedantic -g")
  add_subdirectory(vendor/googletest)
  add_subdirectory(test/)
endif ()
